-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image` (`image`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `images` (`id`, `name`, `parent_id`, `type`, `image`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
(24,	'The Sketch Had David Once Again Impersonating Sanders As Voters ',	10,	'blog',	'uploads/blog/36b34c3c238ebb0222ba7b77b9a401da.jpg',	'2016-09-29 07:20:32',	4,	NULL,	NULL),
(25,	'The sketch had David once again impersonating Sanders as voters ',	11,	'blog',	'uploads/blog/654a7ed44cee6838a3ae0b8ac563809a.jpg',	'2016-09-29 07:23:48',	4,	NULL,	NULL),
(26,	'Those people later turn out to be key to his slim loss to Hillar',	12,	'blog',	'uploads/blog/f8d916eb7a3f672d3ab3f51f4fda76d9.jpg',	'2016-09-29 07:25:19',	4,	NULL,	NULL),
(27,	'Taking a line right out of David\'s book, Sanders responded',	13,	'blog',	'uploads/blog/59f87e82cbbd80857d1146626ccece12.jpg',	'2016-09-29 07:26:50',	4,	NULL,	NULL),
(28,	'Donald Trump goes after Mike Bloomberg where it hurt',	14,	'blog',	'uploads/blog/6d22197f9a3c0311e915004e2ccc7192.jpg',	'2016-09-29 07:29:00',	4,	NULL,	NULL),
(31,	'As the number of cases on the national hotline increases, so doe',	18,	'blog',	'uploads/blog/1de39c21cf7f1dae0a5449fbe0293c27.jpg',	'2016-09-29 08:36:22',	4,	NULL,	NULL),
(32,	'The modern slavery that is intricately woven throughout our glob',	19,	'blog',	'uploads/blog/e41e347083980cd18fa864ce9e6970d0.jpg',	'2016-09-29 08:37:11',	4,	NULL,	NULL),
(34,	'The depth and breadth of the modern slavery that is intricately',	21,	'blog',	'uploads/blog/6f35543fc36aa45674b0115784f27eb9.jpg',	'2016-09-29 08:39:10',	4,	NULL,	NULL),
(51,	'The Interview Comes As The Infidelity Scandal And Other',	38,	'blog',	'uploads/blog/c82b42a33430548c0ca23092c76df7d0.jpg',	'2016-09-30 08:10:17',	4,	NULL,	NULL),
(53,	'He Just Sort Of Puts His Hands On Her Shoulders',	40,	'blog',	'uploads/blog/61703cb1dc29b664955b7c57b60e2f96.jpg',	'2016-09-30 08:12:19',	4,	NULL,	NULL),
(54,	'The Winner Of The Caucuses Is Decided By State',	41,	'blog',	'uploads/blog/a6b098612d9d17a5c66120cc31e9713c.jpg',	'2016-09-30 08:12:58',	4,	NULL,	NULL),
(55,	'Rosa Went With The Man To Another Town In Mexico Called Tenancin',	42,	'blog',	'uploads/blog/8d31d38c4ae5766c4c92d02585b99658.jpg',	'2016-09-30 08:13:41',	4,	NULL,	NULL),
(56,	'Judul',	1,	'blog',	'uploads/blog/af6d03ae2632a20156904bac7329319f.jpg',	'2016-11-02 03:34:26',	4,	NULL,	NULL),
(57,	'Judul',	19,	'blog',	'uploads/blog/335f705c24ac6305e7114076315cb531.jpg',	'2016-11-02 07:37:12',	4,	NULL,	NULL),
(58,	'Judul',	20,	'blog',	'uploads/blog/56c932d34d18986ebb92e30ede657a86.jpg',	'2016-11-02 07:45:06',	4,	NULL,	NULL),
(61,	'asd',	22,	'blog',	'uploads/blog/3a5eca96d774897035fabe96ec63c0ea.jpg',	'2016-11-17 03:34:17',	4,	NULL,	NULL),
(64,	'Inap',	6,	'blog',	'uploads/blog/8d13ca8cc3c6650c9bbaab3193e4dc6f.jpg',	'2016-11-17 04:07:01',	4,	NULL,	NULL),
(67,	'Judul',	5,	'blog',	'uploads/blog/d14bf666cc5a828bab681af48eaa17b3.png',	'2016-12-05 05:04:01',	4,	NULL,	NULL),
(69,	'Judul222',	27,	'blog',	'uploads/blog/8a98895ea8def98e052a576ce445c121.jpg',	'2017-01-26 05:38:27',	4,	NULL,	NULL),
(100,	'Jatim Park 1',	58,	'blog',	'uploads/blog/a381ab20ce9f70603ad5eacee03db12d.jpg',	'2017-02-14 02:24:19',	4,	NULL,	NULL),
(101,	'Jatim Park 1',	58,	'blog',	'uploads/blog/3694ec886c0aaabf29317a560d2e242c.JPG',	'2017-02-14 02:24:19',	4,	NULL,	NULL),
(102,	'Jatim Park 1',	58,	'blog',	'uploads/blog/99ce463e2231e35664f94c11d0f05397.jpg',	'2017-02-14 02:24:19',	4,	NULL,	NULL),
(103,	'Eco Green Park',	59,	'blog',	'uploads/blog/5ded4535ebb41879068f615b2cf05f29.jpg',	'2017-02-14 02:26:44',	4,	NULL,	NULL),
(104,	'Eco Green Park',	59,	'blog',	'uploads/blog/89db8e0821603f77a653560df64ff7f0.jpg',	'2017-02-14 02:26:44',	4,	NULL,	NULL),
(105,	'Jatim Park 2',	60,	'blog',	'uploads/blog/2e43fe4022844631e70280ecca4d3534.jpg',	'2017-02-14 02:30:03',	4,	NULL,	NULL),
(106,	'BNS',	61,	'blog',	'uploads/blog/528164ae3b3e2c66d6e666c0dbe4a261.jpg',	'2017-02-28 03:04:47',	4,	NULL,	NULL),
(107,	'BNS',	61,	'blog',	'uploads/blog/1a31d3871b07a26e424d581ad98e3fd4.jpg',	'2017-02-28 03:04:47',	4,	NULL,	NULL),
(108,	'BNS',	61,	'blog',	'uploads/blog/41a5ce8b42a1a1fef8466fc87a9a0efd.jpeg',	'2017-02-28 03:04:47',	4,	NULL,	NULL),
(109,	'Pasar Parkiran Kota Batu',	62,	'blog',	'uploads/blog/df6f713d7844557c7bdec883b8da86cf.jpg',	'2017-03-14 10:34:48',	4,	NULL,	NULL),
(110,	'Pasar Parkiran Kota Batu',	62,	'blog',	'uploads/blog/fa1c25f127d2ad6ce1feb775c9c48485.jpg',	'2017-03-14 10:34:49',	4,	NULL,	NULL),
(111,	'Paralayang Songgoriti',	63,	'blog',	'uploads/blog/fec73301d91fcf39d44903ffb39e6b53.jpg',	'2017-03-14 10:46:35',	4,	NULL,	NULL),
(112,	'Paralayang Songgoriti',	63,	'blog',	'uploads/blog/5d1618e45461232efba9a0054c16f8ad.jpg',	'2017-03-14 10:46:35',	4,	NULL,	NULL),
(113,	'Paralayang Songgoriti',	63,	'blog',	'uploads/blog/743b6f8f528d2bef488bcc88a0caa4eb.jpg',	'2017-03-14 10:46:35',	4,	NULL,	NULL),
(114,	'Alun-Alun Kota Batu',	64,	'blog',	'uploads/blog/0fb241eb7db58227d3ada0d7da172ae0.jpg',	'2017-03-14 10:55:23',	4,	NULL,	NULL),
(115,	'Alun-Alun Kota Batu',	64,	'blog',	'uploads/blog/f77eac5d9fb5ef73122121aa24cfee35.JPG',	'2017-03-14 10:55:23',	4,	NULL,	NULL),
(116,	'Alun-Alun Kota Batu',	64,	'blog',	'uploads/blog/ecb85d783d2b05f1c3141673bc39ec7d.jpg',	'2017-03-14 10:55:23',	4,	NULL,	NULL),
(117,	'Coban Rais - Batu',	65,	'blog',	'uploads/blog/7a59b7befb614a5a3a6a0211cdab4c2b.jpg',	'2017-03-15 03:16:31',	4,	NULL,	NULL),
(118,	'Coban Rais - Batu',	65,	'blog',	'uploads/blog/5ba34a1a8a5fc019b1e561172e1a2e2f.jpg',	'2017-03-15 03:16:32',	4,	NULL,	NULL),
(119,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/1b04cfe22ed7d4d5781ed847b9fb6089.jpg',	'2017-03-15 03:45:38',	4,	NULL,	NULL),
(120,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/e0a93518b8910d9a2312dd36aaffe8ae.jpg',	'2017-03-15 03:45:38',	4,	NULL,	NULL),
(121,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/5db96f65a186bda5e8f4bf543005f7af.jpg',	'2017-03-15 03:45:38',	4,	NULL,	NULL),
(122,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/7bba8a1180be01761492dbce30f42886.jpg',	'2017-03-15 03:45:38',	4,	NULL,	NULL),
(123,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/03fb5df483b829becefc39c68754afa8.jpg',	'2017-03-15 03:45:39',	4,	NULL,	NULL),
(124,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/33147c9212deb08e8ded4563cf9c083b.jpg',	'2017-03-15 03:45:39',	4,	NULL,	NULL),
(125,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/bcc9abf2e8eb22fdaa63a73b4d6f080c.jpg',	'2017-03-15 03:45:39',	4,	NULL,	NULL),
(126,	'Homestay Puta Menggala Kota Batu',	1,	'blog',	'uploads/penginapan/',	'2017-03-15 03:45:39',	4,	NULL,	NULL),
(127,	'Tampak Depan',	2,	'blog',	'uploads/penginapan/3ec3e73b94aacf37232b5dcd93eada04.jpg',	'2017-03-15 03:56:52',	4,	NULL,	NULL),
(128,	'Homestay Putra Menggala - Kota Batu,Malang Murah',	3,	'blog',	'uploads/penginapan/134235726c6b1d3dbffb376c5acfe696.jpg',	'2017-03-15 04:13:12',	4,	NULL,	NULL),
(129,	'Homestay Putra Menggala - Kota Batu,Malang Murah',	3,	'blog',	'uploads/penginapan/1f244d3f9a528f50488647e84ec44e72.jpg',	'2017-03-15 04:13:12',	4,	NULL,	NULL),
(130,	'Homestay Putra Menggala - Kota Batu,Malang Murah',	3,	'blog',	'uploads/penginapan/a6bf037ad87d676c9e77ebfa9453d197.jpg',	'2017-03-15 04:13:13',	4,	NULL,	NULL),
(131,	'Homestay Putra Menggala - Kota Batu,Malang Murah',	3,	'blog',	'uploads/penginapan/4200421b3060a87e007b80ae6fa2038a.jpg',	'2017-03-15 04:13:14',	4,	NULL,	NULL),
(132,	'Homestay Putra Menggala - Kota Batu,Malang Murah',	3,	'blog',	'uploads/penginapan/3ad514cfe1c82627ab4e3ea3ed180bc8.jpg',	'2017-03-15 04:13:14',	4,	NULL,	NULL),
(133,	'Kurnia Homestay Murah di batu Malang',	4,	'blog',	'uploads/penginapan/7bbc3cfceee2c24d6b328b19828ecbf1.jpg',	'2017-03-17 10:39:45',	4,	NULL,	NULL),
(134,	'Kurnia Homestay Murah di batu Malang',	4,	'blog',	'uploads/penginapan/8ef5c0e6e77160b35d5d51e70c377da0.jpg',	'2017-03-17 10:39:46',	4,	NULL,	NULL),
(135,	'Kurnia Homestay Murah di batu Malang',	4,	'blog',	'uploads/penginapan/27aad749fa643b4a4150e1ae6c7f6bfc.jpg',	'2017-03-17 10:39:47',	4,	NULL,	NULL),
(136,	'Kurnia Homestay Murah di batu Malang',	4,	'blog',	'uploads/penginapan/1c5dbd3014ad4643f2b956dff34ce032.jpg',	'2017-03-17 10:39:48',	4,	NULL,	NULL),
(137,	'Kurnia Homestay Murah di batu Malang',	4,	'blog',	'uploads/penginapan/c96594ffb9886eb3796b327e41c83693.jpg',	'2017-03-17 10:39:48',	4,	NULL,	NULL),
(138,	'Lorem Penginapan',	6,	'blog',	'uploads/blog/5c3ee2d4ecbcc81176453feab1ea1b3c.jpg',	'2017-04-05 09:23:36',	81,	NULL,	NULL),
(139,	'Tampak Depan',	8,	'blog',	'uploads/penginapan/c8416af843905fcb909fb96aa7235640.jpg',	'2017-04-06 03:53:31',	81,	NULL,	NULL),
(140,	'test',	9,	'blog',	'uploads/penginapan/3fde5b7bf520f32e16908402a295f8a3.jpg',	'2017-04-06 04:04:58',	81,	NULL,	NULL),
(145,	'Tampak Depan',	7,	'blog',	'uploads/penginapan/5b2749688ee15582c05ad773b578ea8c.jpg',	'2017-04-06 04:14:37',	81,	NULL,	NULL),
(146,	'Tampak Depan',	7,	'blog',	'uploads/penginapan/3587efa38b4edaab88934b4e9a5ef2b2.jpg',	'2017-04-06 04:14:37',	81,	NULL,	NULL),
(147,	'Museum Angkut',	66,	'blog',	'uploads/blog/283b9a21625f6155f8fe0b6ff1e9d902.jpg',	'2017-05-14 14:42:22',	4,	NULL,	NULL),
(148,	'Museum Angkut',	66,	'blog',	'uploads/blog/72859ac0d58eccdc3484392f18a3ff36.jpg',	'2017-05-14 14:42:22',	4,	NULL,	NULL),
(149,	'test',	16,	'blog',	'uploads/penginapan/3476bf4e2f32768bda389b2ab4d4bd09.jpg',	'2017-05-15 05:44:06',	4,	NULL,	NULL),
(150,	'Homestay Dennis Murah',	17,	'blog',	'uploads/penginapan/c827960dec1084105d55816087bf8d7f.jpg',	'2017-05-15 05:48:05',	4,	NULL,	NULL),
(151,	'Tampak Depan',	18,	'blog',	'uploads/penginapan/b205fe6d09f7f8f7f0e3754a5b2e7e14.jpg',	'2017-05-15 06:00:25',	NULL,	NULL,	NULL),
(152,	'Tampak Depan',	19,	'blog',	'uploads/penginapan/c97f4ee6f9cb8bf272b2040c3270cf25.jpg',	'2017-05-15 06:01:42',	NULL,	NULL,	NULL),
(153,	'Tampak Depan',	20,	'blog',	'uploads/penginapan/3cc2d0686ac167b75ccaaabb55fefed4.jpg',	'2017-05-15 06:01:58',	NULL,	NULL,	NULL),
(154,	'Tampak Depan',	21,	'blog',	'uploads/penginapan/f88ff4176a1d7e12d5bfa98c16cc1137.jpg',	'2017-05-15 06:02:31',	NULL,	NULL,	NULL),
(155,	'Test',	22,	'blog',	'uploads/penginapan/47ec1e378c6c21c1ef2da7ac08203357.jpg',	'2017-05-15 06:08:16',	4,	NULL,	NULL),
(156,	'coba',	23,	'blog',	'uploads/penginapan/9aab4c13097075e2b0cc85623506620a.jpg',	'2017-05-15 06:12:48',	134,	NULL,	NULL),
(157,	'testing',	24,	'blog',	'uploads/penginapan/9173f6fbc1fdaed82da406b3c110bc2e.jpg',	'2017-05-15 06:17:17',	NULL,	NULL,	NULL),
(158,	'dari agen',	25,	'blog',	'uploads/penginapan/c321a520cf2d7b8009c64aa5698196ee.jpg',	'2017-05-15 06:21:56',	134,	NULL,	NULL),
(159,	'Dari admin',	26,	'blog',	'uploads/penginapan/e090f5dbab3e776068b4b8fe8ab84342.jpg',	'2017-05-15 06:30:16',	NULL,	NULL,	NULL),
(160,	'sudo su',	27,	'blog',	'uploads/penginapan/f07bbb7b92bdf976d452b2bbbdf2f7a5.jpg',	'2017-05-15 07:02:18',	NULL,	NULL,	NULL),
(161,	'dari admin',	28,	'blog',	'uploads/penginapan/71e1973f2b851aafe3907cf1e70f6643.jpg',	'2017-05-15 07:10:55',	NULL,	NULL,	NULL),
(162,	'dari agen',	29,	'blog',	'uploads/penginapan/b9655b570142c669afd2b7683b5f1ba5.jpg',	'2017-05-15 07:13:51',	134,	NULL,	NULL),
(164,	'asd',	30,	'blog',	'uploads/penginapan/e476ca9d22855dfb366091b231303489.jpg',	'2018-02-14 05:30:33',	4,	NULL,	NULL),
(165,	'dgfdg',	31,	'blog',	'uploads/penginapan/dd0edda333f2c0a5c356a16328a4db50.jpg',	'2018-02-14 05:31:28',	4,	NULL,	NULL),
(166,	'asdasd',	32,	'blog',	'uploads/penginapan/55730bd47da07c9f63228f7aea2009ed.jpg',	'2018-02-14 05:33:55',	NULL,	NULL,	NULL),
(167,	'dsfsdf',	33,	'blog',	'uploads/penginapan/0659000a9786d7f1c0c28f2995465bb8.jpg',	'2018-02-14 05:37:11',	NULL,	NULL,	NULL),
(168,	'dgf',	34,	'blog',	'uploads/penginapan/e1ee43dac17a09760f0f846433ed41bd.jpeg',	'2018-02-14 05:42:52',	NULL,	NULL,	NULL),
(169,	'sad',	35,	'blog',	'uploads/penginapan/8e45aa983fad010c2f9c56b1a8760b2d.jpg',	'2018-02-14 05:53:25',	4,	NULL,	NULL),
(171,	'wado',	36,	'blog',	'uploads/penginapan/380135fe3ede91e825faed4327271468.jpeg',	'2018-02-14 05:59:34',	4,	NULL,	NULL),
(173,	'ilike',	37,	'blog',	'uploads/penginapan/033573ade5c03f976eb1a3e2ca985163.jpeg',	'2018-02-14 06:01:08',	4,	NULL,	NULL),
(174,	'asw',	38,	'blog',	'uploads/penginapan/286a52b7149df51ef408ac89aeae44a0.jpeg',	'2018-02-14 06:13:20',	139,	NULL,	NULL),
(176,	'adminasw',	39,	'blog',	'uploads/penginapan/16e555b722c0f1c88d85e37b9ac08bfb.jpg',	'2018-02-14 06:52:58',	4,	NULL,	NULL);

DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `levels` (`id`, `nama`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
(1,	'kasir',	'2018-04-16 12:45:57',	0,	'2018-04-16 12:45:57',	0),
(2,	'waiter',	'2018-04-16 12:46:08',	0,	'2016-12-20 04:01:09',	4);

DROP TABLE IF EXISTS `level_access`;
CREATE TABLE `level_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `hak_akses` text NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `level_access` (`id`, `id_level`, `hak_akses`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
(6,	1,	'dashboard,access,levels,admins,food,drink,product,order,waiter',	'2018-04-16 13:07:07',	0,	'2016-12-13 03:26:38',	4),
(7,	2,	'order,dashboard,report',	'2018-04-16 21:53:33',	0,	'2017-01-12 03:27:53',	4);

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `token_email` varchar(128) DEFAULT NULL,
  `status` varchar(16) DEFAULT NULL,
  `id_level` int(11) NOT NULL,
  `fb_id` text NOT NULL,
  `fb_picture` text NOT NULL,
  `google_id` text NOT NULL,
  `google_picture` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `login` (`id`, `name`, `email`, `phone`, `password`, `token_email`, `status`, `id_level`, `fb_id`, `fb_picture`, `google_id`, `google_picture`, `created_at`, `modified_at`, `modified_by`) VALUES
(4,	'Kasir Ganteng',	'kasir@erp.com',	'08223416788021',	'3ZVt0U1qGQ5Wt2H4nLRQ3fbPWyhpoB0wOxgAftmVjUAFcXdcdjP3DuvuKA0gqYd4VVDVs0a/nQ3XxEwpTfCTrA==',	NULL,	'active',	1,	'0',	'',	'',	'',	'2016-09-02 08:04:53',	'2016-12-02 10:04:05',	4),
(82,	'',	'',	NULL,	'',	NULL,	NULL,	0,	'301192323585479',	'https://graph.facebook.com/301192323585479/picture?type=normal',	'',	'',	'2016-09-30 09:03:57',	NULL,	NULL),
(83,	'',	'',	NULL,	'',	NULL,	NULL,	0,	'',	'',	'113217639075039404276',	'https://lh5.googleusercontent.com/-TWh5TSpaT7c/AAAAAAAAAAI/AAAAAAAAASM/fZyb1g5ltX8/photo.jpg',	'2016-10-20 06:23:31',	NULL,	NULL),
(84,	'',	'',	NULL,	'',	NULL,	NULL,	0,	'414901202187355',	'https://graph.facebook.com/414901202187355/picture?type=normal',	'',	'',	'2017-02-09 06:24:51',	NULL,	NULL),
(87,	'nando',	'waiter@erp.com',	'839538',	'+8TBIRmSGgPjdvO3DJg935GhggxnYprjRy6q4gi5ow5GTnDZ6RK1OpOWbTkqBZD6FTgs13zQavUWsna83QIgGA==',	NULL,	'active',	2,	'',	'',	'',	'',	'2018-04-16 07:41:18',	'2018-04-16 07:44:22',	4);

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` varchar(255) NOT NULL,
  `table_num` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `order` (`id`, `table_num`, `status`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
('ERP170418-001',	12,	'active',	'2018-04-17 07:04:39',	87,	'2018-04-17 13:46:53',	4),
('ERP180418-002',	1,	'active',	'2018-04-18 07:01:57',	87,	NULL,	0);

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(255) NOT NULL,
  `product_id` int(200) NOT NULL,
  `qty` int(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `order_detail` (`id`, `parent_id`, `product_id`, `qty`) VALUES
(49,	'ERP170418-001',	26,	3),
(50,	'ERP170418-001',	27,	2),
(51,	'ERP180418-002',	26,	2),
(52,	'ERP180418-002',	29,	6);

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `price` varchar(200) NOT NULL,
  `image` varchar(300) NOT NULL,
  `type` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `product` (`id`, `name`, `price`, `image`, `type`, `status`, `created_at`, `created_by`, `modified_at`, `modified_by`) VALUES
(26,	'burgers',	'45000',	'uploads/product/d60fbc3743704917a69eda47ed36761f.jpg',	'food',	'ready',	'2018-04-16 06:29:03',	4,	'2018-04-16 06:30:36',	4),
(27,	'Milkshake',	'5000',	'uploads/product/a1986bae2fbf359d6df7ee0d976112bd.jpg',	'drink',	'ready',	'2018-04-16 07:03:09',	4,	'2018-04-16 23:11:45',	0),
(28,	'Pizza',	'200000',	'uploads/product/342710a9b792c4505306d0068469e82e.jpeg',	'food',	'ready',	'2018-04-17 12:45:10',	4,	'2018-04-17 12:45:10',	0),
(29,	'chicken wings',	'200000',	'uploads/product/d85fef47665d95353b394c8593af9d52.jpg',	'food',	'ready',	'2018-04-17 12:46:19',	4,	'2018-04-17 12:46:19',	0),
(30,	'Ice tea',	'100000',	'uploads/product/9e78726b57a38b5bfe8a36408d4fddaa.jpeg',	'drink',	'ready',	'2018-04-17 05:44:13',	4,	'2018-04-17 17:39:41',	0);

DROP TABLE IF EXISTS `tabel_log`;
CREATE TABLE `tabel_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_user` varchar(255) DEFAULT NULL,
  `log_tipe` int(11) DEFAULT NULL,
  `log_desc` varchar(255) DEFAULT NULL,
  `ip_addr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tabel_log` (`log_id`, `log_time`, `log_user`, `log_tipe`, `log_desc`, `ip_addr`) VALUES
(10,	'2018-04-17 16:02:07',	NULL,	2,	'nando add order',	NULL),
(11,	'2018-04-17 16:15:53',	NULL,	2,	'nando add order',	NULL),
(12,	'2018-04-17 16:34:30',	NULL,	2,	'nando add order',	NULL),
(13,	'2018-04-17 16:34:40',	NULL,	2,	'nando add order',	NULL),
(14,	'2018-04-17 16:35:05',	NULL,	2,	'nando add order',	NULL),
(15,	'2018-04-17 16:36:03',	NULL,	2,	'nando add order',	NULL),
(16,	'2018-04-17 16:36:35',	NULL,	2,	'nando add order',	NULL),
(17,	'2018-04-17 16:37:01',	NULL,	2,	'nando add order',	NULL),
(18,	'2018-04-18 07:01:57',	'nando',	2,	'nando add order',	NULL),
(20,	'2018-04-18 11:02:38',	'4',	0,	'Kasir Ganteng Login',	NULL),
(21,	'2018-04-18 11:11:26',	'4',	1,	'Kasir Ganteng Logout',	NULL),
(22,	'2018-04-18 11:11:31',	'87',	0,	'nando Login',	NULL),
(23,	'2018-04-18 11:19:08',	'87',	1,	'nando Logout',	'127.0.0.1'),
(24,	'2018-04-18 11:19:17',	'4',	0,	'Kasir Ganteng Login',	'127.0.0.1'),
(25,	'2018-04-18 11:20:09',	'4',	1,	'Kasir Ganteng Logout',	'127.0.0.1'),
(26,	'2018-04-18 11:20:15',	'4',	0,	'Kasir Ganteng Login',	'127.0.0.1'),
(27,	'2018-04-18 11:20:17',	'4',	1,	'Kasir Ganteng Logout',	'127.0.0.1'),
(28,	'2018-04-18 11:20:24',	'4',	0,	'Kasir Ganteng Login',	'127.0.0.1'),
(29,	'2018-04-18 11:20:39',	'4',	1,	'Kasir Ganteng Logout',	'127.0.0.1'),
(30,	'2018-04-18 11:20:43',	'87',	0,	'nando Login',	'127.0.0.1'),
(31,	'2018-04-18 11:21:41',	'87',	1,	'nando Logout',	'127.0.0.1'),
(32,	'2018-04-18 11:21:46',	'4',	0,	'Kasir Ganteng Login',	'127.0.0.1'),
(33,	'2018-04-18 11:21:48',	'4',	1,	'Kasir Ganteng Logout',	'127.0.0.1'),
(34,	'2018-04-18 11:22:13',	'87',	0,	'nando Login',	'127.0.0.1'),
(35,	'2018-04-18 11:22:27',	'87',	1,	'nando Logout',	'127.0.0.1'),
(36,	'2018-04-18 11:22:35',	'87',	0,	'nando Login',	'127.0.0.1'),
(37,	'2018-04-18 11:22:37',	'87',	1,	'nando Logout',	'127.0.0.1'),
(38,	'2018-04-18 11:23:12',	'4',	0,	'Kasir Ganteng Login',	'127.0.0.1'),
(39,	'2018-04-18 11:23:20',	'4',	1,	'Kasir Ganteng Logout',	'127.0.0.1');

-- 2018-04-18 11:24:11
