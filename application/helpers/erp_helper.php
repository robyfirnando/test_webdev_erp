<?php
	
	function is_login()
	{
		$CI =& get_instance();
	    $is_logged_in = $CI->session->userdata('back_userid');
		if(!isset($is_logged_in)){
			redirect('auth', 'refresh');   
		}
	}

    function is_login_frontend()
    {
        $CI =& get_instance();
        $is_logged_in = $CI->session->userdata('front_userid');
        if(!isset($is_logged_in)){
            redirect('login', 'refresh');   
        }
    }
    
	function crazy_key()
	{
		$key="fjKJIJLKSJKAJSD"; 
		return $key;
	}

	function to_Encrypt($string, $key="", $url_safe=TRUE)
	{
    	if($key==null || $key=="") $key=crazy_key();
    	$CI =& get_instance();
    	$ret = $CI->encrypt->encode($string, $key);
	    if ($url_safe)
	    {
	        $ret = strtr( $ret, array(
				                    '+' => '.',
				                    '=' => '-',
				                    '/' => '~'
				                )
	            		);
	    }
	    return $ret;
	}
	
	function to_Decrypt($string, $key="")
	{
	    if($key==null || $key=="") $key=crazy_key();
	    $CI =& get_instance();
	    $string = strtr( $string, array(
					                '.' => '+',
					                '-' => '=',
					                '~' => '/'
					            )
	        			);
	    return $CI->encrypt->decode($string, $key);
	}

	function do_upload_single($input = 'img', $path='./uploads/')
	{
		$CI =& get_instance();

		$config = array(
            'upload_path' 		=> $path,
            'max_size' 			=> 1024 * 100,
            'allowed_types' 	=> 'gif|jpeg|jpg|png',
            'overwrite' 		=> true,
            'remove_spaces'	 	=> true,
            'encrypt_name' 		=> true
        );

		$CI->load->library('upload', $config);

		if ( ! $CI->upload->do_upload($input))
		{
				$error = array('error' => $CI->upload->display_errors());
				return $error;
		}
		else
		{
				$data = array('data' => $CI->upload->data());
				return $data;
		}
	}


    
	function resize_image($path, $filename, $dir)
    {
    	$CI =& get_instance();
    	$CI->load->library('image_lib');

        $config['image_library'] 	= 'gd2';
        $config['source_image'] 	= $path;
        $config['new_image'] 		= $dir.$filename;
        $config['maintain_ratio'] 	= TRUE;
        $config['width'] 			= 600;
        $config['height'] 			= 320;
        $config['overwrite']		= TRUE;
       
        $CI->image_lib->clear();
       	$CI->image_lib->initialize($config);
       	$CI->image_lib->resize();
    }

    function query_implode($str) {
	    return sprintf("'%s'", $str);
	}


    function get_gabungan($id)
    {

        $ci = &get_instance();
        $ci->load->model('reportmodel', 'report');
        $rs = $ci->report->get_gabung($id);
        return $rs;
    }

	function get_image_top_only($id, $type)
    {

        $ci = &get_instance();
        $ci->load->model('homemodel', 'home');
        $rs = $ci->home->get_image_top_only($id, $type);
        return $rs;
    }

        function get_image_inap_top_only($id, $type)
    {

        $ci = &get_instance();
        $ci->load->model('homemodel', 'modelhome');
        $rs = $ci->modelhome->get_image_inap_top_only($id, $type);
       
        return $rs;
    }



    function rupiah($num)
{
    return 'Rp. ' . number_format(floatval($num), 0, ',', '.');
}

    function hak_akses(){

        $ci =& get_instance();
        $ci->load->model('Authmodel', 'model');
        $url        = $ci->uri->segment(2);
        $d          = $ci->model->cek_akses($ci->session->userdata('kk_level'));
        $array_d    = explode(',', str_replace(" ", "", $d));
        $akses      = 0;
        foreach ($array_d as $key ) {
            if($key == $url) $akses = 1;
        }


        $level = $ci->session->userdata('kk_level');

        if($akses == 0){
            if ($level == 1) {
                    redirect("kasir/dashboard");
                }else if ($level == 2) {
                    redirect("waiter/dashboard");
                }
        }


         
    }

    function autonumber($id_terakhir, $panjang_kode, $panjang_angka) {

        $kode = substr($id_terakhir, 0, $panjang_kode);

        $angka = substr($id_terakhir, $panjang_kode, $panjang_angka);

        $angka_baru = str_repeat("0", $panjang_angka - strlen($angka+1)).($angka+1);
        $id_baru = $kode.$angka_baru;
      
        return $id_baru;
    }

    function helper_log($tipe = "", $str = ""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    else{
        $log_tipe  = 4;
    }
 
    // paramter
    $param['log_user']      = $CI->session->userdata('back_userid');
    $param['log_tipe']      = $log_tipe;
    $param['log_desc']      = $str;
    $param['ip_addr']       = $_SERVER['REMOTE_ADDR'];

    //load model log
    $CI->load->model('m_log');
 
    //save to database
    $CI->m_log->save_log($param);
 
}

 


         
?>
