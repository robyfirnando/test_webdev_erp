<?php

class Authmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_login($email, $password)
    {
        $pass2 = $this->get_password($email);

       

        if ($pass2 != false){
            $pass = $this->encrypt->decode($pass2[0]->password);
            if($password != $pass){
                $pass2 = 'Wrong password';
            }elseif($pass2[0]->status == "non-active"){
                $pass2 = 'Account is not active';
            }elseif($pass2[0]->status == "deleted"){
                $pass2 = 'Your account is deleted';
            }
        }else{
            $pass2 = 'User / Email Not Found';
        }
        return $pass2;
    }

    public function get_password($email)
    {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where("(email='".$email."' OR name='".$email."')");
        $this->db->limit( 1 );
        $pass = false;
        $pass = $this->db->get();
        $pass = $pass->result();
        return $pass;
    }


    public function changepassword($id)
    {
        var_dump($id);
            $password           = $this->input->post("password");
      
            $this->db->where("id", $id)
                     ->set("password", $this->encrypt->encode($password))
                     ->set("modified_at", date("Y-m-d H:i:s"))
                     ->update("login");
        
        return true;
    }

    public function get_by_email($email)
    {

        $this->db->select("id");
        $this->db->from("login");
        $this->db->where("email", $email);
        $query  = $this->db->get();
        return $query->row()->id;
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("login");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

   public function insertfb($data)
    {   
        $id         =$data['id'];
        $picture    =$data['picture'];

        $idd = $this->get_id($id);

        if ($idd != false){
            
        }else{
            $this->db->set("fb_id", $id)
                 ->set("fb_picture", "https://graph.facebook.com/".$id."/picture?type=normal")
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->insert("login");
        }
        
        return true;
    }

    public function insertgoogle($data)
    {   

        $id         =$data['id'];
        $picture    =$data['picture1'];

         $idd = $this->get_id($id);

        if ($idd != false){
            
        }else{
            $this->db->set("google_id", $id)
                 ->set("google_picture",$picture)
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->insert("login");
        }

        return true;
    }

    public function get_id($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('fb_id=',$id);
        $this->db->or_where('google_id=',$id);
        $this->db->limit( 1 );
        $iidd = false;
        $iidd = $this->db->get();
        $iidd = $iidd->result();
        return $iidd;
    }
    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();


        return $akses[0]->hak_akses;
    }

    public function get_picfb(){
        $a = $this->get_id();
        var_dump($a);
        exit();
        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();
    }
}


