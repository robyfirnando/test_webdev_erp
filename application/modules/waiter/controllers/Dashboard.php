<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->library('googlemaps');
        $this->load->helper('url');
        $this->load->model('Dashboardmodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{	
		$status	= array('active', 'non-active');
		$data['include_script'] = '<script src="'. base_url().'assets/backend/js/libs/flot/jquery.flot.min.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/flot/jquery.flot.time.min.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/flot/jquery.flot.resize.min.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/flot/jquery.flot.orderBars.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/flot/jquery.flot.pie.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/flot/curvedLines.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/jquery-knob/jquery.knob.min.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/sparkline/jquery.sparkline.min.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/d3/d3.min.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/d3/d3.v3.js"></script>
									<script src="'. base_url().'assets/backend/js/libs/rickshaw/rickshaw.min.js"></script>
									<script src="'. base_url().'assets/backend/js/core/demo/DemoDashboard.js"></script>';
		
		$data['include_css'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/theme-default/libs/rickshaw/rickshaw.css" />
									<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/theme-default/libs/morris/morris.core.css" />';
		$data['menu_active'] 	= 'dashboard';
		$data['logger'] 		= $this->model->get_all();
		$data['count_user']     = $this->model->count_mind();

		$this->template->load('template', 'dashboard',$data);

	}
	

}

