<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
public function __construct(){
	parent::__construct();
	$this->load->model('Reportmodel','model');
}

	public function index()
	{
		$data['styles'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/jquery.dataTables.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.colVis.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.tableTools.css">';
		$data['scripts'] 	= '<script src="'. base_url().'assets/backend/js/libs/DataTables/jquery.dataTables.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
								<script src="'. base_url().'assets/backend/js/core/demo/DemoTableDynamic.js"></script>';
		$data['data'] = $this->model->get_all();
	
		$this->template->load('template','report/index', $data);
	}



	public function cetak($id){

		ob_start();
        	$salary 				= $this->model->get_by_id($id);
			$data['salary'] 		= $salary[0];
			$data['detail']			= $this->model->get_by_id($id);
			$data['count']    		= $this->model->count($id);
        $this->load->view('print2', $data);
        $html = ob_get_contents();
				ob_end_clean();

		$name = $this->input->post('nama');				
        require_once APPPATH.'third_party/html2pdf/html2pdf.class.php';
        $pdf = new HTML2PDF('P','A4','en');
        $pdf->addFont('cabin', '', 'cabin');
        $pdf->addFont('font','','font');
        $pdf->addFont('cabinb','','cabinb');
        $pdf->WriteHTML($html);
        $pdf->Output('Data Report '.$name.'.pdf', 'D');
	}


}

