<?php
class Ordermodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }


    public function get_max_id(){
        $this->db->select('order.id');
        $this->db->from( 'order' );
        $cnt = $this->db->get();
        $query = $cnt->result();
        if ($cnt->num_rows() > 0) {
            $inc = $query[0]->id;
            $code = str_repeat("0", 3 - strlen(substr($inc, 10)+001)).(substr($inc, 10)+001);
            return $code;
        }else{
             return $code = str_repeat("0", 3 - strlen(0+1)).(0+1);
        }
    }

    public function insert()
    {
        // $status             = ($this->input->post("status") == 'on') ? 'active' : 'non-active' ;
        $table_num          = $this->input->post("table_num");
        $id_ord             = $this->input->post('id_ord');

        $this->db->set("id", $id_ord)
                 ->set("table_num", $table_num)
                 ->set("status", 'active')
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("order");

        $this->session->set_userdata("last_code", $id_ord);
        helper_log("add", $this->session->userdata('back_name')." add order");
        return true;
    }

    public function update($id)
    {
        $table_num          = $this->input->post("table_num");
        $this->db->where("id", $id)
                 ->set("table_num", $table_num)
                 ->set("modified_at", date("Y-m-d H:i:s"))
                 ->set("modified_by", $this->session->userdata('back_userid'))
                 ->update("order");
        
        return true;
    }

    public function tricky_del($id_ord){
        $this->db->where("parent_id", $id_ord);
        $this->db->delete('order_detail');
        return true; 
    }
    public function update_detail($id, $product, $qty)
    {
        
        $this->db->where("id", $id)
                 // ->set("parent_id", $id_ord)
                 ->set("product_id", $product)
                 ->set("qty",$qty)
                 ->update("order_detail");
        // var_dump ($id."\n");
        // die();
        return true;
    }
    
    public function delete($id)
    {
        $this->db->where("id", $id);
        // $this->db->set("status", "deleted");
        // $this->db->update("category");
        $this->db->delete('order');
        return true; 
    }

    public function deletedetail($id)
    {
       $this->db->where('id', $id)
                 ->delete('order_detail');
        return true; 
    }

    public function deletedetail2($id)
    {
       $this->db->where('parent_id', $id)
                 ->delete('order_detail');
        return true; 
    }

    public function get_detail($id){
        $this->db->select("order.*,order_detail.parent_id,order_detail.product_id,order_detail.id as id_detail,order_detail.qty,product.*");
        $this->db->join('order_detail', 'order.id = order_detail.parent_id','left');
        $this->db->join('product', 'product.id = order_detail.product_id','left');
        $this->db->from("order");
        $this->db->where("parent_id", $id);
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_order($id){
        $this->db->select("*");
        $this->db->from("order");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("order");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }


     public function get_product()
    {
        $this->db->select("*");
        $this->db->where("status","ready");
        $this->db->from("product");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function get_all($status)
    {
        $this->db->select("*");
        $this->db->where("status", "active");
        $this->db->where("created_by", $this->session->userdata("back_userid"));
        $this->db->from("order");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_inactive(){
        $this->db->select("*");
        $this->db->where("status","non-active");
        $this->db->where("created_by", $this->session->userdata("back_userid"));
        $this->db->from("order");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function check_email($email)
    {
        $this->db->select("count(0) as cnt");
        $this->db->from("login");
        $this->db->where("email", $email);
        $qry = $this->db->get()->result();
        return $qry ? $qry[0]->cnt:0;
    }

    public function check_email_edit($email, $uri_email)
    {
        $this->db->select("count(0) as cnt");
        $this->db->from("login");
        $this->db->where("email", $email);
        $this->db->where("email !=", $uri_email);
        $qry = $this->db->get()->result();
        return $qry ? $qry[0]->cnt:0;
    }

        public function cek_akses($id_level)
    {   
        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();

        return $akses[0]->hak_akses;
    }

    public function get_level_id($name)
    {
        $this->db->select("id as cnt");
        $this->db->from("levels");
        $this->db->where("nama", $name);
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    
    }

}