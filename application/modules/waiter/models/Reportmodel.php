<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportmodel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function get_all($me){
	    $this->db->select("*");
        $this->db->where("created_by",$me);
        $this->db->from("order");
        $query  = $this->db->get();
        $data   = $query->result();
        // echo '<pre>';
        // var_dump ($data);
        // die();
        return $data;
    }

     public function get_detail($id){
        $this->db->select("order.*,order_detail.parent_id,order_detail.product_id,order_detail.id as id_detail,order_detail.qty,product.*");
        $this->db->join('order_detail', 'order.id = order_detail.parent_id','left');
        $this->db->join('product', 'product.id = order_detail.product_id','left');
        $this->db->from("order");
        $this->db->where("parent_id", $id);
        // $this->db->where("order.created_by", $this->session->userdata('back_userid'));
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

   public function get_gabung($id){
        $this->db->select("order.*,order_detail.parent_id,order_detail.product_id,order_detail.id as id_detail,order_detail.qty,product.*");
        $this->db->join('order_detail', 'order.id = order_detail.parent_id','left');
        $this->db->join('product', 'product.id = order_detail.product_id','left');
        $this->db->from("order");
        $this->db->where("parent_id", $id);
        // $this->db->where("order.created_by", $this->session->userdata('back_userid'));
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }
}
