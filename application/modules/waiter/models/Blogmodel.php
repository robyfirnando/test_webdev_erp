<?php

class Blogmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($pic)
    {
        
        $status             = ($this->input->post("status") == 'on') ? 'active' : 'nonactive' ;
        $title              = $this->input->post("title");
        $write_by           = $this->input->post("write_by");
        $description        = $this->input->post("description");
        $datetime           = $this->input->post("datetime");
        $tags               = $this->input->post("tags");
        $video_url          = $this->input->post("video_url");
        $category           = $this->input->post("category");
        $lat                = $this->input->post("lat");
        $long               = $this->input->post("lang");

        $this->db->set("title", $title)
                 ->set("write_by", $write_by)
                 ->set("description", $description)
                 ->set("datetime", $datetime)
                 ->set("status", $status)
                 ->set("tags", $tags)
                 ->set("video_url", $video_url)
                 ->set("lat", $lat)
                 ->set("long", $long)
                 ->set("category_id", $category)
                 ->set("type", "blog")
                 ->set("slug", "")
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("tb_article");

        $id   = $this->db->insert_id();
        $slug    = strtolower($title.' '.$id);
        $slug    = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug));
        $this->db->where('id', $id);
        $this->db->set('slug', $slug)->update("tb_article");


        foreach ($pic['status'] as $key => $value) {
            if(!empty($id) && $value == 'success')  
            {
                $this->db->set("name", $title );
                $this->db->set("image", 'uploads/blog/'.$pic['data'][$key]['file_name'] );
                $this->db->set("parent_id", $id);
                $this->db->set("type", 'blog');
                $this->db->set("created_at", date("Y-m-d H:i:s"));
                $this->db->set("created_by", $this->session->userdata('back_userid'));
                $this->db->insert("images");
               
            }
        }


        return true;
    }

    public function update($id,$picdata)
    {
        error_reporting(0);
        $img_id             = $this->input->post('image_id');
        $status           = ($this->input->post("status") == 'on') ? 'active' : 'nonactive' ;
        $title              = $this->input->post("title");
        $write_by           = $this->input->post("write_by");
        $description        = $this->input->post("description");
        $datetime           = $this->input->post("datetime");
        $slug               = strtolower($title.' '.$id);
        $slug               = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug));
        $tags               = $this->input->post("tags");
        $video_url          = $this->input->post("video_url");
        $category           = $this->input->post("category");
        $lat                = $this->input->post("lat");
        $long               = $this->input->post("lang");

        

        $this->db->where("id", $id)
                    ->set("title", $title)
                    ->set("write_by", $write_by)
                    ->set("description", $description)
                    ->set("datetime", $datetime)
                    ->set("status", $status)
                    ->set("slug", $slug)
                    ->set("tags", $tags)
                    ->set("video_url", $video_url)
                    ->set("lat",$lat)
                    ->set("long",$long)
                    ->set("category_id", $category)
                    ->set("modified_at", date("Y-m-d H:i:s"))
                    ->set("modified_by", $this->session->userdata('back_userid'))
                    ->update("tb_article");

          if(!empty($img_id))

        {

            foreach ($img_id as $key => $value) {

                if($value != 0)

                {

                    $size =  $_FILES['image'.$value]['size'];

                    if($size > 0)

                    {

                        $pic = do_upload_single('image_'.$value, './uploads/blog/');





                        $this->db->where('id', $value);

                        $this->db->set("name", $title);

                        $this->db->set("image", 'uploads/blog/'.$pic['data']['file_name'] );

                        $this->db->set("modified_at", date("Y-m-d H:i:s"));

                        $this->db->set("modified_by", $this->session->userdata('back_userid'));

                        $this->db->update("images");



                    }

                }

                        

            }

        }



        if($picdata != "")

        {

            foreach ($picdata['status'] as $key => $value) {

                if(!empty($id) && $value == 'success')  

                {   

                $this->db->set("name", $title );

                $this->db->set("image", 'uploads/blog/'.$picdata['data'][$key]['file_name'] );

                $this->db->set("parent_id", $id);

                $this->db->set("type", 'blog');

                $this->db->set("created_at", date("Y-m-d H:i:s"));

                $this->db->set("created_by", $this->session->userdata('back_userid'));

                $this->db->insert("images");

                }



            }

        }


    }

 public function inactive($id){
        $this->db->where("id",$id)
                 ->set("status","nonactive")
                 ->update("tb_article");
        return true;
    }

    public function active($id){
        $this->db->where("id",$id)
                 ->set("status","active")
                 ->update("tb_article");
        return true;
    }
    
    public function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->set("status", "deleted");
        $this->db->update("tb_article");
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("tb_article.*,images.name as name_img,images.image,images.id as id_detail");
        $this->db->from("tb_article");
        $this->db->join('images', 'tb_article.id = images.parent_id','left');
        $this->db->where("tb_article.id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all($status)
    {
        $this->db->select("tb_article.*,category.name as namecategory");
        $cby = $this->session->userdata('front_userid');
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("tb_article.status", $st);
                    $this->db->where("tb_article.created_by", $cby);
                }else{
                    $this->db->or_where("tb_article.status", $st);
                    $this->db->where("tb_article.created_by", $cby);
                }
                $i++;
            }
        }
        $this->db->from("tb_article");
        $this->db->join('category','tb_article.category_id = category.id','left');
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_category()
    {
        $this->db->select("*");
        $this->db->where("type","pariwisata");
        $this->db->from("category");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function position(){
        $this->db->select("*");
        $this->db->where("lat");
        $this->db->from("tb_article");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function delete_detail($id)
    {
       $this->db->where('id', $id)
                 ->delete('images');
        return true; 
    }

    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();


        return $akses[0]->hak_akses;
    }




}