<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/backend/css/report.css">
</head>
<body>
	<img src="<?php echo base_url(); ?>assets/backend/img/logo.png">
	<div class="head" style="margin-left: 200px">Daily Report</div><br>
	Printed at: <?php echo date('d M,Y');?>
	<hr>
<?php 
	foreach ($data as $c => $data) :
		// echo '<pre>';
		// var_dump ($data);
		// die();
?>
<br><br><br>
<table>
	<tr>
		<td>ID Order</td><td>:</td><td><?php echo $data->id;?></td>
	</tr>
	<tr>
		<td>Tables' Number</td><td>:</td><td><?php echo $data->table_num;?></td>
	</tr>
	<tr>
		<td>Order At</td><td>:</td><td><?php echo $data->created_at;?></td>
	</tr>
</table>

<table class="a tabel2">
	<tbody>
	<tr class="descval">
	<td class="desc" valign="middle">&nbsp;Product</td>
	<td class="tb_num" valign="middle">&nbsp;Price</td>
	<td class="tb_num" valign="middle">&nbsp;Quantity</td>
	</tr>
	    <?php foreach(get_gabungan($data->id) as $key => $val): 
	    // var_dump ($val);
	    // die();
	    ?>
		<tr class="nameval">
				<td id="list">	
					<?php echo $val->name; ?>
				</td>

				<td>	
					<?php echo $val->price; ?>
				</td>

				<td>	
					<?php echo $val->qty; ?>
				</td>

		</tr>
	
 	<?php 
 	 endforeach; ?>
	</tbody>

</table>
 <?php endforeach; ?>
 <div style="text-align: right;margin-top:200px;">
 	Hormat Saya<br><br><br><br><br><br><?php echo $this->session->userdata('back_name');?>
 </div>

  <div style="text-align: center;margin-top:200px;">
  <hr>
  Contact : Lorem@ipsum.leet</div>
</body>
</html>