<div id="content">
	<section class="style-default-bright">
		<div class="section-header">
			<h2 class="text-primary">Listing data orders</h2>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-md-8">
					<article class="margin-bottom-xxl">
						<p class="lead">
							Listing data orders
						</p>
					</article>
				</div>
			</div>
			<div class="row">
				<?php if(isset($message)): ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $message ?>
					</div>
				<?php endif; ?>
				<?php if(isset($success_message)): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $success_message ?>
					</div>
				<?php endif; ?>
				<div class="col-lg-12">
					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="sort-numeric">No</th>
									<th>ID ORDER</th>
									<th>Table Number</th>
									<th>Order At</th>
									<th>Detail</th>

								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($data as $data):
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $data->parent_id; ?>
									<td><?php echo $data->table_num; ?></td>
									</td>
									<td><?php echo $data->created_at; ?></td>
									<td><a class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
								</tr>
								<?php $no++; endforeach;?>
							</tbody>
						</table>
					</div><!--end .table-responsive -->
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
		
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delete', function(){

			var del_url =  $(this).attr('href');

			bootbox.confirm("<h4>Anda yakin ingin menghapus ?</h4>", function (result) {
		        if (result) {
		           location.href = del_url;
		        }
	    	});
	   	 	return false;
		})
	})
</script>