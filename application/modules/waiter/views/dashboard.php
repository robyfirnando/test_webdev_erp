	<!-- BEGIN ELFINDER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/jquery-ui/css/base/jquery-ui.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/elfinder/css/theme.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/elfinder/css/elfinder.min.css'); ?>" />
    <script type="text/javascript" src="<?php echo base_url('assets/jquery-1.7.2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/jquery-ui/js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/elfinder/js/elfinder.min.js'); ?>"></script>
     

    <!-- END ELFINDER -->

<div id="content">
	<section>
		<div class="section-body">
			<div class="row">
				
				<!-- BEGIN ALERT - TIME ON SITE -->
				<div class="col-md-6 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-success no-margin">
								<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
								<strong class="text-xl">54 sec.</strong><br>
								<span class="opacity-50">Avg. time on site</span><br>
								<span><?php echo "IP Address: ".$this->input->ip_address(); ?></span>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - TIME ON SITE -->
				
				<!-- BEGIN ALERT - USERS -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-success no-margin">
								<h1 class="pull-right text-success"><i class="md md-count"></i></h1>
								<strong class="text-xl">Total Order</strong><br>
								<span class="count" style="font-size:25px;"><?php 
								echo count($count_user); ?></span><br>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - TIME ON SITE -->

		
			</div><!--end .row -->
			<div class="row">
				
				<!-- BEGIN SITE ACTIVITY -->
				<div class="col-md-9">
				<div class="card-head">
					<header>Welcome <?php 
								echo $this->encrypt->encode('erpwaiter123');

					echo $this->session->userdata('back_name'); ?></header>
				</div><!--end .card-head -->
					<!-- <div id="elfinder-tag"></div> -->
				</div>
				<!-- END SITE ACTIVITY -->
				
				<!-- BEGIN SERVER STATUS -->
				<div class="col-md-3">
					<div class="card">
						
					
					</div><!--end .card -->		
				</div><!--end .col -->
				<!-- END SERVER STATUS -->
				
			</div><!--end .row -->
			<div class="row">
				
				
				<!-- BEGIN REGISTRATION HISTORY -->
				<div class="col-md-12">
					<div class="card">
						<div class="card-head">
							<header>History</header>
							<div class="tools">
								<a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
								<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="card">
									<div class="card-body">

									<table class="table">
									<thead>
										<tr>
											<th>Activity at</th>
											<th>IP Addr</th>
											<th>Desc</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($logger as $log) :
										?>
										<tr>
											<td><?php echo $log->log_time; ?></td>
											<td><?php echo $log->ip_addr; ?></td>
											<td><?php echo $log->log_desc; ?></td>

										</tr>	
										<?php endforeach; ?>
									</tbody>
								</table>
								</div><!--end .row -->
							</div>
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END REGISTRATION HISTORY -->
				

		

				
			</div><!--end .row -->
		</div><!--end .section-body -->
	</section>
</div><!--end #content-->

	<script type="text/javascript">
		$('.count').each(function () {
		    $(this).prop('Counter',0).animate({
		        Counter: $(this).text()
		    }, {
		        duration: 1000,
		        easing: 'swing',
		        step: function (now) {
		            $(this).text(Math.ceil(now));
		        }
		    });
		});
	</script>