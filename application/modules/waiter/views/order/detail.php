<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form">
						<div class="card">
							<div class="card-head style-primary">
								<header>Form detail <?php echo $myparent; ?></header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input readonly type="text" class="form-control" id="name" name="id_order" required data-rule-minlength="2" value="<?php echo $myparent; ?>">
									<label for="name">ID Order</label>
								</div>
								<?php 
									if (empty(count($single))) :
								?>
								<div class="form-group">
									<input readonly type="text" class="form-control" id="name" name="table_num" required data-rule-minlength="2" value="<?php echo $get_order[0]->table_num;?>">
									<label for="name">Table's Number</label>
								</div>
								<?php else : ?>
								<div class="form-group">
									<input readonly type="text" class="form-control" id="name" name="table_num" required data-rule-minlength="2" value="<?php echo $single[0]->table_num; ?>">
									<label for="name">Table's Number</label>
								</div>
								<?php endif;?>
								<table class="table table-bordered" width="50%" border="0" align="center" style="text-align: center;" id="table2">
									<tr>
										<td width="600px"><label>Product (Menu Ready) </label></td>
										<td><label>Price</label></td>
										<td><label>Quantity</label></td>
										<td><label>Action</label></td>

									</tr>
								<?php 
								if (!empty(count($single))) :
									foreach ($single as $data) :
									
								?>
									<tr class="tile-<?php echo $data->id_detail; ?>">
										<td><select class="form-control e1" name="product[]">
											<?php foreach($products as $prod){ ?>
											<option value="<?php echo $prod->id; ?>" <?php echo $prod->id == $data->id ? 'selected="selected"' : '' ?>><?php echo $prod->name;?></option><?php } ?>
											</select>
										</td>
										<td><?php echo rupiah($data->price); ?></td>
										<input type="hidden" name="id_detail[]" value="<?php echo $data->id_detail; ?>">
										<td>
											<input type="number" name="qty[]" class="form-control" value="<?php echo $data->qty; ?>"> 
										</td>

										<td><input type="button" class="btn btn-danger delete_detail" value="Remove" name="delete" detail_id="<?php echo $data->id_detail; ?>"></td>
									</tr>
								
								<?php 
								endforeach;
									else :
										echo '';
									endif;
								?>
								</table>									
								<input type="button" class="btn btn-info btn-sm" value="Add Order" id="addrow2" style="margin:45px;"/>
							
							<div class="form-group" style="font-size:20px;">
								<b>Total Payment : </b><br>
								<?php 

								if (empty(count($single))) {
									echo "0";
								}else{
									foreach ($single as $value) {

									  $new_times[] = $value->price * $value->qty;

									}
									echo rupiah(array_sum($new_times));
								}
								?>

							</div>
							
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
					</form>
				</div>
						<em class="text-caption">Form create new category</em>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'waiter/order/' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#addrow2').click(function() {
		$("#table2 > tbody:last").append('<tr>\n\
		<td colspane="3"> <select class="form-control e1" name="product[]">'+'<?php foreach($products as $prod){ ?>'+'<option value='+'<?php echo $prod->id;?>'+'>'+'<?php echo $prod->name;?>'+'</option>'+'<?php } ?>'+'</select></td>\n\
		<td><input type="number" name="qty[]" class="form-control"></td><td><input type="hidden" class="form-control" name="id_riwayat_pendidikan[]"><input type="button" class="btn btn-danger" value="Remove" name="delete" onClick="$(this).closest(\'tr\').remove();"></td></tr>');

		});
	});

		$(document).on('click', '.delete_detail', function(){

    	var url 		= "<?php echo base_url('waiter/order/deletedetail'); ?>"
    	var detail_id 	= $(this).attr('detail_id');
    	var element 	= $(".tile-"+detail_id);
    	$.ajax({
	        url: url,
	        type: "GET",
	        data: 'detail_id='+detail_id,
	       	dataType : "JSON",
	        contentType: false,
	        cache: false,
	        processData:false,
	        beforeSend: function()
	        {

	        },
	        success: function(data)
	        {
	        	console.log(data.status);
	        	if(data.status == true){
	        		element.parent().remove();
	        	}else{
	        		alert('data gagal dihapus');
	        	}

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	              alert('Error adding / update data');
	        }         
	      });


    	return false;


    })

</script>



