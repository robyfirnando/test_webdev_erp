<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Food extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Foodmodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{
		$status	= array('active', 'nonactive');
        $data['success_message'] = $this->session->flashdata('success_message');
		
		$data['styles'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/jquery.dataTables.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.colVis.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.tableTools.css">';
		$data['scripts'] 	= '<script src="'. base_url().'assets/backend/js/libs/DataTables/jquery.dataTables.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
								<script src="'. base_url().'assets/backend/js/core/demo/DemoTableDynamic.js"></script>';
								
		$data['product']		= $this->model->get_all($status);
		$data['title']		= 'Members';

		$this->template->load('template', 'food/index', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		if ( $this->form_validation->run() == true){

			$this->model->insert();
            $this->session->set_flashdata('success_message', "Add food success");
            redirect("kasir/food", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['title']		= 'Add Ads';
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   	   <script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   	   <script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			
			$this->template->load('template', 'food/add', $data);
		}
	}

	public function edit($id)
	{
		$status	= array('ready', 'not-ready','deleted');
		$this->form_validation->set_rules('name', 'Name', 'trim|required');

		if ( $this->form_validation->run() == true){

			$this->model->update(to_Decrypt($id));
            $this->session->set_flashdata('success_message', "Update food success");
            redirect("kasir/food", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$single 			= $this->model->get_by_id(to_Decrypt($id));
			$data['single'] 	= $single[0];
			// echo '<pre>';
			// var_dump ($data['single']);
			// die();
			$data['title']		= 'Edit Ads';
			$data['ads']		= $this->model->get_all($status);

			$this->template->load('template', 'food/edit', $data);
		}
	}

	public function delete($id)
	{
		$query = $this->model->delete(to_Decrypt($id));
		if($query){
			$this->session->set_flashdata('success_message', "Delete food success");
			redirect("kasir/food", 'refresh');
		}else{
			$this->session->set_flashdata('message', "Delete admins error");
			redirect("kasir/food/", 'refresh');
		}
	}

	

}
	