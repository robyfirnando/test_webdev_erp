<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waiter extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Waitermodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{
		$status	= array('active', 'non-active');
        $data['success_message'] = $this->session->flashdata('success_message');
		
		$data['styles'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/jquery.dataTables.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.colVis.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.tableTools.css">';
		$data['scripts'] 	= '<script src="'. base_url().'assets/backend/js/libs/DataTables/jquery.dataTables.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
								<script src="'. base_url().'assets/backend/js/core/demo/DemoTableDynamic.js"></script>';
								
		$data['waiters']		= $this->model->get_all($status);
		$data['title']		= 'Waiter';

		$this->template->load('template', 'waiter/index', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ( $this->form_validation->run() == true){

			$this->model->insert();
            $this->session->set_flashdata('success_message', "Add waiters success");
            redirect("kasir/waiter", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['title']		= 'Add Waiter';
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			
			$this->template->load('template', 'waiter/add', $data);
		}
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');

		if ( $this->form_validation->run() == true){

			$this->model->update(to_Decrypt($id));
            $this->session->set_flashdata('success_message', "Update waiter success");
            redirect("kasir/waiter", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$single 			= $this->model->get_by_id(to_Decrypt($id));
			$data['single'] 	= $single[0];
			$data['title']		= 'Edit Waiter';

			$this->template->load('template', 'waiter/edit', $data);
		}
	}

	public function delete($id)
	{
		$query = $this->model->delete(to_Decrypt($id));
		if($query){
			$this->session->set_flashdata('success_message', "Delete waiter success");
			redirect("kasir/waiter", 'refresh');
		}else{
			$this->session->set_flashdata('message', "Delete waiter error");
			redirect("kasir/waiter", 'refresh');
		}	}

	public function check_email_waiter()
	{	
		$data 	= array();
		$email 	= $this->input->post('email');
		$valid 	=  $this->model->check_email($email);
		if($valid > 0){
			$data['status'] 	= FALSE;
			$data['message'] 	= "Email already use, please change the email";
		}else{
			$data['status']		= TRUE;
			$data['message']	= "Email can be use";
		}
		echo json_encode($data);
	}

	public function check_email_waiter_edit()
	{
		$data 		= array();
		$email 		= $this->input->post('email');
		$uri_email 	= $this->input->post('uri_email');
		$valid 		=  $this->model->check_email_edit($email, $uri_email);
		if($valid > 0){
			$data['status'] 	= FALSE;
			$data['message'] 	= "Email already use, please change the email";
		}else{
			$data['status']		= TRUE;
			$data['message']	= "Email can be use";
		}
		echo json_encode($data);
	}

	

}
	