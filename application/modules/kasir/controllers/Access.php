 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Accessmodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{
		
		$data['success_message'] = $this->session->flashdata('success_message');
		
		$data['styles'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/jquery.dataTables.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.colVis.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.tableTools.css">';
		$data['scripts'] 	= '<script src="'. base_url().'assets/backend/js/libs/DataTables/jquery.dataTables.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
								<script src="'. base_url().'assets/backend/js/core/demo/DemoTableDynamic.js"></script>';
								
		$data['aksess']		= $this->model->get_all();
		$data['title']		= 'Access';

		$this->template->load('template', 'access/index', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('nama', 'Name', 'trim|required');

		if ( $this->form_validation->run() == true){

			$this->model->insert();
            $this->session->set_flashdata('success_message', "Add Access success");
            redirect("backend/access", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['title']		= 'Add Access';
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$data['view'] 		= $this->model->levels();
			
			$this->template->load('template', 'access/add', $data);
		}
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('nama', 'Name', 'trim|required');

		if ( $this->form_validation->run() == true){

			$this->model->update(to_Decrypt($id));
            $this->session->set_flashdata('success_message', "Edit Access success");
            redirect("backend/access", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$akses 				= $this->model->get_by_id(to_Decrypt($id));
			$data['akses'] 		= $akses[0];
			$data['title']		= 'Edit Access';
			$data['view'] 		= $this->model->levels();

			$this->template->load('template', 'access/edit', $data);
		}
	}

	public function delete($id)
	{
		$query = $this->model->delete(to_Decrypt($id));
		if($query){
			$this->session->set_flashdata('success_message', "Delete Access success");
			redirect("backend/access", 'refresh');
		}else{
			$this->session->set_flashdata('message', "Delete Access error");
			redirect("backend/access", 'refresh');
		}
	}



}
	