<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Ordermodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{
		$status	= array('active', 'non-active');
        $data['success_message'] = $this->session->flashdata('success_message');
		$data['styles'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/jquery.dataTables.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.colVis.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.tableTools.css">';
		$data['scripts'] 	= '<script src="'. base_url().'assets/backend/js/libs/DataTables/jquery.dataTables.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
								<script src="'. base_url().'assets/backend/js/core/demo/DemoTableDynamic.js"></script>';
								
		$data['orders']		= $this->model->get_all($status);
		$data['title']		= 'Order';
		$data['status'] 	= 'active';

		$this->template->load('template', 'order/index', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('table_num', 'Table Number', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		if ( $this->form_validation->run() == true){

			  $this->model->insert();
			  $id_det = $this->session->userdata('last_code');
			  $product = $this->input->post('product');
			  $qty = $this->input->post('qty');
			  // $product = $this->input->post('product');
	     	  $result = array();
	     	 
		     foreach($product as $key => $val){
				      $result[] = array(
					   "parent_id"   => $id_det,
				       "product_id"  => $_POST['product'][$key],
				       "qty"  	 => $_POST['qty'][$key],
				      );
		    	 }


				$test= $this->db->insert_batch('order_detail', $result); // fungsi dari codeigniter untuk menyimpan multi array
			$this->session->unset_userdata("last_code");
            $this->session->set_flashdata('success_message', "Add order success");
            redirect("kasir/order", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['title']		= 'Add category';
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
   			$data['get_max'] 			    = $this->model->get_max_id();
			$data['products']	= $this->model->get_product();

			$this->template->load('template', 'order/add', $data);
		}
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('table_num', 'Table Number', 'trim|required');
		if ( $this->form_validation->run() == true){

			$this->model->update(to_Decrypt($id));
            $this->session->set_flashdata('success_message', "Update Order success");
            redirect("kasir/order", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$order 				= $this->model->get_by_id(to_Decrypt($id));
			// $data['order'] 		= $order[0];
			$data['title']		= 'Edit Order';

			$single 			= $this->model->get_by_id(to_Decrypt($id));
			$data['single']		= $single[0];
			$data['myparent'] 	=  to_Decrypt($id);
			$data['products']	= $this->model->get_product();

			$this->template->load('template', 'order/edit', $data);
		}
	}

	public function detail($id)
	{
		if ($_POST) {
		$id_ord = $this->input->post('id_order');

		$query = $product = $this->input->post('product');
				 $qty = $this->input->post('qty');
				 $id_detail = $this->input->post('id_detail');

		$check		= $this->model->get_detail(to_Decrypt($id));
		if (count($product) > count($check)) {

			$this->model->tricky_del($id_ord);
			  $id_det = $this->input->post('id_order');
			  $productid = $this->input->post('product');
			  $qty = $this->input->post('qty');
	     	  $result = array();
	     	 
		     foreach($productid as $key => $val){
				      $result[] = array(
					   "parent_id"   => $id_det,
				       "product_id"  => $_POST['product'][$key],
				       "qty"  	 => $_POST['qty'][$key],
				      );
		    	 }


			$test= $this->db->insert_batch('order_detail', $result); 
		}else{
					foreach ($query as $key => $value) {
						
						$this->model->update_detail($id_detail[$key], $product[$key], $qty[$key]);
					};
		
			 

		}
			if($query){
				$this->session->set_flashdata('success_message', "Edit Order Detail Success");
            	// redirect("waiter/order", 'refresh');
			}	
		}
		$getdet 			= $this->model->get_detail(to_Decrypt($id));
		$data['myparent'] 	=  to_Decrypt($id);
		$data['single'] 	= $getdet;
		$data['products']	= $this->model->get_product();

		$this->template->load('template', 'order/detail', $data);
	}

	public function delete($id)
	{
		$query = $this->model->delete(to_Decrypt($id));
		if($query){
			$this->session->set_flashdata('success_message', "Delete order success");
			redirect("kasir/order", 'refresh');
		}else{
			$this->session->set_flashdata('message', "Delete order error");
			redirect("kasir/order", 'refresh');
		}	
	}

	public function order_inactive(){
        $data['success_message'] = $this->session->flashdata('success_message');
		
		$data['orders']		= $this->model->get_inactive();
		$data['status'] 	= 'inactive';
		$data['title']		= 'Order Nonactive';

		$this->template->load('template', 'order/index', $data);
	}

	public function deletedetail()
	{
		$detail_id 	 = $this->input->get('detail_id');
		$del 		 = $this->model->deletedetail($detail_id);

		if($del){
			$data['status'] = true;
		}else{
			$data['status'] = false;
		}
		echo json_encode($data);

	}

	
}
	