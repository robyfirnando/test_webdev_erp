<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Admin Panel - <?php echo isset($title) ? $title : 'Dashboard'; ?></title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/bootstrap.css?1422792965" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/materialadmin.css?1425466319" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/font-awesome.min.css?1422529194" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/material-design-iconic-font.min.css?1421434286" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/libs/rickshaw/rickshaw.css?1422792967" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/libs/morris/morris.core.css?1420463396" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/backend/css/style.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.css">
		<?php echo isset($styles) ? $styles : '' ?>
		<!-- END STYLESHEETS -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/js/libs/utils/html5shiv.js?1403934957"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/backend/js/libs/utils/respond.min.js?1403934956"></script>
		<![endif]-->
		
		<script src="<?php echo base_url() ?>assets/backend/js/libs/jquery/jquery-1.11.2.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/bootstrap/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/spin.js/spin.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/autosize/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/moment/moment.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/flot/jquery.flot.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/flot/jquery.flot.time.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/flot/jquery.flot.resize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/flot/jquery.flot.orderBars.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/flot/jquery.flot.pie.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/flot/curvedLines.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/jquery-knob/jquery.knob.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/sparkline/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/d3/d3.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/d3/d3.v3.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/libs/rickshaw/rickshaw.min.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/App.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/AppNavigation.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/AppOffcanvas.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/AppCard.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/AppForm.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/AppNavSearch.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/source/AppVendor.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/core/demo/Demo.js"></script>
		<script src="<?php echo base_url() ?>assets/backend/js/bootbox.min.js"></script>
		<script src="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>	
  		<script src="<?php echo base_url(); ?>/assets/backend/js/libs/ckeditor/ckeditor.js"></script>
		<script src="<?php echo base_url(); ?>/assets/backend/js/libs/ckeditor/adapters/jquery.js"></script>
		
		<!-- datetime -->
		 <script src="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		 <script src="<?php echo base_url();?>assets/backend/js/jquery.datetimepicker.full.js"></script>
		 <script src="<?php echo base_url();?>assets/backend/js/jquery.js"></script>
		 <script src="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

		<?php echo isset($scripts) ? $scripts : '' ?>
		
		<!-- <script src="<?php echo base_url() ?>assets/backend/js/core/demo/DemoDashboard.js"></script> -->
		
		<script type="text/javascript">
			var url = '<?php echo base_url(); ?>';
		</script>
		
	</head>
	<body class="menubar-hoverable header-fixed ">
		<!-- BEGIN HEADER-->
		<header id="header" >
			<div class="headerbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="headerbar-left">
					<ul class="header-nav header-nav-options">
						<li class="header-nav-brand" >
							<div class="brand-holder">
								<a href="<?php echo base_url() ?>/kasir/dashboard">
									<span class="text-lg text-bold text-primary">Admin Panel</span>
								</a>
							</div>
						</li>
						<li>
							<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
								<i class="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="headerbar-right">
				
					<ul class="header-nav header-nav-profile">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
								<img src="<?php echo base_url() ?>assets/backend/img/avatar1.jpg?1403934956" alt="" />
								<span class="profile-info">
									<?php echo $this->session->userdata('back_name'); ?>
									<small>Administrator</small>
								</span>
							</a>
							<ul class="dropdown-menu animation-dock">
								<li class="divider"></li>
								<li><a href="<?php echo base_url() ?>html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>
								<li><a href="<?php echo base_url().'auth/logout' ?>"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
							</ul><!--end .dropdown-menu -->
						</li><!--end .dropdown -->
					</ul><!--end .header-nav-profile -->
				</div><!--end #header-navbar-collapse -->
			</div>
		</header>
		<!-- END HEADER-->

		<!-- BEGIN BASE-->
		<div id="base">

			<!-- BEGIN CONTENT-->
			<?php echo $contents ?>
			<!-- END CONTENT -->

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="<?php echo base_url() ?>/backend/dashboard">
							<span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
						</a>
					</div>
				</div>
				<div class="menubar-scroll-panel">

					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">
					
						<!-- BEGIN DASHBOARD -->
						<li>
							<a href="<?php echo base_url() ?>kasir/dashboard" class="active">
								<div class="gui-icon"><i class="md md-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END DASHBOARD -->
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-dashboard"></i></div>
								<span class="title">Login</span>
							</a>
							<!--start submenu -->
						
								<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url() ?>kasir/levels" ><span class="title">Levels</span></a></li>
							</ul><!--end /submenu -->
							<ul>
								<li><a href="<?php echo base_url() ?>kasir/access" ><span class="title">Access</span></a></li>
							</ul><!--end /submenu -->

						</li>

						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="fa fa-book"></i></div>
								<span class="title">Product</span>
							</a>
							<!--start submenu -->
							<ul>
								
								<li><a href="<?php echo base_url() ?>kasir/food" ><span class="title">Foods</span></a></li>
							</ul>
							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url() ?>kasir/drink" ><span class="title">Drinks</span></a></li>
							</ul><!--end /submenu -->
						</li>


						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="fa fa-shopping-cart"></i></div>
								<span class="title">Orders</span>
							</a>
							<!--start submenu -->
							<ul>
								
								<li><a href="<?php echo base_url() ?>kasir/order" ><span class="title">Orders</span></a></li>
							</ul>
							<!--start submenu -->
							<ul>
								<li><a href="<?php echo base_url() ?>kasir/order/order_inactive" ><span class="title">Non-active Orders</span></a></li>
							</ul><!--end /submenu -->
						</li>

						<li>
							<a href="<?php echo base_url() ?>kasir/waiter">
								<div class="gui-icon"><i class="fa fa-users"></i></div>
								<span class="title">Waiters</span>
							</a>
						</li><!--end /menu-li -->


						<!--start submenu -->
				
					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span style="font-size:13px;">Copyright &copy; 2018</span> <strong style="font-size:13px;">0x1337</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->

		</div><!--end #base-->
		<!-- END BASE -->

		<!-- BEGIN JAVASCRIPT -->
		
		<!-- END JAVASCRIPT -->
		
	</body>
</html>
