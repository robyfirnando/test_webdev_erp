	<!-- BEGIN ELFINDER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/jquery-ui/css/base/jquery-ui.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/elfinder/css/theme.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/elfinder/css/elfinder.min.css'); ?>" />
    <script type="text/javascript" src="<?php echo base_url('assets/jquery-1.7.2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/jquery-ui/js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/elfinder/js/elfinder.min.js'); ?>"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
     <!-- select2-search-choice -->
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#elfinder-tag').elfinder({
                url: '<?php echo site_url('backend/dashboard/elfinder'); ?>',
            }).elfinder('instance');
        });
    </script>
    <!-- END ELFINDER -->
    <style type="text/css">
    	.shadow1 {
			  margin: 40px;
			  background-color: rgb(68,68,68); /* Needed for IEs */

			  -moz-box-shadow: 5px 5px 5px rgba(68,68,68,0.6);
			  -webkit-box-shadow: 5px 5px 5px rgba(68,68,68,0.6);
			  box-shadow: 5px 5px 5px rgba(68,68,68,0.6);

			  filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30);
			  -ms-filter: "progid:DXImageTransform.Microsoft.Blur(PixelRadius=3,MakeShadow=true,ShadowOpacity=0.30)";
			  zoom: 1;
			}
		.shadow1 .content {
			  position: relative; /* This protects the inner element from being blurred */
			  padding: 100px;
			  background-color: #DDD;
}
    </style>

<div id="content">
	<section>
		<div class="section-body">
			<div class="row">
				
				<!-- BEGIN ALERT - TIME ON SITE -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-success no-margin">
								<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
								<strong class="text-xl">54 sec.</strong><br>
								<span class="opacity-50">Avg. time on site</span><br>
								<span><?php echo "IP Address: ".$this->input->ip_address(); ?></span>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - TIME ON SITE -->
				
				<!-- BEGIN ALERT - USERS -->
				<div class="col-md-3 col-sm-6">
				
				</div><!--end .col -->
				<!-- END ALERT - TIME ON SITE -->

				<!-- BEGIN ALERT - PARIWISATA -->
				<div class="col-md-3 col-sm-6">
			
				</div><!--end .col -->
				<!-- END ALERT - PARIWISATA -->

				<!-- BEGIN ALERT - PARIWISATA -->
				<div class="col-md-3 col-sm-6">
					
				</div><!--end .col -->
				<!-- END ALERT - PARIWISATA -->

			</div><!--end .row -->
			<div class="row">
				
				<!-- BEGIN SITE ACTIVITY -->
				<div class="col-md-9">
				<div class="card-head">
				</div><!--end .card-head -->
					<!-- <div id="elfinder-tag"></div> -->
				</div>
				<!-- END SITE ACTIVITY -->
				
			
				
			</div><!--end .row -->
			<div class="row">
			
				
				<!-- BEGIN REGISTRATION HISTORY -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-head">
							<header>Your History</header>
							<div class="tools">
								
							</div>
						</div><!--end .card-head -->
							<div class="card">
									<div class="card-body">

									<table class="table">
									<thead>
										<tr>
											<th>Activity at</th>
											<th>IP Addr</th>
											<th>Desc</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($logger as $log) :
										?>
										<tr>
											<td><?php echo $log->log_time; ?></td>
											<td><?php echo $log->ip_addr; ?></td>
											<td><?php echo $log->log_desc; ?></td>

										</tr>	
										<?php endforeach; ?>
									</tbody>
								</table>
								</div><!--end .row -->
							</div>
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END REGISTRATION HISTORY -->
				
				<!-- BEGIN TODOS -->
				<div class="col-md-3">
					
				</div><!--end .col -->
				<!-- END TODOS -->

				<!-- BEGIN REGISTRATION BLOG -->
				<div class="col-md-6">
					
				<div class="col-md-6">
					
				</div><!--end .col -->
				<!-- END REGISTRATION BLOG -->
				
			</div><!--end .row -->
		</div><!--end .section-body -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/libs/typeahead/typeahead.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/libs/typeahead/typeahead.jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/libs/typeahead/typeahead.bundle.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/libs/typeahead/bloodhound.js"></script>
			<!-- BEGIN REGISTRATION BLOG -->
				<div class="col-md-12">
				</div>
				<!-- END REGISTRATION BLOG -->
			<!-- 	<div class="col-md-12">
					<div class="card">
						<div class="card-head">
						 <div id="map_two"></div>
						    <div id="right-panel">
						      <h2>Results</h2>
						      <ul id="places"></ul>
						      <button id="more">More results</button>
						    </div>
						</div>
					</div>
				</div> -->
	</section>
</div><!--end #content-->



