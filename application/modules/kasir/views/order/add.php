<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form">
						<div class="card">
							<div class="card-head style-primary">
								<header>Form create new order</header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="id_ord" required data-rule-minlength="2" value="<?php echo 'ERP'.date('dmy').'-'.$get_max?>" readonly>
									<label for="name">ID Order</label>
								</div>
								<div class="form-group">
									<input type="number" class="form-control" id="name" name="table_num" required data-rule-minlength="2" value="<?php echo isset($_POST['table_num']) ? $_POST['table_num'] : ''; ?>">
									<label for="name">Table's Number</label>
								</div>
								
								<table class="table table-bordered" width="50%" border="0" align="center" id="table2">
									<tr>
											<td width="800px"><label>Product (Menu Ready) </label></td>
											<td><label>Quantity</label></td>
											<td><label>Action</label></td>
									</tr>
									<tr>
									</tr>
								</table>
								<input type="button" class="btn btn-info btn-sm" value="Add Order" id="addrow2" style="margin:45px;"/>

								<div class="form-group">
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" name="status">
											<span>Active</span>
										</label>
									</div>
								</div>
								<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
								</div>
							</div>
						</div>
						<em class="text-caption">Form create new order</em>
					</form>
				</div>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/category' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>


<script type="text/javascript">
    // jQuery(function($) {
	$(document).ready(function(){
		$('#addrow2').click(function() {
		$("#table2 > tbody:last").append('<tr>\n\
		<td colspane="3"> <select class="form-control e1" name="product[]">'+'<?php foreach($products as $prod){ ?>'+'<option value='+'<?php echo $prod->id;?>'+'>'+'<?php echo $prod->name;?>'+'</option>'+'<?php } ?>'+'</select></td>\n\
		<td><input type="number" name="qty[]" class="form-control"></td><td><input type="hidden" class="form-control" name="id_riwayat_pendidikan[]"><input type="button" class="btn btn-danger" value="Remove" name="delete" onClick="$(this).closest(\'tr\').remove();"></td></tr>');


		// 	$(function(){
		//     $('.e1').change(function(){
		//         if($(this).attr('class') == 'e1' && $(this).val() == 'Default'){
		//             $('.e1').not(this).prop('disabled', true).val('Disabled');
		//         } else {
		//             $('.e1').not(this).removeProp('disabled');
		//             $('.e1 option').removeProp('disabled');
		//             $('.e1').each(function(){
		//                 var val = $(this).val();
		//                 if(val != 'Default' || val != 'Disabled'){
		//                     $('.e1 option[value="'+val+'"]').not(this).prop('disabled', true);
		//                 }
		//             });
		//         }
		//     });
		// });



		});
	});


	// });

</script>