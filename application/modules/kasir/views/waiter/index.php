<div id="content">
	<section class="style-default-bright">
		<div class="section-header">
			<h2 class="text-primary">Listing data waiters</h2>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-md-8">
					<article class="margin-bottom-xxl">
						<p class="lead">
							Listing data waiters
						</p>
					</article>
				</div>
			</div>
			<div class="row">
				<?php if(isset($message)): ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $message ?>
					</div>
				<?php endif; ?>
				<?php if(isset($success_message)): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $success_message ?>
					</div>
				<?php endif; ?>
				<div class="col-lg-12">
					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="sort-numeric">No</th>
									<th>Name</th>
									<th>Email</th>
									<th class="sort-numeric">Phone</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($waiters as $admin):
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $admin->name; ?></td>
									<td><?php echo $admin->email; ?></td>
									<td><?php echo $admin->phone; ?></td>
									<td><?php echo ucwords(str_replace("-", " ", $admin->status)); ?></td>
									<td>
										<a href="<?php echo base_url().'kasir/waiter/edit/'.to_Encrypt($admin->id); ?>" class="btn ink-reaction btn-flat btn-primary active" data-toggle="tooltip" data-placement="top" data-original-title="Edit <?php echo $admin->name; ?>">
											<i class="md md-mode-edit"></i>
										</a>
										<a href="<?php echo base_url().'kasir/waiter/delete/'.to_Encrypt($admin->id); ?>" class="btn ink-reaction btn-primary delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete <?php echo $admin->name; ?>">
											<i class="md md-delete"></i>
										</a>
									</td>
								</tr>
								<?php $no++; endforeach;?>
							</tbody>
						</table>
					</div><!--end .table-responsive -->
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'kasir/waiter/add' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Add Waiters">
					<i class="md md-add"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delete', function(){

			var del_url =  $(this).attr('href');

			bootbox.confirm("<h4>Anda yakin ingin menghapus ?</h4>", function (result) {
		        if (result) {
		           location.href = del_url;
		        }
	    	});
	   	 	return false;
		})
	})
</script>