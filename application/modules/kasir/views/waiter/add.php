<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form">
						<div class="card">
							<div class="card-head style-primary">
								<header>Form create new waiter</header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="name" required data-rule-minlength="2" value="<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>">
									<label for="name">Name</label>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" id="email" name="email" required value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
									<label for="email">Email</label>
									<p class="help-block error_dup" style="color:red; display:none"></p>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="phone" name="phone" data-rule-minlength="5" value="<?php echo isset($_POST['phone']) ? $_POST['phone'] : ''; ?>">
									<label for="phone">Phone</label>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="password" name="password" required data-rule-minlength="6">
									<label for="password">Password</label>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="confirm_password" name="confirm_password" required data-rule-minlength="6">
									<label for="confirm_password">Confirm Password</label>
									<p class="help-block error_pass" style="color:red; display:none"></p>
								</div>

								<div class="form-group">
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" name="status">
											<span>Active Waiter</span>
										</label>
									</div>
								</div>
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active r">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form create new waiter</em>
					</form>
				</div>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'kasir/waiter' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('keyup', '#email', function(){
			$.ajax({
		        url 	: url+ 'kasir/waiter/check_email_waiter',
		        type 	: 'POST',
		        data 	: 'email='+$(this).val(),
		        dataType: 'JSON',
		        beforeSend: function()
		        {
		        },
		        success: function(data)
		        {
		        	$('.error_dup').html(data.message);
		        	$('.error_dup').show();
		        	if(data.status){
		        		$('#form').find('.submit').attr('disabled', false);
		        	}else{
		        		$('#form').find('.submit').attr('disabled', true);
		        	}
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		        }         
		    });
		    return false;
		});
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});

		$(document).on('keyup', '#confirm_password', function(){
			var pass 	= $('#password').val();
			var cpass 	= $(this).val();
			if(pass != cpass){
				$('.error_pass').show();
				$('.error_pass').html('Confirm password doesnt same');
				$('#form').find('.submit').attr('disabled', true);
			}else{
				$('.error_pass').hide();
				$('.error_pass').html('');
				$('#form').find('.submit').attr('disabled', false);
			}
		});
		
	})	
</script>