<div id="content">
	<section class="style-default-bright">
		<div class="section-header">
			<h2 class="text-primary">Listing data Drinks</h2>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-md-8">
					<article class="margin-bottom-xxl">
						<p class="lead">
							Listing data Drinks
						</p>
					</article>
				</div>
			</div>
			<div class="row">
				<?php if(isset($message)): ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $message ?>
					</div>
				<?php endif; ?>
				<?php if(isset($success_message)): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $success_message ?>
					</div>
				<?php endif; ?>
				<div class="col-lg-12">
					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="sort-numeric">No</th>
									<th>Title</th>
									<th>Image</th>
									<th class="sort-numeric">Price</th>
									<th>Status</th>
									<th>Created at</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($product as $my_prod):
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $my_prod->name; ?></td>
									<td>
                     <?php 
                    echo (!empty($my_prod->image)) ? "<img src='".base_url($my_prod->image)."' style = 'width:120px; height:120px;' >" : "<img src='".base_url('assets/frontend/images/watermark2.png')."' style = ' width:120px; height:120px;' />"; 
                ?>
                 </td>
									<td><?php echo rupiah($my_prod->price); ?></td>
									<td><?php echo $my_prod->status; ?></td>
									<td><?php echo $my_prod->created_at; ?></td>
									<!-- <td><?php echo ucwords(str_replace("-", " ", $my_prod->status)); ?></td> -->
									<td>
										<a href="<?php echo base_url().'kasir/drink/edit/'.to_Encrypt($my_prod->id); ?>" class="btn ink-reaction btn-flat btn-primary active" data-toggle="tooltip" data-placement="top" data-original-title="Edit <?php echo $my_prod->name; ?>">
											<i class="md md-mode-edit"></i>
										</a>
										<a href="<?php echo base_url().'kasir/drink/delete/'.to_Encrypt($my_prod->id); ?>" class="btn ink-reaction btn-primary delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete <?php echo $my_prod->name; ?>">
											<i class="md md-delete"></i>
										</a>
									</td>
								</tr>
								<?php $no++; endforeach;?>
							</tbody>
						</table>
					</div><!--end .table-responsive -->
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'kasir/drink/add' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Add Dirnks">
					<i class="md md-add"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delete', function(){

			var del_url =  $(this).attr('href');

			bootbox.confirm("<h4>Anda yakin ingin menghapus ?</h4>", function (result) {
		        if (result) {
		           location.href = del_url;
		        }
	    	});
	   	 	return false;
		})
	})
</script>


 <script type="text/javascript">
    var save_method;
    save_method = 'add';
    var table;
    $(document).ready(function() {
      table = 
          $('#table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "<?php echo $datatables_url.'/ajax_list';?>",
                "type": "POST"
            },
            "columnDefs": [{ 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
            }],
          });

   

      $("#form").on('submit',(function() {
        var url;
        if(save_method == 'add') {
            url = "<?php echo $datatables_url.'/ajax_add';?>";
        }else{
            url = "<?php echo $datatables_url.'/ajax_update';?>";
        }  

        $.ajax({
          url: url,
          type: "POST",
          data:  new FormData(this),
          mimeType:"multipart/form-data",
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function()
          {
            $('#form').find('.btnsv').html('<i class="fa fa-spinner fa-pulse"></i>');
            $('#form').find('.btnsv').attr('disabled', true);
          },
          success: function(data)
          {
            save_method = 'add';
            
            $('#form').find('.btnsv').html('SUMBIT');
            $('#form').find('.btnsv').attr('disabled', false);

            $('#form')[0].reset();
            $('#ckeditor').val('');

            $('#place_image').hide();
            $('#parent_id').html("");
            reload_table();
   

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
                alert('Error adding / update data');
          }         
        });

        return false;
      }));

    });

    function edit_category(id)
    {
      save_method = 'edit';
      $('#form')[0].reset();
      $('#ckeditor').val();
      var base_url = "<?php echo base_url() ?>";

      $.ajax({
        url : "<?php echo $datatables_url.'/ajax_edit';?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 

            $('[name="id"]').val(data.id);
            $('[name="nama"]').val(data.nama);
            $('[name="alamat"').val(data.alamat);

            

            if(data.image != ""){
              $('#place_image').show();
              $('#image_category').attr("src", base_url+"uploProduct/gambar/"+data.img);
              $('#btn_choose_image').html('Update Image');
            }else{
              $('#place_image').hide();
              $('#btn_choose_image').html('Choose Image');
            }
            
           

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
      });
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_category(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
          $.ajax({
            url : "<?php echo $datatables_url.'/ajax_delete';?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }

   



     $(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
    });



  </script>