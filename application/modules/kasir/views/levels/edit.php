<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form" enctype="multipart/form-data">
						<div class="card">
							<div class="card-head style-primary">
								<header>Update Levels <?php echo isset($levels['nama']) ? $levels['nama'] : ''; ?></header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="nama" name="nama" required data-rule-minlength="2" value="<?php echo isset($levels['nama']) ? $levels['nama'] : ''; ?>">
									<label for="name">Name</label>
								</div>
								
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form Edit Levels</em>
					</form>
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/levels' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});
		
	});	
</script>