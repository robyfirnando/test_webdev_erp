<div id="content">
	<section class="style-default-bright">
		<div class="section-header">
			<h2 class="text-primary">Listing data Foods</h2>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-md-8">
					<article class="margin-bottom-xxl">
						<p class="lead">
							Listing data Foods
						</p>
					</article>
				</div>
			</div>
			<div class="row">
				<?php if(isset($message)): ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $message ?>
					</div>
				<?php endif; ?>
				<?php if(isset($success_message)): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $success_message ?>
					</div>
				<?php endif; ?>
				<div class="col-lg-12">
					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="sort-numeric">No</th>
									<th>Title</th>
									<th>Image</th>
									<th class="sort-numeric">Price</th>
									<th>Status</th>
									<th>Created at</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($product as $my_prod):
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $my_prod->name; ?></td>
									<td>
                     <?php 
                    echo (!empty($my_prod->image)) ? "<img src='".base_url($my_prod->image)."' style = 'width:120px; height:120px;' >" : "<img src='".base_url('assets/frontend/images/watermark2.png')."' style = ' width:120px; height:120px;' />"; 
                ?>
                 </td>
									<td><?php echo rupiah($my_prod->price); ?></td>
									<td><?php echo $my_prod->status; ?></td>
									<td><?php echo $my_prod->created_at; ?></td>
									<!-- <td><?php echo ucwords(str_replace("-", " ", $my_prod->status)); ?></td> -->
									<td>
										<a href="<?php echo base_url().'kasir/food/edit/'.to_Encrypt($my_prod->id); ?>" class="btn ink-reaction btn-flat btn-primary active" data-toggle="tooltip" data-placement="top" data-original-title="Edit <?php echo $my_prod->name; ?>">
											<i class="md md-mode-edit"></i>
										</a>
										<a href="<?php echo base_url().'kasir/food/delete/'.to_Encrypt($my_prod->id); ?>" class="btn ink-reaction btn-primary delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete <?php echo $my_prod->name; ?>">
											<i class="md md-delete"></i>
										</a>
									</td>
								</tr>
								<?php $no++; endforeach;?>
							</tbody>
						</table>
					</div><!--end .table-responsive -->
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'kasir/food/add' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Add Product">
					<i class="md md-add"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delete', function(){

			var del_url =  $(this).attr('href');

			bootbox.confirm("<h4>Anda yakin ingin menghapus ?</h4>", function (result) {
		        if (result) {
		           location.href = del_url;
		        }
	    	});
	   	 	return false;
		})
	})
</script>


 <script type="text/javascript">
   

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }



     $(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
    });



  </script>