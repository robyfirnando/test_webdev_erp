 <script src="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>

					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form" enctype="multipart/form-data">

						<div class="card">
							<div class="card-head style-primary">
								<header>Form create new Product</header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="name" required data-rule-minlength="2" value="<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>">
									<label for="title">Name product</label>
								</div>
								<div class="form-group" id="place_image" style="display: none;">
			                      <img src="" id="image_category" style="width: 95px;height: 50px;">
			                  </div>

			                  	<div class="form-group">
									<label for="write_by">Price</label>
									<!-- <input type="text" class="form-control" id="write_by" name="price" required value="<?php echo isset($_POST['price']) ? $_POST['price'] : ''; ?>"> -->

									<input type="text" value="Rp 1.000,00" onblur="display(this,1)" onfocus="edit(this,1)" onkeydown="return haltnondigit(event)" class="form-control"><input type=hidden name="price" id="1" value="<?php echo isset($_POST['price']) ? $_POST['price'] : ''; ?>" class="form-control">
								</div>


			                  <div class="form-group">
			                      <a class="btn btn-primary" id="btn_choose_image" onclick="$('#choose_image').click();">Choose Image</a>
			                      <input style="display: none;" type="file" id="choose_image" name="image"></input>
			                  </div>
							<div class="form-group">
									<select class="form-control" name="type">
										<option value="food">Food</option>
										<option value="drink">Drink</option>
									</select>
									<label for="title">Type</label>
							</div>

								<div class="form-group">
											<select name="status" class="form-control">
											<option value="">-Choose Status-</option>
											<option value="ready">Ready</option>
											<option value="notready">Not Ready</option>
											</select>
								</div>

							
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form create new Product</em>
					</form>
				</div>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'kasir/food' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
		</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('blur, change', '#email', function(){
			$.ajax({
		        url 	: url+ 'backend/admins/check_email_admins',
		        type 	: 'POST',
		        data 	: 'email='+$(this).val(),
		        dataType: 'JSON',
		        beforeSend: function()
		        {
		        },
		        success: function(data)
		        {
		        	$('.error_dup').html(data.message);
		        	$('.error_dup').show();
		        	if(data.status){
		        		$('#form').find('.submit').attr('disabled', false);
		        	}else{
		        		$('#form').find('.submit').attr('disabled', true);
		        	}
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		        }         
		    });
		    return false;
		});
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});

		$(document).on('blur, change', '#confirm_password', function(){
			var pass 	= $('#password').val();
			var cpass 	= $(this).val();
			if(pass != cpass){
				$('.error_pass').show();
				$('.error_pass').html('Confirm password doesnt same');
				$('#form').find('.submit').attr('disabled', true);
			}else{
				$('.error_pass').hide();
				$('.error_pass').html('');
				$('#form').find('.submit').attr('disabled', false);
			}
		});

		$(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
    	});
		
	})	
</script>
<script>
/*Rupiah Input*/
var thoudelim=".";
var decdelim=",";
var curr="Rp ";
var d=document;

function haltnondigit(e) {
 var allowkey=Array(48,49,50,51,52,53,54,55,56,57,8,9,188,190,45,46,13,33,34,35,36,37,38,39,40,112,113,114,115,116,117,118,119,120,121,123);
 if(allowkey.indexOf(e.keyCode)==-1) return false;
 return true;
}
function format(s,r) {
 s=Math.round(s*Math.pow(10,r))/Math.pow(10,r);
 s=String(s);s=s.split(".");var l=s[0].length;var t="";var c=0;
 while(l>0){t=s[0][l-1]+(c%3==0&&c!=0?thoudelim:"")+t;l--;c++;}
 s[1]=s[1]==undefined?"0":s[1];
 for(i=s[1].length;i<r;i++) {s[1]+="0";}
 return curr+t+(r==0?"":decdelim+s[1]);
}
function display(t,i) {
 t.value=parseFloat(t.value);
 d.getElementById(i).value=t.value;
 t.value=format(t.value,2);
}
function edit(t,i) { t.value=d.getElementById(i).value; }
</script>