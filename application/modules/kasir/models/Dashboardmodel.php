<?php

class Dashboardmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $this->db->select("*");
        $this->db->where("log_user",$this->session->userdata('back_userid'));
        $this->db->from("tabel_log");
        $query  = $this->db->get();

        $data   = $query->result();
     
        return $data;
    }

    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();
        return $akses[0]->hak_akses;
    }


}