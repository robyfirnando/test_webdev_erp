<?php

class Foodmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function insert()
    {
        $name               = $this->input->post("name");
        $image               = $this->input->post("image");
        $type                = $this->input->post("type");
        $status              = $this->input->post("status");
        $price               = $this->input->post("price");

        if($_FILES['image']['size'] > 0)
        {
            $pic   = do_upload_single('image', './uploads/product/');
            $this->db->set("image", 'uploads/product/'.$pic['data']['file_name']);
        }

        $this->db->set("name", $name)
                 ->set("price", $price)
                 ->set("type", $type)
                 ->set("status", $status)
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("product");
    }




    public function update($id)
    { 
      
        $name                = $this->input->post("name");
        $type                = $this->input->post("type");
        $status              = $this->input->post("status");
        $price               = $this->input->post("price");


        if($_FILES['image']['size'] > 0)
                {
                    $pic   = do_upload_single('image', './uploads/ads/');
                    $this->db->set("image", 'uploads/ads/'.$pic['data']['file_name']);
        }

       $this->db->where("id",$id)
                 ->set("name", $name)
                 ->set("price", $price)
                 ->set("type", $type)
                 ->set("status", $status)
                 ->set("modified_at", date("Y-m-d H:i:s"))
                 ->set("modified_by", $this->session->userdata('back_userid'))
                 ->update("product");
      
        return true;
    }
    
    public function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->set("status", "deleted");
        $this->db->update("product");
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("product");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all($status)
    {
        $this->db->select("*");
        // $this->db->where("status", "ready");
        $this->db->where("type", "food");
        // if(count($status) > 0)
        // {
        //     $i = 1;
        //     foreach ($status as $st) 
        //     {
        //         if($i == 1){
        //             $this->db->where("status", $st);
        //         }else{
        //             $this->db->or_where("status", $st);
        //         }
        //         $i++;
        //     }
        // }
        $this->db->from("product");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }
    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();


        return $akses[0]->hak_akses;
    }

   

}   