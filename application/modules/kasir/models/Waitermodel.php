<?php

class Waitermodel extends CI_Model
{   
	public function __construct()
    {
        parent::__construct();
    }

    public function insert()
    {
        $status             = ($this->input->post("status") == 'on') ? 'active' : 'non-active' ;
        $name				= $this->input->post("name");
        $email              = $this->input->post("email");
        $phone				= $this->input->post("phone");
        $password			= $this->encrypt->encode($this->input->post("password"));
        $levelid            = $this->get_level_id('waiter');
		
        $this->db->set("name", $name)
                 ->set("email", $email)
                 ->set("phone", $phone)
                 ->set("password", $password)
                 ->set("status", $status)
                 ->set("id_level",$levelid)
				 ->set("created_at", date("Y-m-d H:i:s"))
                 ->insert("login");
        return true;
    }

    public function update($id)
    {
        $name				= $this->input->post("name");
        $email				= $this->input->post("email");
        $phone              = $this->input->post("phone");
        $password			= $this->input->post("password");
        $status             = ($this->input->post("status") == 'on') ? 'active' : 'non-active' ;

		if(!empty($password)){
			$this->db->where("id", $id)
					 ->set("name", $name)
					 ->set("email", $email)
                     ->set("phone", $phone)
					 ->set("password", $this->encrypt->encode($password))
                     ->set("status", $status)
                     ->set("modified_at", date("Y-m-d H:i:s"))
                     ->set("modified_by", $this->session->userdata('back_userid'))
					 ->update("login");
		}
		else{
			$this->db->where("id", $id)
					 ->set("name", $name)
					 ->set("email", $email)
                     ->set("phone", $phone)
                     ->set("status", $status)
                     ->set("modified_at", date("Y-m-d H:i:s"))
                     ->set("modified_by", $this->session->userdata('back_userid'))
					 ->update("login");
		}
        return true;
    }
    
    public function delete($id)
    {
        $this->db->where("id", $id);
        // $this->db->set("status", "deleted");
        // $this->db->update("login");
        $this->db->delete('login');
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("login");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all($status)
    {
        $waiter =$this->get_level_id('waiter');
        $this->db->select("*");
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("status", $st);
                    $this->db->where("id_level", $waiter );
                }else{
                    $this->db->or_where("status", $st);
                    $this->db->where("id_level", $waiter);
                }
                $i++;
            }
        }
        $this->db->from("login");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function check_email($email)
    {
        $this->db->select("count(0) as cnt");
        $this->db->from("login");
        $this->db->where("email", $email);
        $qry = $this->db->get()->result();
        return $qry ? $qry[0]->cnt:0;
    }

    public function check_email_edit($email, $uri_email)
    {
        $this->db->select("count(0) as cnt");
        $this->db->from("login");
        $this->db->where("email", $email);
        $this->db->where("email !=", $uri_email);
        $qry = $this->db->get()->result();
        return $qry ? $qry[0]->cnt:0;
    }

        public function cek_akses($id_level)
    {   
        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();

        return $akses[0]->hak_akses;
    }

    public function get_level_id($name)
    {
        $this->db->select("id as cnt");
        $this->db->from("levels");
        $this->db->where("nama", $name);
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    
    }

}