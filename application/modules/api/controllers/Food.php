<?php

require APPPATH . '/libraries/REST_Controller.php';

class Food extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Foodmodel','model');
    }

    function index_get() {
         $woi = $this->model->get_all();
        $this->response($woi, 200);
    }
}