<?php

require APPPATH . '/libraries/REST_Controller.php';

class auth extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Authmodel','model');

    }

    function index_get()
    {
        $email = $this->get('email');
        $password = $this->get('password');

        $login = $this->model->get_login($email,$password);
       
        if ($login) {
            $this->response($login, 200);
        } 
        else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function get_password_get()
    {
        $email = $this->get('email');

        $get_pass = $this->model->get_password($email);

        if ($get_pass) {
            $this->response($get_pass, 200);
        } 
        else {
            $this->response(array('status' => 'fail', 502));
        }

    }
    

    function changepassword_put()
    {
        $id = $this->put('id');
        
        $changepass = $this->model->changepassword($id);

        if ($changepass) {
            $this->response($changepass, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    function get_by_email_get()
    {
        $email = $this->get('email');

        $get_email = $this->model->get_by_email($email);

        if($get_email){
            $this->response($get_email);
        }else{
            $this->response(array('status' => 'fail', 502));
        }

    }

    function get_by_id_get()
    {
        $id = $this->get('id');

        $getbyid = $this->model->get_by_id($id);

        if($getbyid){
            $this->response($getbyid);
        }else{
            $this->response(array('status' => 'fail', 502));
        } 

        
    }

     function get_id_get()
    {
        $id = $this->get('id');

        $getid = $this->model->get_id($id);

        if($getid){
            $this->response($getid);
        }else{
            $this->response(array('status' => 'fail', 502));
        } 

    }

    function cek_akses_get()

    {   
        $id_level = $this->get('id_level');

        $akses = $this->model->cek_akses($id_level);

        
        $this->response($akses,200);
       
    }
}