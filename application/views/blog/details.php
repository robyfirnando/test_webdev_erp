<style type="text/css">
.tt-query,
.tt-hint {
  width: 396px;
  height: 30px;
  padding: 8px 12px;
  font-size: 24px;
  line-height: 30px;
  border: 2px solid #ccc;
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  outline: none;
}

.typeahead {
  background-color: #fff;
}

.typeahead:focus {
  border: 2px solid #0097cf;
}

.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-menu {
  width: 422px;
  margin: 12px 0;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  font-size: 18px;
  line-height: 24px;
}

.tt-suggestion:hover {
  cursor: pointer;
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion.tt-cursor {
  color: #fff;
  background-color: #0097cf;

}

.tt-suggestion p {
  margin: 0;
}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/phpholidays_default/vendor/unite-gallery/js/unitegallery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/phpholidays_default/vendor/unite-gallery/themes/tiles/ug-theme-tiles.js"></script>
<script type="" src="<?php echo base_url(); ?>/assets/frontend/phpholidays_default/vendor/unite-gallery/themes/default/ug-theme-default.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- include star rating -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/css/star-rating.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/css/star-rating.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/js/star-rating.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/js/star-rating.min.js"></script>
<script  type="text/javascript" src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1_eSUPKVxRMxI7lgnmeeuZzaITaYQqN4&libraries=places&callback=initMap" async defer></script> -->
 <script src="http://maps.google.com/maps/api/js?key=AIzaSyB1_eSUPKVxRMxI7lgnmeeuZzaITaYQqN4&language=id&libraries=places" type="text/javascript"></script>
<div class="container">
    <!-- Inner Layout Start -->
    <div class="inner">
        <div class="row">

            <!-- Content Start -->
            <div class="col-md-12">
                <div class="drawer-overlay hotels">
                    <style type="text/css">
                        #map-canvas {
                            min-height: 400px;
                            margin: 0 0 20px 0;
                            padding: 0;
                        }
                        
                        .map-tab input {
                            margin: 5px 0;
                        }
                    </style>

                    <div>
                        <div class="module">
                            <div class="col-md-12 col-sm-12">
                                <!-- pager -->
                                <ul class="pager">
                                    <li class="previous disabled"><a href="javascript:void(0)"><i class="fa fa-chevron-left"></i> Previous</a></li>
                                    <li class="next "><a href="../Ascott-Raffles-Place-Hotel.html">Next <i class="fa fa-chevron-right"></i></a></li>
                                </ul>
                                <!-- pager -->

                                <!-- info block -->

                                <!-- Top Bar End -->
                                <?php
                                    foreach($blog_images as $image):
                                 ?>
                                    <h1><?php echo $image->title; ?></h1>
                                    <div class="ratings">
                                    <font size="3">
                                    <input id="" name="rating"
                                           class="rating "
                                           min="0" max="5" step="0.5" data-size="xs"
                                           accept="" data-symbol="&#xf005;" data-glyphicon="false"
                                           data-rating-class="rating-fa" value="<?php echo $image->total_point; ?>">
                                    </div>
                                    </font>
                                    <script>
                                        $( document ).ready(function() {
                                            $('.rating').on('rating.change', function (event, value, caption) {
                                                var rate_id = $(this).prop('id');
                                                var pure_id = rate_id.substring(6);
                                                $.post('<?= base_url()?>blog/create_rate', {score: value, pid: pure_id},
                                                    function (data) {
                                                        $('#' + rate_id).rating('refresh', {
                                                            showClear: false,
                                                            showCaption: false,
                                                            disabled: true
                                                        });
                                                    });
                                                alert(value);
                                                console.log(pure_id);
                                            });
                                        });
                                    </script>
                                    <br>
                                    <br>

                                    <!-- info block -->
                                    <br>

                                    <!-- Slide Show Start -->
                                    <center>
                                        <div id="gallery" style="overflow:hidden; height:400px !important;position:center;">
                                            <?php foreach(get_image_detail_only($image->id, 'images') as $key => $img):
                                             ?>
                                                <img alt=", Bangladesh" src="<?php echo base_url($img->image);?>" data-image="<?php echo base_url($img->image);?>">
                                                <?php endforeach; ?>
                                                    <?php 
                                                         if ($image->video_url){ ?>
                                                        <center>
                                                            <iframe width="80%" height="80%" src="<?php echo $image->video_url; ?>" allowfullscreen></iframe>
                                                        </center>
                                                        <?php } ?>

                                        </div>
                                    </center>
                                    <!-- Slide Show End -->

                                    <!-- Circular Button Start -->

                                    <div class="circle-nav-bar">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <figure class="circle-btn title-tipso" title="Print Details">
                                                        <div class="circle">
                                                            <a class="print_btn" id="print_btn" onclick="print();"><i class="fa fa-print print"><i class="circle-border"></i></i></a>
                                                        </div>
                                                    </figure>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <figure class="circle-btn title-tipso" title="Save">
                                                        <div class="circle">
                                                            <a href="#" class="save_hotels_btn" rel="2"><i class="fa fa-save save"><i class="circle-border"></i></i></a>
                                                        </div>
                                                    </figure>
                                                </div>

                                            </div>
                                        </div>
                                          <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <table class="table table-bordered table-striped table-hover">

                                                    <tr>
                                                        <td class="td-width"><i class="fa fa-map-marker" aria-hidden="true"></i> Alamat :</td>
                                                        <td>
                                                            <?php echo "".$image->address."";?></td>
                                                    </tr>

                                                    <tr>
                                                        <td><i class="fa fa-phone" aria-hidden="true"></i> Telepon :</td>
                                                        <td>
                                                            <?php echo "".$image->phone."";?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fa fa-clock-o" aria-hidden="true"></i> Jam Buka :</td>
                                                        <td>
                                                            <?php echo "".$image->jam_buka."-".$image->jam_tutup.' WIB';?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                    <!-- Circular Button End -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4>Description</h4>
                                        </div>

                                        <div class="panel-body">
                                            <?php 
                                                echo $image->description;
                                           ?>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                        <!-- address Start -->
                                        <div id="map-tab" class="map-tab1">
                                            <ul class="resp-tabs-list">
                                                <li>Map Search Neaby Place</li>
                                            </ul>

                                            <div class="resp-tabs-container">
                                                <div>
                                                    <?php 
                                                        // echo $map_one['js']; 
                                                        // echo $map_one['html']; 
                                                    ?>
                                                    <div id="map-canvas" style="height:100%;"></div>

                                                        <div class="map text-center">
                                                            <h2><span id="map_loader"></span></h2>
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <!-- <input id="location_nearby" type="text" value="" class="form-control" > -->
                                                                    <select id="location_nearby" class="form-control">
                                                                      <option value="atm">ATM</option>
                                                                      <option value="bank">Bank</option>
                                                                      <option value="hospital">Rumah Sakit</option>
                                                                      <option value="police">Kantor Polisi</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                  <div class="col-md-4 col-sm-4 col-xs-6">
                                                                      <input type="button" value="Near By" class="btn btn-global btn-block" id="nearby">
                                                                  </div>
                                                                  <div class="col-md-4 col-sm-4 col-xs-6">
                                                                      <input type="button" value="Reset" class="btn btn-block" id="reset_map1">
                                                                  </div>
                                                                  <!-- <div class="col-md-4 col-sm-4 col-xs-6">
                                                                      <input type="button" value="Direction" class="btn btn-global btn-block" id="direction_btn1">
                                                                  </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div id="map-tab" class="map-tab2" style="">
                                          <div class="col-md-9">
                                            <div class="resp-tabs-container">
                                                <div>
                                                    <?php 
                                                        // echo $map_one['js']; 
                                                        // echo $map_one['html']; 
                                                    ?>
                                                    <div id="map-canvas2" style="height:100%;"></div>
                                                        <div class="map text-center">
                                                            <h2><span id="map_loader"></span></h2>
                                                            <div class="row">
                                                            <div id="animate-a">
                                                              <div class="col-md-4 col-sm-4 col-xs-12">
                                                              Start
                                                                <select id="start" name="start" class="form-control">
                                                                <?php
                                                                    foreach($get_locations as $us):
                                                                  ?>

                                                                <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>"><?php echo ucwords(str_replace("-", " ", $us->title)); ?></option>
                                                                <?php
                                                                  endforeach;
                                                                ?>
                                                                </select> 
                                                              </div>
                                                               <div class="col-md-4 col-sm-4 col-xs-12">
                                                               Your Waypoint
                                                                   <select multiple class="usingselect2 form-control" id="waypoints" style="width:250px;">
                                                                      form-control">
                                                                  <?php
                                                                      foreach($get_locations as $us):
                                                                    ?>
                                                                  <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>"><?php echo ucwords(str_replace("-", " ", $us->title)); ?></option>
                                                                  <?php
                                                                    endforeach;
                                                                  ?>
                                                                      </select>
                                                                </div>

                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                   <!-- <select id="start" name="start" class="form-control">
                                                                          <?php
                                                                              foreach($get_locations as $us):
                                                                            ?>
                                                                          <option value="<?php echo $us->id; ?>"><?php echo ucwords(str_replace("-", " ", $us->title)); ?></option>
                                                                          <?php
                                                                            endforeach;
                                                                          ?>
                                                                          </select> -->
                                                                  End
                                                                 <select id="end" name="end" class="form-control">
                                                                <?php
                                                                    foreach($get_locations as $us):
                                                                  ?>

                                                                <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>"><?php echo ucwords(str_replace("-", " ", $us->title)); ?></option>
                                                                <?php
                                                                  endforeach;
                                                                ?>
                                                                </select> 
                                                                </div>
                                                               <div class="col-md-4 col-sm-4 col-xs-12">
                                                                  <select class="form-control" name="travelmode" id="travelmode">
                                                                      <option value="DRIVING">Driving</option>
                                                                      <option value="WALKING">Walking</option>
                                                                      <option value="BICYCLING">Bicycling</option>
                                                                      <option value="TRANSIT">Transit</option>
                                                                  </select>
                                                               </div>
                                                            </div> 
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                                                    <input type="button" value="Get Direction" class="btn btn-global btn-block" id="direction_btn">
                                                                  </div>

                                                                   <div class="col-md-4 col-sm-4 col-xs-12">
                                                                    <input type="button" value="Reset" class="btn btn-block" id="reset_map2">
                                                                   </div>
                                                                   <div class="col-md-4 col-sm-4 col-xs-12">
                                                                     <input type="button" value="Back" class="btn btn-danger btn-block direction_btnback">
                                                                    </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                          </div>
                                        <div class="col-md-3">
                                         <div id="right-panel">
                                          <p>Total Distance: <span id="total"></span></p>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        <!-- Map End-->

                                   

                                        <!-- Details Tab Start-->
                                       
                                        <!-- Details Tab End-->

                                        <!-- Matching Tab Start-->
                                        
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>

                    <div id="dialog_container" title="Message dialog"></div>
                    <div id="dialog_msg" title="Message dialog"></div>

                    <!-- Comments start -->
                    <style type="text/css">
                        .clikable_final:hover {
                            cursor: pointer;
                            cursor: hand;
                        }
                    </style>
                    <div id="click_comment" class="clikable_final"><a><i class="fa fa-commenting"></i> Comments</a></div>

                    <div id="hide_section" style="display:none">
                        <div id="show_comment"></div>

                        <input type="hidden" id="limit_value" name="user" value="2" />

                        <div class="clearfix"> </div>
                        <!-- <h3><i class="fa fa-commenting"></i> </h3> -->
                        <br>
                        <div class="comments_start comments">
                        </div>

                        <div class="clearfix"></div>

                        <div id="dialog_msg" title="Add Comment Dialog"></div>
                        <div id="dialog-confirm" title="Delete Confirmation"></div>
                        <div id="dialog_container" title="Processing........ Please wait." style="display:none">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    <span class="sr-only">100% Complete</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Comments end -->
                </div>
            </div>
            <!-- Content Start -->
        </div>
    </div>
    <!-- Inner Layout End -->
</div>

<!-- Body End-->
</body>


</html>
<script type="text/javascript">


   $(document).ready(function() {
    $(".map-tab2").hide();
     $(".usingselect2").select2({
      allowclear: true
    });
    jQuery(document).ready(function() {
        jQuery("#gallery").unitegallery();
    });

    $('#direction_btn1').click(function(){
        $(".map-tab1").remove();
        $(".map-tab2").removeAttr("style");
        initialize2();
    });

    $('.direction_btnback').click(function(){
      $(".map-tab2").hide("slow");
      $(".map-tab1").show("slow");
      initialize();
    });

    

});

//--------------------------------INITIALIZE MAP || Map 1--------------------------------------------
var map;
  function initialize() {

  
  var map = new google.maps.Map(
    document.getElementById('map-canvas'), {
      center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
  });

 // var myIcon = {
 //       url: "https://image.flaticon.com/icons/svg/272/272876.svg", //url
 //       size: new google.maps.Size(75, 75),
 //       scaledSize: new google.maps.Size(75, 75)
 //     };

  var marker = new google.maps.Marker({
        position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
        map: map
        // icon: myIcon
  });

        var contentString = '<?php echo $title; ?>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
   
}
google.maps.event.addDomListener(window, 'load', initialize);

$('#reset_map1').on('click',initialize);



//------------------------------------END INITIALIZE MAP--------------------------------------------






//-----------------------------------MAP FUNCTION-----------------------------------------------------


/*CUSTOM FUNC*/




/*----------------------MAP Waypoint || Map 2-------------------*/
  var infowindow;
  var map2;
   function initialize2() {
        var directionsService = new google.maps.DirectionsService;
         var directionsDisplay = new google.maps.DirectionsRenderer({
          draggable: true,
          map: map2,
          panel: document.getElementById('right-panel')
        });

       directionsDisplay.addListener('directions_changed', function() {
          computeTotalDistance(directionsDisplay.getDirections());
        });

     function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
          total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        document.getElementById('total').innerHTML = total + ' km';
      }

        var map2 = new google.maps.Map(document.getElementById('map-canvas2'), {
          zoom: 14,
          center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>)
        });
         var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                        map: map2
                  });
         var contentString = '<?php echo $title; ?>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.open(map2, marker);
        });

        directionsDisplay.setMap(map2);

        document.getElementById('direction_btn').addEventListener('click', function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        });

        document.getElementById('reset_map2').addEventListener('click', function() {
          directionsDisplay.setMap(null);
        });
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var selectedMode = document.getElementById('travelmode').value;
        var waypts = [];
        var checkboxArray = document.getElementById('waypoints');
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {
            waypts.push({
              location: checkboxArray[i].value,
              stopover: true
            });
          }
        }

        directionsService.route({
          origin:  document.getElementById('start').value,
          destination: document.getElementById('end').value,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            // var summaryPanel = document.getElementById('directions-panel');
            // summaryPanel.innerHTML = '';
            // For each route, display summary information.
            // for (var i = 0; i < route.legs.length; i++) {
            //   var routeSegment = i + 1;
            //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
            //       '</b><br>';
            //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
            //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
            //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            // }
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
google.maps.event.addDomListener(window, 'load', initialize2);



/*-----------------------NearBy Place Search-------------------------*/
  $("#nearby").click(function() {
        var location_nearby = $("#location_nearby").val()

          $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>blog/nearby_search",
                data: {
                    location_nearby: location_nearby
                },
                beforeSend: function() {
                },
                success: function(data) {
                  var map;
                  var infowindow;

                  function initialize() {
                    var pyrmont = {lat: <?php echo $lat; ?>, lng: <?php echo $long; ?>};

                    map = new google.maps.Map(document.getElementById('map-canvas'), {
                      center: pyrmont,
                      zoom: 15
                    });
                      var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                        map: map
                  });

                    infowindow = new google.maps.InfoWindow();
                    var service = new google.maps.places.PlacesService(map);
                    var input = $("#location_nearby").val();
                    service.nearbySearch({
                      location: pyrmont,
                      radius: 550,
                      type: [input]
                    }, callback);
                  }

             function callback(results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                  for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                  }
                }
              }

              function createMarker(place) {
                var image = {
                            url: place.icon,
                            size: new google.maps.Size(35, 31),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(35, 35)
                          };
                var placeLoc = place.geometry.location;
                var marker = new google.maps.Marker({
                  map: map,
                  icon: image,
                  position: place.geometry.location
                });

                google.maps.event.addListener(marker, 'click', function() {
                  infowindow.setContent(place.name);
                  infowindow.open(map, this);
                });
              }

                initialize();
                
                }
            });
  });
</script>