<style type="text/css">
    .icon-spesifikasi {
        filter: gray;
        -webkit-filter: grayscale(1);
        filter: grayscale(1);
    }
    
    ul {
        list-style-type: none;
    }
    
    .tt-query,
    .tt-hint {
        width: 396px;
        height: 30px;
        padding: 8px 12px;
        font-size: 24px;
        line-height: 30px;
        border: 2px solid #ccc;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        outline: none;
    }
    
    .typeahead {
        background-color: #fff;
    }
    
    .typeahead:focus {
        border: 2px solid #0097cf;
    }
    
    .tt-query {
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }
    
    .tt-hint {
        color: #999
    }
    
    .tt-menu {
        width: 422px;
        margin: 12px 0;
        padding: 8px 0;
        background-color: #fff;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    }
    
    .tt-suggestion {
        padding: 3px 20px;
        font-size: 18px;
        line-height: 24px;
    }
    
    .tt-suggestion:hover {
        cursor: pointer;
        color: #fff;
        background-color: #0097cf;
    }
    
    .tt-suggestion.tt-cursor {
        color: #fff;
        background-color: #0097cf;
    }
    
    .tt-suggestion p {
        margin: 0;
    }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/phpholidays_default/vendor/unite-gallery/js/unitegallery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/frontend/phpholidays_default/vendor/unite-gallery/themes/tiles/ug-theme-tiles.js"></script>
<script type="" src="<?php echo base_url(); ?>/assets/frontend/phpholidays_default/vendor/unite-gallery/themes/default/ug-theme-default.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- include star rating -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/css/star-rating.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/css/star-rating.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/js/star-rating.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/js/star-rating.min.js"></script>
<script type="text/javascript" src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1_eSUPKVxRMxI7lgnmeeuZzaITaYQqN4&libraries=places&callback=initMap" async defer></script> -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyB1_eSUPKVxRMxI7lgnmeeuZzaITaYQqN4&language=id&libraries=places" type="text/javascript"></script>
<div class="container">
    <!-- Inner Layout Start -->
    <div class="inner">
        <div class="row">

            <!-- Content Start -->
            <div class="col-md-12">
                <div class="drawer-overlay hotels">
                    <style type="text/css">
                        #map-canvas {
                            min-height: 400px;
                            margin: 0 0 20px 0;
                            padding: 0;
                        }
                        
                        .map-tab input {
                            margin: 5px 0;
                        }
                    </style>

                    <div>
                        <div class="module">
                            <div class="col-md-12 col-sm-12">
                                <!-- pager -->
                                <ul class="pager">
                                    <li class="previous disabled"><a href="javascript:void(0)"><i class="fa fa-chevron-left"></i> Previous</a></li>
                                    <li class="next "><a href="../Ascott-Raffles-Place-Hotel.html">Next <i class="fa fa-chevron-right"></i></a></li>
                                </ul>
                                <!-- pager -->

                                <!-- info block -->

                                <!-- Top Bar End -->
                                <?php
                                    foreach($blog_images as $image):
                                        // var_dump($image);
                                        //         exit();
                                 ?>
                                    <h1><?php echo $image->title; ?></h1>
                                    <div class="ratings">
                                        <font size="3">
                                    <input id="" name="rating"
                                           class="rating "
                                           min="0" max="5" step="0.5" data-size="xs"
                                           accept="" data-symbol="&#xf005;" data-glyphicon="false"
                                           data-rating-class="rating-fa" value="<?php echo $image->total_point; ?>">
                                    </div>
                                    </font>
                                        <script>
                                            $(document).ready(function() {
                                                $('.rating').on('rating.change', function(event, value, caption) {
                                                    var rate_id = $(this).prop('id');
                                                    var pure_id = rate_id.substring(6);
                                                    $.post('<?= base_url()?>blog/create_rate', {
                                                            score: value,
                                                            pid: pure_id
                                                        },
                                                        function(data) {
                                                            $('#' + rate_id).rating('refresh', {
                                                                showClear: false,
                                                                showCaption: false,
                                                                disabled: true
                                                            });
                                                        });
                                                    alert(value);
                                                    console.log(pure_id);
                                                });
                                            });
                                        </script>
                                        <br>
                                        <br>

                                        <!-- info block -->
                                        <br>

                                        <!-- Slide Show Start -->
                                        <center>
                                            <div id="gallery" style="overflow:hidden; height:400px !important;position:center;">
                                                <?php 
                                                    foreach(get_image_detail_inap_only($image->id, 'images') as $key => $img): 
                                            ?>
                                                    <img alt=", Bangladesh" src="<?php echo base_url($img->image);?>" data-image="<?php echo base_url($img->image);?>">
                                                    
                                                    <?php endforeach; ?>
                                            </div>
                                        </center>
                                        <!-- Slide Show End -->

                                        <!-- Circular Button Start -->

                                        <div class="circle-nav-bar">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <figure class="circle-btn title-tipso" title="Print Details">
                                                        <div class="circle">
                                                            <a class="print_btn" id="print_btn" href="<?php echo base_url().'report/cetak/'.$image->slug; ?>"><i class="fa fa-print print"><i class="circle-border"></i></i></a>
                                                        </div>
                                                    </figure>
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <figure class="circle-btn title-tipso" title="Agent Contact">
                                                        <div class="circle">
                                                            <a href="#" data-toggle="modal" data-target="#myModalHorizontal"><i class="fa fa-phone contact"><i class="circle-border"></i></i></a>
                                                        </div>
                                                    </figure>
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-xs-4">
                                                    <figure class="circle-btn title-tipso" title="Save">
                                                        <div class="circle">
                                                            <a href="#" class="save_hotels_btn" rel="2"><i class="fa fa-save save"><i class="circle-border"></i></i></a>
                                                        </div>
                                                    </figure>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Details Tab Start-->
                                        <div id="details-tab" class="clearfix">
                                            <ul class="resp-tabs-list">
                                                <li>Spesifikasi</li>
                                                <li class="photos">Fasilitas <span class="label label-success"><?php echo ''; ?></span></li>
                                            </ul>

                                            <!-- Tab 1 start -->
                                            <div class="resp-tabs-container">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <table class="table table-bordered table-striped table-hover">

                                                                <tr>
                                                                    <td class="td-width"><img src="http://html.rumah360.com/assets/img/icons/detail_properti_meter.svg" width="50px">Luas Tanah :</td>
                                                                    <td>
                                                                        <?php echo "<h3>".$image->luas_tanah."</h3>";?></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><img src="http://html.rumah360.com/assets/img/icons/detail_properti_kamartidur.svg" width="50px">Kamar Tidur :</td>
                                                                    <td>
                                                                        <?php echo "<h3>".$image->jml_kamartidur."</h3>";?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><img src="http://html.rumah360.com/assets/img/icons/detail_properti_kamarmandi.svg" width="50px">Kamar Mandi :</td>
                                                                    <td>
                                                                        <?php echo "<h3>".$image->jml_kamarmandi."</h3>";?></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <!-- Agent Info Start -->
                                                        <div class="col-md-6 col-sm-6">
                                                            <table class="table table-bordered table-striped table-hover">

                                                                <tr>
                                                                    <td class="td-width"><img src="http://html.rumah360.com/assets/img/icons/detail_properti_meter.svg" width="50px"> Jumlah Lantai :</td>
                                                                    <td>
                                                                        <?php echo "<h3>".$image->jml_lantai."</h3>";?></td>
                                                                </tr>

                                                                <tr>
                                                                    <td><img class="icon-spesifikasi" src="http://html.rumah360.com/assets/img/icons/detail_listrik.svg" width="50px">Listrik :</td>
                                                                    <td>
                                                                        <?php echo "<h3>".$image->listrik." Watt</h3>";?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><img class="icon-spesifikasi" src="http://html.rumah360.com/assets/img/icons/detail_status.svg" width="50px">Sertifikat :</td>
                                                                    <td>
                                                                        <?php echo "<h4>".$image->sertifikat."</h6>";?></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <!-- Agent Info End -->
                                                    </div>
                                                </div>

                                                <div>
                                                    <h3>Features :</h3>
                                                    <br>
                                                    <h4><i class="fa fa-check-square-o"></i> Facilities :</h4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-6">
                                                            <ul class="col-md-9">
                                                              <?php 
                                                                foreach ($fasilitas_data as $fas) :
                                                                  foreach ($exp as $key ) :
                                                                       
                                                             ?> 
                                                                    <div class="col-md-4 col-sm-6">
                                                                        <?php
                                                                            if ($fas->id == $key) {
                                                                                echo "&#8226; ".$fas->desc."";
                                                                            }
                                                                        ?>
                                                                    </div>

                                                            <?php endforeach; ?>
                                                            <?php endforeach; ?>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix">
                                                        <br>
                                                    </div>
                                                    <h4><i class="fa fa-check-square-o"></i> Fasilitas Umum :</h4>
                                                    <hr>
                                                    <div class="row">
                                                       <?php 
                                                                foreach ($fasilitas_umum as $fas) :
                                                                  foreach ($exp as $key ) :
                                                             ?> 
                                                            <div class="col-md-4 col-sm-6">
                                                                    <?php
                                                                            if ($fas->id == $key) {
                                                                                echo "&#8226; ".$fas->desc."";
                                                                            }
                                                                        ?>
                                                            </div>
                                                            <?php endforeach; ?>
                                                            <?php endforeach; ?>
                                                    </div>
                                                </div>

                                                <div>
                                                </div>

                                                <div>
                                                    <div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4>Brochures</h4></div>
                                                            <div class="panel-body">
                                                                <h3></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div id="tabs-8">
                                                        <a name="payment_tab" id="payment_tab"></a>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4><span class="glyphicon glyphicon-th"></span> Financing :</h4></div>
                                                            <div class="panel-body">
                                                                <div class="details">
                                                                    <h2>Payment Process</h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div></div>
                                                </div>

                                                <div>
                                                    <div>
                                                        <ul class="list-group room-chart">
                                                            <li class="list-group-item">
                                                                <div class="row">
                                                                    <div class="col col-md-4 col-sm-12 col-xs-12">
                                                                        <a class="preview" rel="1" link="" title="">Classic Double or Twin Room</a>
                                                                    </div>
                                                                    <div class="col col-md-2 col-sm-12 col-xs-12">
                                                                        <a class="preview" link="" title="hotels front page list title people">
                                                                        </a>
                                                                    </div>
                                                                    <div class="col col-md-3 col-sm-12 col-xs-12 text-right">
                                                                        <span class="price">USD</span>
                                                                    </div>
                                                                    <div class="col col-md-3 col-sm-12 col-xs-12">

                                                                        <select name="dialog_calendar_select0" id="dialog_calendar_select0" class="dialog_calendar_select form-control input-sm">

                                                                            <option value="0">Select to view calendar</option>

                                                                            <option value="1">Room No. 201 (USD&nbsp;) Per night</option>
                                                                            <option value="27">Room no:410 (USD&nbsp;) Per night</option>
                                                                            <option value="28">Room no: 305 (USD&nbsp;) Per night</option>
                                                                            <option value="29">Room no:206 (USD&nbsp;) Per night</option>
                                                                            <option value="30">Room no:505 (USD&nbsp;) Per night</option>
                                                                        </select>

                                                                    </div>

                                                                </div>

                                                            </li>
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                        <div id="example" class="k-content">
                                                            <div>
                                                                <div id="scheduler"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div>
                                                    <div class="review" style="text-align:left">
                                                        <p>No Reviews Found</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Details Tab End-->
                                        <!-- Circular Button End -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4>Description</h4>
                                            </div>

                                            <div class="panel-body">
                                                <?php 
                                                echo $image->description;
                                           ?>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                            <!-- address Start -->
                                            <div id="map-tab" class="map-tab1">
                                                <ul class="resp-tabs-list">
                                                    <li>Map Search Neaby Place</li>
                                                </ul>

                                                <div class="resp-tabs-container">
                                                    <div>
                                                        <?php 
                                                        // echo $map_one['js']; 
                                                        // echo $map_one['html']; 
                                                    ?>
                                                            <div id="map-canvas" style="height:100%;"></div>

                                                            <div class="map text-center">
                                                                <h2><span id="map_loader"></span></h2>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <input id="location_nearby" type="text" value="" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                                                            <input type="button" value="Near By" class="btn btn-global btn-block" id="nearby">
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-4 col-xs-6">
                                                                            <input type="button" value="Reset" class="btn btn-block" id="reset_map1">
                                                                        </div>
                                                                     <!--    <div class="col-md-4 col-sm-4 col-xs-6">
                                                                            <input type="button" value="Direction" class="btn btn-global btn-block" id="direction_btn1">
                                                                        </div> -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div id="map-tab" class="map-tab2" style="">
                                                    <div class="col-md-9">
                                                        <div class="resp-tabs-container">
                                                            <div>
                                                                <?php 
                                                        // echo $map_one['js']; 
                                                        // echo $map_one['html']; 
                                                    ?>
                                                                    <div id="map-canvas2" style="height:100%;"></div>
                                                                    <div class="map text-center">
                                                                        <h2><span id="map_loader"></span></h2>
                                                                        <div class="row">
                                                                            <div id="animate-a">
                                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                    Start
                                                                                    <select id="start" name="start" class="form-control">
                                                                                        <?php
                                                                    foreach($get_locations as $us):
                                                                  ?>

                                                                                            <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>">
                                                                                                <?php echo ucwords(str_replace("-", " ", $us->title)); ?>
                                                                                            </option>
                                                                                            <?php
                                                                  endforeach;
                                                                ?>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                    Your Waypoint
                                                                                    <select multiple class="usingselect2 form-control" id="waypoints" style="width:250px;">
                                                                                        form-control">
                                                                                        <?php
                                                                      foreach($get_locations as $us):
                                                                    ?>
                                                                                            <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>">
                                                                                                <?php echo ucwords(str_replace("-", " ", $us->title)); ?>
                                                                                            </option>
                                                                                            <?php
                                                                    endforeach;
                                                                  ?>
                                                                                    </select>
                                                                                </div>

                                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                    <!-- <select id="start" name="start" class="form-control">
                                                                          <?php
                                                                              foreach($get_locations as $us):
                                                                            ?>
                                                                          <option value="<?php echo $us->id; ?>"><?php echo ucwords(str_replace("-", " ", $us->title)); ?></option>
                                                                          <?php
                                                                            endforeach;
                                                                          ?>
                                                                          </select> -->
                                                                                    End
                                                                                    <select id="end" name="end" class="form-control">
                                                                                        <?php
                                                                    foreach($get_locations as $us):
                                                                  ?>

                                                                                            <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>">
                                                                                                <?php echo ucwords(str_replace("-", " ", $us->title)); ?>
                                                                                            </option>
                                                                                            <?php
                                                                  endforeach;
                                                                ?>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                    <select class="form-control" name="travelmode" id="travelmode">
                                                                                        <option value="DRIVING">Driving</option>
                                                                                        <option value="WALKING">Walking</option>
                                                                                        <option value="BICYCLING">Bicycling</option>
                                                                                        <option value="TRANSIT">Transit</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <input type="button" value="Get Direction" class="btn btn-global btn-block" id="direction_btn">
                                                                                    </div>

                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <input type="button" value="Reset" class="btn btn-block" id="reset_map2">
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                        <input type="button" value="Back" class="btn btn-danger btn-block direction_btnback">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div id="right-panel">
                                                            <p>Total Distance: <span id="total"></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Map End-->

                                            <!-- Room Types Accord START -->

                                            <!-- Room Types Accord End -->

                                            <!-- Matching Tab Start-->
                                                          
                                                       
                                                      
                                       
                                            <ul class="pager">

                                                <li class="previous disabled"><a href="javascript:void(0)"><i class="fa fa-chevron-left"></i> Previous</a></li>

                                                <li class="next "><a href="../Ascott-Raffles-Place-Hotel.html">Next <i class="fa fa-chevron-right"></i></a></li>

                                            </ul>
                                            <!-- Bottom Pager Start -->
                                    </div>
                            </div>
                        </div>

                        <div id="dialog_container" title="Message dialog"></div>
                        <div id="dialog_msg" title="Message dialog"></div>

                        <!-- Comments start -->
                        <style type="text/css">
                            .clikable_final:hover {
                                cursor: pointer;
                                cursor: hand;
                            }
                        </style>
                      
                        <!-- Comments end -->
                    </div>
                </div>
                <!-- Content Start -->
            </div>
        </div>
        <!-- Inner Layout End -->

        <!-- Modal -->
        <div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                    Butuh Info Lebih Lanjut ? 
                </h4>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form class="form-horizontal" role="form" action="" method="POST">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3"><b>Agen</b></label>
                                <label class="col-sm-1 control-label" for="inputPassword3">
                                    <?php echo $img->asu; ?>
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3"><b>Avatar's Agen</b></label>
                                <label class="col-sm-1 control-label" for="inputPassword3">
                                    <img src="<?php echo base_url().$img->avatar; ?>" width="100px">
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3"><b>Telepon Agen</b></label>
                                <!-- <img src="<?php echo base_url().$image->image; ?>" width="100px"> -->
                                <label class="col-sm-1 control-label" for="inputPassword3">
                                    <?php echo $img->phone; ?>
                                </label>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail3">Nama Anda</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nama Anda" name="name_user" value="<?php echo $this->session->userdata('front_login') ? $this->session->userdata('front_name') : " "; ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputPassword3" placeholder="Email" name="email_user" value="<?php echo $this->session->userdata('front_login') ? $this->session->userdata('front_email') : " "; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputPassword3">Deskripsi</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="10" cols="10" name="msg_user" id="msg_user">
Saya menemukan properti ini pada www.travellingnow.com dan ingin informasi detail dari :
                                        <?php echo 'http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>


Mohon hubungi saya secepatnya. Trims
                                    </textarea>
                                </div>
                            </div>
                    </div>

                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary" id="hub_agen">
                            Hubungi Agent
                        </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Body End-->
    </body>

    </html>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".map-tab2").hide();
            $(".usingselect2").select2({
                allowclear: true
            });
            jQuery(document).ready(function() {
                jQuery("#gallery").unitegallery();
            });

            $('#direction_btn1').click(function() {
                $(".map-tab1").remove();
                $(".map-tab2").removeAttr("style");
                initialize2();
            });

            $('.direction_btnback').click(function() {
                $(".map-tab2").hide("slow");
                $(".map-tab1").show("slow");
                initialize();
            });

        });

        //--------------------------------INITIALIZE MAP || Map 1--------------------------------------------
        var map;

        function initialize() {

            var map = new google.maps.Map(
                document.getElementById('map-canvas'), {
                    center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                    zoom: 13,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            // var myIcon = {
            //       url: "https://image.flaticon.com/icons/svg/272/272876.svg", //url
            //       size: new google.maps.Size(75, 75),
            //       scaledSize: new google.maps.Size(75, 75)
            //     };

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                map: map
                    // icon: myIcon
            });

            var contentString = '<?php echo $title; ?>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });

        }
        google.maps.event.addDomListener(window, 'load', initialize);

        $('#reset_map1').on('click', initialize);

        //------------------------------------END INITIALIZE MAP--------------------------------------------

        //-----------------------------------MAP FUNCTION-----------------------------------------------------

        /*CUSTOM FUNC*/

        /*----------------------MAP Waypoint || Map 2-------------------*/
        var infowindow;
        var map2;

        function initialize2() {
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: true,
                map: map2,
                panel: document.getElementById('right-panel')
            });

            directionsDisplay.addListener('directions_changed', function() {
                computeTotalDistance(directionsDisplay.getDirections());
            });

            function computeTotalDistance(result) {
                var total = 0;
                var myroute = result.routes[0];
                for (var i = 0; i < myroute.legs.length; i++) {
                    total += myroute.legs[i].distance.value;
                }
                total = total / 1000;
                document.getElementById('total').innerHTML = total + ' km';
            }

            var map2 = new google.maps.Map(document.getElementById('map-canvas2'), {
                zoom: 14,
                center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>)
            });
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                map: map2
            });
            var contentString = '<?php echo $title; ?>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function() {
                infowindow.open(map2, marker);
            });

            directionsDisplay.setMap(map2);

            document.getElementById('direction_btn').addEventListener('click', function() {
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            });

            document.getElementById('reset_map2').addEventListener('click', function() {
                directionsDisplay.setMap(null);
            });
        }

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            var selectedMode = document.getElementById('travelmode').value;
            var waypts = [];
            var checkboxArray = document.getElementById('waypoints');
            for (var i = 0; i < checkboxArray.length; i++) {
                if (checkboxArray.options[i].selected) {
                    waypts.push({
                        location: checkboxArray[i].value,
                        stopover: true
                    });
                }
            }

            directionsService.route({
                origin: document.getElementById('start').value,
                destination: document.getElementById('end').value,
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: google.maps.TravelMode[selectedMode]
            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                    var route = response.routes[0];
                    // var summaryPanel = document.getElementById('directions-panel');
                    // summaryPanel.innerHTML = '';
                    // For each route, display summary information.
                    // for (var i = 0; i < route.legs.length; i++) {
                    //   var routeSegment = i + 1;
                    //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                    //       '</b><br>';
                    //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                    //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                    //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                    // }
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize2);

        /*-----------------------NearBy Place Search-------------------------*/
        $("#nearby").click(function() {
            var location_nearby = $("#location_nearby").val()

            $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>blog/nearby_search",
                data: {
                    location_nearby: location_nearby
                },
                beforeSend: function() {},
                success: function(data) {
                    var map;
                    var infowindow;

                    function initialize() {
                        var pyrmont = {
                            lat: <?php echo $lat; ?>,
                            lng: <?php echo $long; ?>
                        };

                        map = new google.maps.Map(document.getElementById('map-canvas'), {
                            center: pyrmont,
                            zoom: 15
                        });

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                            map: map
                        });

                        infowindow = new google.maps.InfoWindow();
                        var service = new google.maps.places.PlacesService(map);
                        var input = $("#location_nearby").val();
                        service.nearbySearch({
                            location: pyrmont,
                            radius: 550,
                            type: [input]
                        }, callback);
                    }

                    function callback(results, status) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            for (var i = 0; i < results.length; i++) {
                                createMarker(results[i]);
                            }
                        }
                    }

                    function createMarker(place) {
                        var image = {
                            url: place.icon,
                            size: new google.maps.Size(35, 31),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(17, 34),
                            scaledSize: new google.maps.Size(35, 35)
                        };
                        var placeLoc = place.geometry.location;
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: image,
                            position: place.geometry.location
                        });

                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.setContent(place.name);
                            infowindow.open(map, this);
                        });
                    }

                    initialize();

                }
            });

            ////////////////////////////////////
            ////////////CUSTOM JS//////////////
          

        });
    </script>
<script type="text/javascript" src="<?php echo $this->config->item('socker_url').'/socket.io/socket.io.js'; ?>"></script>
    <script type="text/javascript">
        var url         = '<?php echo base_url(); ?>';
        var surl        = '<?php echo $this->config->item('socker_url'); ?>';
        var room        = 'public';
        var users_id    = '<?php echo $this->session->userdata('front_userid'); ?>';
        var email     = '<?php echo $this->session->userdata('front_email'); ?>';
        var message_input = $('.message_input').val();
        var socket      = io.connect(surl);
        socket.emit('join:room', {'room_name' : room, 'email' : email, 'id' : users_id});

        // socket.on('message', function(data){ 
        //     var $message;
        //     var message_side = 'left';

        //     $message = $($('.message_template').clone().html());
        //     $message.addClass(message_side).find('.text').html(data.content);
        //     $message.addClass('appeared');
        //     $message.find('.avatar').css('background-image', 'url(' + data.avatar + ')');
        //     $('.messages').append($message);
        //     $('.messages').animate({ scrollTop: $('.messages').prop('scrollHeight') }, 300);
        //     // var audio = document.getElementById("audio");
        //     // audio.play();
        // });

        function send_message(text, group)
        {
        $.ajax({
            url     : url+'user/chat/chat_agen/<?php echo $image->created_by;?>',
            data    : 'group='+group+'&content='+text,
            type    : 'POST',
            dataType: 'JSON',
            beforeSend: function()
            {
                
            },
            success: function(message)
            {   
                // console.log(message);
                if(message.status)
                {
                    socket.emit('send:message', message);

                    $('.message_input').val('');
                    var $message;
                    var message_side = 'right';

                    $message = $($('.message_template').clone().html());

                    $message.addClass(message_side).find('.text').html(message.content);
                    $message.addClass('appeared');

                    $message.find('.avatar').css('background-image', 'url(' + message.avatar + ')');
                    $('.messages').append($message);
                    $('.messages').animate({ scrollTop: $('.messages').prop('scrollHeight') }, 300);
                    // socket.emit('wrote message',{'msg' : $('.message_input').val(), 'email' : email});

                    // place_lastupdate = $message.find('.lasttime');
                    // place_lastupdate.attr('id', "chat_"+message.id);
                }
                alert('Your Message Has Been Sent To Agent !');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }         
        });
            return false;
        }

        function get_chats(group)
        {
        $.ajax({
            url     : url+'user/get_chats',
            data    : 'group='+group,
            type    : 'POST',
            dataType: 'JSON',
            beforeSend: function()
            {
                
            },
            success: function(message)
            {   
                $.each( message.data, function( key, value ) {
                    var $message;
                    value_userid = value.users_id;
                    var message_side = users_id == value.users_id ? 'right' : 'left';
            
                    $message = $($('.message_template').clone().html());
                    $message.addClass(message_side).find('.text').html(value.content);

                    if (users_id == value.users_id) {
                        place_lastupdate = $message.find('.lasttime');
                        var lastupdate_day  = value.day;
                        var lastupdate_our  = value.our;
                        var lastupdate_minute = value.minute;
                        var lastupdate_second = value.second;
                        var today = new Date();
                        var h = today.getHours();
                        var m = today.getMinutes();
                        var s = today.getSeconds();
                        today = h+":"+m;

                        if(lastupdate_day > 0) place_lastupdate.html('Terakhir Dilihat '+lastupdate_day+' hari yang lalu ');
                        if(lastupdate_day == 0 && lastupdate_our > 0) place_lastupdate.html('Terakhir Dilihat '+lastupdate_our+' jam yang lalu');
                        if(lastupdate_day == 0 && lastupdate_our == 0 && lastupdate_minute > 0 ) place_lastupdate.html('Terakhir Dilihat '+lastupdate_minute+' menit yang lalu');
                        if(lastupdate_day == 0 && lastupdate_our == 0 && lastupdate_minute == 0 && lastupdate_second > 0) place_lastupdate.html('Terakhir Dilihat '+lastupdate_second+' detik yang lalu');
                        if(lastupdate_day == 0 && lastupdate_our == 0 && lastupdate_minute == 0 && lastupdate_second == 0 ) place_lastupdate.html('Telah Dilihat pada '+today);

                        place_lastupdate.attr('id', "chat_"+value.id);
                        
                    }

                    $message.addClass('appeared');
                    $message.find('.avatar').css('background-image', 'url(' + value.image + ')');

                    $('.messages').append($message);

                }); 

                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }         
        });
        return false;
    }

    $(document).on('click', '#hub_agen', function(e){
        e.preventDefault();
        var message = $('#msg_user').val();
        send_message(message, room);
    });

    </script>