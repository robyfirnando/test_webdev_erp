<link rel="stylesheet" type="text/css" href="http://demos.creative-tim.com/light-bootstrap-dashboard-pro/assets/css/demo.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/pc/css/light-bootstrap-dashboard.css">
<link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/template.min.css" media="screen" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!-- JS  -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyB1_eSUPKVxRMxI7lgnmeeuZzaITaYQqN4&language=id&libraries=places" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
<title>Find Tour By Map</title>
<div class="content">
    <div class="container-fluid">
         <br>
        <div class="panel panel-default" id="direction_panel" style="background-color:#0773E0;"> 
            <div class="panel-body">
              <div id="form1">
                    <div class="col-md-3">
                        <div class="form-group">
                             <input id="origin-input" class="form-control" type="text" placeholder="Enter an origin location">
                        </div>
                    </div>

                    <div class="col-md-2">
                        <!-- /.form-group -->
                        <div class="form-group">
                            <div class="input-group location">
                            <select multiple class="usingselect2 form-control" id="waypoints" style="width:200px;">
                                <?php
                                      foreach($get_locations as $us):
                                ?>
                                    <option value="<?php echo $us->lat.','.$us->long; ?>" coord="<?php echo $us->lat.','.$us->long; ?>">
                                        <?php echo ucwords(str_replace("-", " ", $us->title)); ?>
                                    </option>
                                    <?php
                                        endforeach;
                                    ?>
                            </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" class="typeahead form-control" placeholder="Destination" autocomplete="off" spellcheck="false" name="destination" id="end">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                        <div class="btn btn-primary" id="travelmode_driving" style="filter: brightness(0) invert(1);"><img src="http://maps.gstatic.com/mapfiles/transit/iw2/6/drive.png" width="15px"></div>
                    
                        <div class="btn btn-primary" id="travelmode_transit" style="filter: brightness(0) invert(1);"><img src="http://maps.gstatic.com/mapfiles/transit/iw2/6/bus.png" width="15px"></div>

                        <div class="btn btn-primary" id="travelmode_walking" style="filter: brightness(0) invert(1);"><img src="http://maps.gstatic.com/mapfiles/transit/iw2/6/walk.png" width="15px"></div>
                        <div class="btn btn-danger" id="reset_direction"><i class="fa fa-undo"></i></div>
                </div>
                        <input type="button" value="Get Direction" class="btn-primary btn btn-global btn-block" id="direction_btn" style="display:none;">
                </div>
        </div>

                <div class="container-fluid">
                    <div class="row">
                        <!-- Map Canvas-->
                        <div class="map-canvas list-width-30">
                            <!-- Map -->
                            <div class="map">
                                <div class="toggle-navigation">
                                    <div class="icon">
                                        <div class="line"></div>
                                        <div class="line"></div>
                                        <div class="line"></div>
                                    </div>
                                </div>
                                <!--/.toggle-navigation-->
                                <div id="map-canvas" class="has-parallax"></div>
                                <!--/#map-->
                                <div class="search-bar horizontal">
                                    <form class="main-search border-less-inputs" role="form">
                                            <div class="input-row">
                                                <div class="col-md-4">
                                                    <input type="text" class="typeahead form-control" placeholder="Search Place" autocomplete="off" spellcheck="false" name="destination" id="search">
                                                </div>

                                                <div class="col-md-4">
                                                     <a class="btn btn-primary" id="direction-btn">Direction</a>
                                                     <!-- <a class="btn btn-primary" id="wheres-me">Where's Me ?</a> -->
                                                </div>

                                                <div class="col-md-4">
                                                     <select class="form-control" id="filter">
                                                         <option value="all">All</option>
                                                         <option value="wisata">Wisata</option>
                                                         <option value="penginapan">Penginapan</option>
                                                     </select>
                                                </div>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                            </div>
                                            <!-- /.form-group -->
                                    </form>
                                    <!-- /.main-search -->
                                </div>
                                <!-- /.search-bar -->
                            </div>
                                <!-- end Map -->
                                <!--Items List-->
                                <div class="items-list">
                                    <div class="inner">
                                        <header>
                                            <h3>Results</h3>
                                        </header>
                                        <div id="right-panel">
                                        </div>
                                         <!-- <p>Total Distance: <span id="total"></span></p> -->
                                        </div>
                                        <br>
                                    </div>
                                    <!--results-->
                        </div>
                    </div>
                </div>
    </div>
</div>
<script>
$(document).ready(function() {

        var map;
        var directionsService;
        var directionsDisplay;
        var end;
        var marker_wisata;
        var marker_penginapan;
        var traffic_mode = "macet";
        var trafficLayer;
        var originAutocomplete;
        var method;
        var gmarkers=[];
        var markerCluster;

  $(window).on('load', function(){
        get_marker_wisata();
        get_marker_penginapan();
    });

    $("#direction_panel").hide();

    $("#direction-btn").click(function(){
        $("#direction_panel").show(300);
    });

    $("#wheres-me").click(function(){
        $("#direction_panel").show(300);
          if (navigator.geolocation) {
               navigator.geolocation.getCurrentPosition(function(position) {
                 var pos = {
                   lat: position.coords.latitude,
                   lng: position.coords.longitude
                 };
                 map.setCenter(pos);
                 var marker = new google.maps.Marker({
                   position: pos,
                   map: map,
                   title: String(pos.lat) + ", " + String(pos.lng),
                 });
               }, function() {
                 handleLocationError(true, false, map.getCenter());
               });
             } else {
               // Browser doesn't support Geolocation
               handleLocationError(false, false, map.getCenter());
             }

        $("#origin-input").val(String(pos.lat) + ", " + String(pos.lng));

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                var waypts = [];
                var checkboxArray = document.getElementById('waypoints');
                for (var i = 0; i < checkboxArray.length; i++) {
                    if (checkboxArray.options[i].selected) {
                        waypts.push({
                            location: checkboxArray[i].value,
                            stopover: true
                        });
                    }
                }

                directionsService.route({
                    origin: String(pos.lat) + ", " + String(pos.lng),
                    destination: end,
                    waypoints: waypts,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING

                }, function(response, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                        var route = response.routes[0];
                       
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }

            calculateAndDisplayRoute(directionsService, directionsDisplay);


      
    });

        //select2
        $(".usingselect2").select2({
                placeholder: "Your Waypoint",
                allowclear: true,
                maximumSelectionLength: 3
        });

         $( "#travelmode_driving" ).click(function(){
                function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                // var selectedMode = document.getElementById('travelmode').value;
                var waypts = [];
                var checkboxArray = document.getElementById('waypoints');
                for (var i = 0; i < checkboxArray.length; i++) {
                    if (checkboxArray.options[i].selected) {
                        waypts.push({
                            location: checkboxArray[i].value,
                            stopover: true
                        });
                    }
                }

                directionsService.route({
                    origin: document.getElementById('origin-input').value,
                    destination: end,
                    waypoints: waypts,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.DRIVING

                }, function(response, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                        var route = response.routes[0];
                        // var summaryPanel = document.getElementById('directions-panel');
                        // summaryPanel.innerHTML = '';
                        // For each route, display summary information.
                        // for (var i = 0; i < route.legs.length; i++) {
                        //   var routeSegment = i + 1;
                        //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                        //       '</b><br>';
                        //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                        //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                        //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                        // }
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }
            calculateAndDisplayRoute(directionsService, directionsDisplay);
          });

          $( "#travelmode_walking" ).click(function(){
                function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                // var selectedMode = document.getElementById('travelmode').value;
                var waypts = [];
                var checkboxArray = document.getElementById('waypoints');
                for (var i = 0; i < checkboxArray.length; i++) {
                    if (checkboxArray.options[i].selected) {
                        waypts.push({
                            location: checkboxArray[i].value,
                            stopover: true
                        });
                    }
                }

                directionsService.route({
                    origin: document.getElementById('origin-input').value,
                    destination: end,
                    waypoints: waypts,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.WALKING

                }, function(response, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                        var route = response.routes[0];
                        // var summaryPanel = document.getElementById('directions-panel');
                        // summaryPanel.innerHTML = '';
                        // For each route, display summary information.
                        // for (var i = 0; i < route.legs.length; i++) {
                        //   var routeSegment = i + 1;
                        //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                        //       '</b><br>';
                        //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                        //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                        //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                        // }
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }
            calculateAndDisplayRoute(directionsService, directionsDisplay);
          });

           $( "#travelmode_transit" ).click(function(){
                function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                // var selectedMode = document.getElementById('travelmode').value;
                var waypts = [];
                var checkboxArray = document.getElementById('waypoints');
                for (var i = 0; i < checkboxArray.length; i++) {
                    if (checkboxArray.options[i].selected) {
                        waypts.push({
                            location: checkboxArray[i].value,
                            stopover: true
                        });
                    }
                }

                directionsService.route({
                    origin: document.getElementById('origin-input').value,
                    destination: end,
                    waypoints: waypts,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.TRANSIT

                }, function(response, status) {
                    if (status === 'OK') {
                        directionsDisplay.setDirections(response);
                        var route = response.routes[0];
                        // var summaryPanel = document.getElementById('directions-panel');
                        // summaryPanel.innerHTML = '';
                        // For each route, display summary information.
                        // for (var i = 0; i < route.legs.length; i++) {
                        //   var routeSegment = i + 1;
                        //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                        //       '</b><br>';
                        //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                        //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                        //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                        // }
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }
            calculateAndDisplayRoute(directionsService, directionsDisplay);
          });


        // initialize map direction
        function initialize() {


            directionsService = new google.maps.DirectionsService;
            directionsDisplay = new google.maps.DirectionsRenderer({
                draggable: true,
                map: map,
                panel: document.getElementById('right-panel')
            });


            var mapOptions = {
                zoom: 8,
                scrollwheel: false,
                center: new google.maps.LatLng(-7.90582, 112.5047944),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            
            google.maps.event.addListener(map, 'click', function(event){
                this.setOptions({scrollwheel:true});
            });


            function computeTotalDistance(result) {
                var total = 0;
                var myroute = result.routes[0];
                for (var i = 0; i < myroute.legs.length; i++) {
                    total += myroute.legs[i].distance.value;
                }
                total = total / 1000;
                document.getElementById('total').innerHTML = total + ' km';
            }

            directionsDisplay.setMap(map);

            document.getElementById('direction_btn').addEventListener('click', function() {
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            });

            new AutocompleteDirectionsHandler(map);

            function AutocompleteDirectionsHandler(map) {
                this.map = map;
                this.originPlaceId = null;
                this.destinationPlaceId = null;
                // this.travelMode = 'WALKING';
                var originInput = document.getElementById('origin-input');
                // var destinationInput = document.getElementById('end');
                var modeSelector = document.getElementById('travelmode');
                this.directionsService = new google.maps.DirectionsService;
                this.directionsDisplay = new google.maps.DirectionsRenderer;
                this.directionsDisplay.setMap(map);

                var originAutocomplete = new google.maps.places.Autocomplete(
                    originInput, {
                        placeIdOnly: true
                    });


                 directionsDisplay.setPanel(document.getElementById('right-panel'));

            }

            $("#origin-input").change(function(){
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            });

            $("#waypoints").bind('change',function(){
                var optionObj = jQuery(this).find("option:selected");
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            });

            // AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
            //     var me = this;
            //     autocomplete.bindTo('bounds', this.map);
            //     autocomplete.addListener('place_changed', function() {
            //       var place = autocomplete.getPlace();
            //       if (!place.place_id) {
            //         window.alert("Please select an option from the dropdown list.");
            //         return;
            //       }
            //       if (mode === 'ORIG') {
            //         me.originPlaceId = place.place_id;
            //       } else {
            //         me.destinationPlaceId = place.place_id;
            //       }
            //       me.route();
            //     });

            //   };
         
        google.maps.event.addListener($("#origin-input"), 'place_changed', function() {
            alert("ok");
        });





    // function getPotisionMarkerbyContainer(marker, data)
    // {
    //     map.addListener('dragend', function() {
    //         if(locked_maps == true){
    //             var cnt = 0;
    //             $('.loadingblur').show();
    //             $('#property_place').html("");

    //             jQuery.each( marker, function( i, mapcluster ) {
    //                 if (map.getBounds().contains(mapcluster.getPosition())){
    //                     var html = position_append_data(mapcluster);
    //                     $('#property_place').append(html);
    //                     cnt = cnt + 1;
    //                 }
    //             });

    //             setTimeout(function(){
    //               $('.loadingblur').hide();
    //             }, 300);

    //         }
    //     });
    // }

        // $( "#traffic_mode" ).click(function(){
        //     // var img = document.createElement('img');
        //     // $(this).css("opacity","1.0");
        //     if (traffic_mode == "macet") {
        //         trafficLayer = new google.maps.TrafficLayer();
        //         trafficLayer.setMap(map);
        //         traffic_mode = "gkmacet";
        //     }else{
        //         trafficLayer.setMap(null);  
        //         traffic_mode = "macet";              
        //     }   
            
        //   });


        //  $( "traffic_mode" ).click(function() {
        //     $( this ).toggleClass( "highlight" );
        // });
        }

    

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            // var selectedMode = document.getElementById('travelmode').value;
            var waypts = [];
            var checkboxArray = document.getElementById('waypoints');
            for (var i = 0; i < checkboxArray.length; i++) {
                if (checkboxArray.options[i].selected) {
                    waypts.push({
                        location: checkboxArray[i].value,
                        stopover: true
                    });
                }
            }


    
    
            directionsService.route({
                origin: document.getElementById('origin-input').value,
                destination: end,
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: 'DRIVING'
                // travelMode: google.maps.TravelMode[method]

            }, function(response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                    var route = response.routes[0];
                    // var summaryPanel = document.getElementById('directions-panel');
                    // summaryPanel.innerHTML = '';
                    // For each route, display summary information.
                    // for (var i = 0; i < route.legs.length; i++) {
                    //   var routeSegment = i + 1;
                    //   summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                    //       '</b><br>';
                    //   summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                    //   summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                    //   summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                    // }
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });

            
            $("#reset_direction").click(function(){
                directionsDisplay.setMap(null);
                $("#origin-input").val('');
                $("#end").val('');
                $("#waypoints").val('');
            });
        }




   /* Search Marker By Autocomplete */
        var site = '<?php echo base_url(); ?>';
        $('#search').typeahead({
            source: function(query, process) {
                // var a = $("#filterdestination").val(); 

                $.ajax({
                    url: site + 'find_place/json_search/',
                    type: 'POST',
                    dataType: 'JSON',
                    data: 'query=' + query,
                    autoSelect: true,
                    minLength: 2,
                    delay: 400,
                    success: function(data) {
                             process(data);
                    }
                });
            },
            highlighter: function(item) {
                // Split JSON Array into multiple pieces of data from a database
                var object = item.split('#'),
                body = '<div class="typeahead">';
                body += '<div class="media">'
                body += '<div class="media-body">';
                body += '<span>' + object[0] + '</span>' + '<span style="font-size:7pt" ></span></p>';
                body += '</div>';
                body += '</div>';
                return body;
            },
            updater: function(item) {
                var object = item.split('#');
                latwis = parseFloat(object[1]);
                longwis = parseFloat(object[2]);

                var center = latwis + ',' + longwis;
                console.log(end);
        
                   map.setCenter(new google.maps.LatLng(latwis, longwis));
                   map.setZoom(17);
                  // infowindowContent.children['place-name'].textContent = place.name;
                  // infowindowContent.children['place-id'].textContent = place.place_id;
                  // infowindowContent.children['place-address'].textContent =
                  //     place.formatted_address;
                  infowindow.open(map, marker);
              
                   
                return object[0];
            },
        });

        /* Direction By Autocomplete */
        var site = '<?php echo base_url(); ?>';
        $('#end').typeahead({
            source: function(query, process) {
                // var a = $("#filterdestination").val();
                $.ajax({
                    url: site + 'find_place/json_search/',
                    type: 'POST',
                    dataType: 'JSON',
                    data: 'query=' + query,
                    autoSelect: true,
                    minLength: 2,
                    delay: 400,
                    success: function(data) {
                             process(data);
                    }
                });
            },
            highlighter: function(item) {
                // Split JSON Array into multiple pieces of data from a database
                var object = item.split('#'),
                html2 = '<div class="typeahead">';
                html2 += '<div class="media">'
                html2 += '<div class="media-body">';
                html2 += '<span>' + object[0] + '</span>' + '<span style="font-size:7pt" ></span></p>';
                html2 += '</div>';
                html2 += '</div>';
                return html2;
            },
            updater: function(item) {
                var object = item.split('#');
                latwis = parseFloat(object[1]);
                longwis = parseFloat(object[2]);

                end = latwis + ',' + longwis;
                console.log(end);

                calculateAndDisplayRoute(directionsService, directionsDisplay);

                return object[0];
            },
        });


 /* get marker penginapan from database*/
     function get_marker_penginapan(){
        var siteurl = "<?php echo base_url(); ?>find_place/get_data_penginapan";
        var activeInfoWindow;  
        $.ajax({
            url: siteurl,
            type: "POST",
            dataType: "JSON",
            beforeSend: function() {

            },
            success: function(data) {
                infowindow = new google.maps.InfoWindow();
                var jsonwisata = data.get_all_penginapan;
                // Creating a global infoWindow object that will be reused by all markers
                // var marker;
                // Looping through the JSON data
                for (var i = 0, length = jsonwisata.length; i < length; i++) {
                    var data = jsonwisata[i],
                        latLng = new google.maps.LatLng(data.lat, data.long)
                       
                    // Creating a marker and putting it on the map 
                    marker_penginapan = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        icon: '<?php echo base_url(); ?>'+'assets/marker/hotels.png',
                        animation: google.maps.Animation.DROP
                    });

                

                    // Creating a closure to retain the correct data, notice how I pass the current data in the loop into the closure (marker, data)
                    (function(marker_penginapan, data) {
                     
                        var contentString = '<b><center>' + data.title + '</center></b><br>' + data.description.substring(0, 200) + ' ...' + '<br>' + '<a href=<?php echo base_url(); ?>blog/detail_inap/' + data.slug + ' target="_blank">Selengkapnya .. </a>';

                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });

                        // Attaching a click event to the current marker
                        google.maps.event.addListener(marker_penginapan, "click", function(e) {

                            if(activeInfoWindow != null) activeInfoWindow.close();

                            // Open InfoWindow
                            infowindow.open(map, marker_penginapan);

                            // Store new open InfoWindow in global variable
                            activeInfoWindow = infowindow;
                            infowindow.open(map, marker_penginapan);

                            map.setZoom(18);
                            map.setCenter({lat:data.lat, lng:data.long});

                            document.getElementById("info-detail").innerHTML = data.title+'<br>'+'<br>'+data.description.substring(0, 200)+'...';
                        });

                    })(marker_penginapan, data);
                    gmarkers.push(marker_penginapan);

                }
                   markerCluster = new MarkerClusterer(map, gmarkers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

            }

        });
     }
     /* get marker wisata from database*/
    function get_marker_wisata(){
        var siteurl = "<?php echo base_url(); ?>find_place/get_data_wisata";
        var activeInfoWindow;  
        $.ajax({
            url: siteurl,
            type: "POST",
            dataType: "JSON",
            beforeSend: function() {

            },
            success: function(data) {
                infowindow = new google.maps.InfoWindow();
                var jsonwisata = data.get_all_wisata;
                // Creating a global infoWindow object that will be reused by all markers
                // var marker;
                // Looping through the JSON data
                for (var i = 0, length = jsonwisata.length; i < length; i++) {
                    var data = jsonwisata[i],
                        latLng = new google.maps.LatLng(data.lat, data.long)
                       
                    // Creating a marker and putting it on the map 
                    marker_wisata = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        icon: '<?php echo base_url(); ?>'+'assets/marker/travel.png',
                        animation: google.maps.Animation.DROP
                    });


                    // Creating a closure to retain the correct data, notice how I pass the current data in the loop into the closure (marker, data)
                    (function(marker_wisata, data) {
                     
                        var contentString = '<b><center>' + data.title + '</center></b><br>' + data.description.substring(0, 200) + ' ...' + '<br>' + '<a href=<?php echo base_url(); ?>blog/detail/' + data.slug + ' target="_blank">Selengkapnya .. </a>';

                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });

                        // Attaching a click event to the current marker
                        google.maps.event.addListener(marker_wisata, "click", function(e) {

                            if(activeInfoWindow != null) activeInfoWindow.close();

                            // Open InfoWindow
                            infowindow.open(map, marker_wisata);

                            // Store new open InfoWindow in global variable
                            activeInfoWindow = infowindow;
                            infowindow.open(map, marker_wisata);

                            map.setZoom(18);
                            map.setCenter({lat:data.lat, lng:data.long});

                            document.getElementById("info-detail").innerHTML = data.title+'<br>'+'<br>'+data.description.substring(0, 200)+'...';
                        });

                    })(marker_wisata, data);
                    gmarkers.push(marker_wisata);
                }

         
            
            }
        });
 //filter begin
    $('#filter').on('change', function() {
          if (this.value == 'all') {
            clear_all();
            get_marker_wisata();
            get_marker_penginapan();
          }else if(this.value == 'wisata'){
            clear_all();
            get_marker_wisata();
          }else{
            clear_all();
            get_marker_penginapan();
          }
    
    });

    }
    
    function clear_all() { 
        for (var i = 0; i < gmarkers.length; i++ ) { 
            gmarkers[i].setMap(null); 
        }

        gmarkers = [];

        markerCluster.clearMarkers();
    }
   
        google.maps.event.addDomListener(window, 'load', initialize);

    });
</script>

<style type="text/css">
    .pac-container:after {
        content: none !important;
    }
    .select2-container--default .select2-search--inline .select2-search__field {
        height:30px;
    }
    .highlight{
        opacity: 1.0;
    }
    .default{
        opacity:0.8;
    }

</style>