 <?php echo $script_captcha; // javascript captcha ?>
<!-- Body Start-->
<div class="container">
	    <!-- Inner Layout Start -->
    <div class="inner">
        <div class="row">
            <!-- Content Start -->
            <div class="col-md-12">
                <div class="drawer-overlay members">
                    <div id="fb-root"></div>
                        <div id="status"></div>
                            <div class="module front-login">
                                <section>
                                    <div class="trans-box">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2">
                                                <div class="panel panel-default">
                                                    <div class="panel-body login-box">
                                                        <h4>Please Enter Your Login Details</h4>

                                                        <div id="actionMessage">
                                                            <?php if(isset($message)): ?>
                                                                <div class="alert alert-danger">
                                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                    <?php echo $message ?>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if(isset($success_message)): ?>
                                                                <div class="alert alert-success">
                                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                                    <?php echo $success_message ?>
                                                                </div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <div id="login_box" style="display:">
                                                            <form method="POST" action="" name="formAction" id="formAction" role="form" class=""><input type="hidden" name="security" value="a4f2e25ab2f5a242ae3697d3844a0a07-b787d346517c3ee789406782e0420d2d">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                                        <input type="email" name="email" placeholder="Username" class="form-control" value="">
                                                                    </div>
                                                                <div class="input-errors" name="firstBlock[username]_err"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                                                        <input type="password" name="password" placeholder="Password" class="form-control" value="">
                                                                    </div>
                                                                    <div class="input-errors" name="firstBlock[password]_err"></div>
                                                                </div>
                                                                 <div class="form-group">
                                                                    <?php echo $captcha; // tampilkan recaptcha ?>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="hidden" name="firstBlock&#x5B;remember_me&#x5D;" value="0"><input type="checkbox" name="firstBlock&#x5B;remember_me&#x5D;" class="" value="1"> <label for="firstBlock&#x5B;remember_me&#x5D;">Remember me !!!!!!!!</label>
                                                                    <div class="input-errors" name="firstBlock[remember_me]_err"></div>
                                                                </div>
                                                               
                                                            
                                                            <div class="text-center">
                                                                <div class="form-group">
                                                                    <input type="submit" name="login" value="LOGIN" class="login_send_btn btn btn-global btn-block"/>
                                                                </div>
                                                                <a href="<?php echo getFacebookLoginUrl(); ?>" id="fb_login_btn" class="btn btn-primary btn-fb btn-block facebook-login-action"><i class="fa fa-facebook"></i> Login with facebook</a>
                                                               <!--  <a href="<?php echo $login_url; ?>" style="background: #DD4B39;" class="btn btn-primary btn-fb btn-block facebook-login-action"><i class="fa fa-google"></i> Login with google</a> -->
                                                            </div>
                                                            <br>
                                                            <p><a href="/Frontend-Forget-Password" class="btn-link">Forgot Password?</a></p>

                                                            </form>
                                                           
                                                        </div>

                                                        <div id="logout_box" style="display:none;">

                                                            <div class="form-group">
                                                                <a class="btn btn-global btn-block" href="/Frontend-Logout">LOGOUT</a>
                                                                
                                                            </div>

                                                            <div><a href="/Members/dashboard"  id="mem_link">Member Area</a></div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <div class="well">
                                    <h1>Travellingnow.com</h1>
                                    <h4><ins>For New Member:</ins></h4>
                                                <ul>
                                            <li>If you are already a member, please use the form on right to login to our Members Section.</li><li>If you are not a member, please click on link below to signup. Registration is easy and FREE!</li>            </ul>
                                                <br>
                                    <div class="text-center">
                                        <a class="btn btn-global btn-lg" href="<?php echo base_url(); ?>registration/user">"Join Now FREE!"</a>
                                    </div>

                                    <hr>

                                    <h4><ins>For Existing Member:</ins></h4>
                                                <ul>
                                            <li>If you are already a Member, please use the form on the right, to login to your existing account and enjoy all Member Benefits.</li>            </ul>
                                        
                                    <p>Not ready to signup yet? Feel free to browse some other popular site sections!</p>    </div>
                            </div>

                                <div id="dialog_msg" title="Please Enter Your Login Details Dialog"></div>
                                    <div id="dialog_container" title="Processing........ Please wait." style="display:none">
                                    	<div class="progress progress-striped active">
                                    		<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            	<span class="sr-only">100% Complete</span>
                                            </div>
                                    	</div>
                                    </div>
                                    </div>
                                </div>
                                <!-- Content Start -->
                            </div>
                        </div>
                        <!-- Inner Layout End -->
                    </div>
                    <!-- Body End-->

<script type="text/javascript">
    $(document).ready(function(){
    $(".close").click(function(){
        $(".alert-danger").hide(300);
    });
});
</script>