		<div id="news-listing" class="container-fluid">								
			<div class="section">
				<div class="row">
					<div class="col-sm-9">
						<div id="site-content" class="site-content">
							<h1 class="section-title title">
							<?php
		                        $no     = 1;
		                        $data = array();
		                        foreach( $category as $cat ){
		                            $data[$no]['namecategory'] = $cat->namecategory;
		                        $no++;
		                        }

		                    ?> 
						<h1><?php echo "Penginapan ".ucfirst($cat->namecategory); ?></h1>
						<div class="middle-content" id="middle">								
							<div class="section">
									<div class="row" id="ajax_table">
										<?php
											foreach($category as $admin):
										?>
										     <input type="hidden" name="namecategory" id="namecategory" value="<?php echo $admin->namecategory; ?>">
										<?php 
											endforeach;
										?>
									</div>
								</div><!--/.lifestyle -->
							</div><!--/.middle-content-->
						</div><!--/#site-content-->
					<div class="load-more text-center">
						<button class="btn btn-primary btn-block" id="load_more" data-val="0">Load More Tours</button>
						<input type="hidden" name="limit" id="limit" value="2"/>
						<input type="hidden" name="offset" id="offset" value="10"/>
					</div>
					</div><br/>
					 <div class="col-md-3">
                <!-- Sidebar Start -->
                <div class="clearfix"></div>
                <div class="block block-subscribe">
                    <div class="block-body">
                        <h3>Email Subscription</h3>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-check-circle"></i> Don't miss out on our great offers</li>
                            <li><i class="fa fa-check-circle"></i> Receive deals and updated from our agents via e-mail</li>
                        </ul>

                        <p>Join our mailing list and win exciting offer, get special promotional offer and other services. Type your name and email address below for newsletter subscriptions.</p>

                        <div id="actionMessage_newsletter"></div>

                        <input type="hidden" id="newsletter_common_err_msg" name="newsletter_common_err_msg" value="There are some error to save this article. Please See below --" />

                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" action="" name="newsletter_form_box" id="newsletter_form_box" role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" required="required" value="" title="Enter Name" id="name">
                                            <div class="input-errors " name="name_err"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" name="email" class="form-control" placeholder="Enter Email" required="required" value="" title="Enter Email" id="email">
                                            <div class="input-errors " name="email_err"></div>
                                        </div>
                                    </div>

                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" name="submit" value="Subscribe Now" class="newsletter_snd_btn btn btn-global btn-block" id="save">
                                </div>
                            </div>

                            </form>
                        </div>

                        <div id="newsletter_loader_process" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></div>
                        <div id="sent" style="display:none;"><i class="fa fa-check-circle" aria-hidden="true">
        </i>Thank You For Subscribe Us !</div>

                    </div>
                </div>
				</div>	
			</div><!--/.section-->

		</div><!--/.container-fluid-->
</div>
		
		<div class="footer-top">
			<div class="container-fluid">
			
			</div>
		</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/frontend/vendor/jquery/jquery.min"></script> -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/libs/jquery/jquery-1.11.2.min.js" ></script>

<script>
    $(document).ready(function(){
    
    getCategoryPenginapan(0);

        $("#load_more").click(function(e){
            e.preventDefault();
            var category = $('#namecategory').val();
            var page = $(this).data('val');
            var data_string = 'page='+ page +'&category='+ category;
            getCategoryPenginapan(page);

        });

    });
	
	
	var page = $(this).data('val');
    var getCategoryPenginapan = function(page){
	var category = $('#namecategory').val();
    var page = $('#load_more').data('val');
    var data_string = 'page='+ page +'&category='+ category;
    var url 		= "<?php echo base_url('home/getCategoryPenginapan/'); ?>"
            
        $.ajax({
            url:url,
            type:'GET',
            data: data_string
        }).done(function(response){
            $("#ajax_table").append(response);
            $('#load_more').data('val', ($('#load_more').data('val')+1));            
        });
    };

$("#load_more").click(function(){
 		$("body").animate({scrollTop: $("#load_more").offset().top}, 900);
});

    // var scroll  = function(){
    //     $('#middle').animate({
    //         scrollTop: $('#load_more').offset().top
    //     }, 1000);
    // };


    function like (id) {
		$.ajax({
			url: "<?php echo site_url('home/like')?>/"+ id,
			type: "POST",
			dataType: "JSON",
			success: function(data){
				load();
			}				
			
		});
		
		
	}

	function load(){
		location.reload();
	}
    $(function() {
        $("#save").click(function(event) {
            event.preventDefault(); //prevent auto submit data

            var name = $("#name").val();
            var email = $("#email").val();
            //assign our rest of variables
            $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>home/subscription_add",
                data: {
                    name: name,
                    email: email
                },
                beforeSend: function() {
                    $("#newsletter_loader_process").show();
                },
                success: function(data) {
                    console.log(data);
                    $("#newsletter_loader_process").hide();
                    $("#sent").show();
                }
            });
        });
    });
</script>