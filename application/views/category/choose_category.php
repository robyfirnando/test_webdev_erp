<style type="text/css">
/* Shine */
.hover14 figure {
	position: relative;
}
.hover14 figure::before {
	position: absolute;
	top: 0;
	left: -75%;
	z-index: 2;
	display: block;
	content: '';
	width: 50%;
	height: 100%;
	background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
	background: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,.3) 100%);
	-webkit-transform: skewX(-25deg);
	transform: skewX(-25deg);
}
.hover14 figure:hover::before {
	-webkit-animation: shine .75s;
	animation: shine .75s;
}
@-webkit-keyframes shine {
	100% {
		left: 125%;
	}
}
@keyframes shine {
	100% {
		left: 125%;
	}
}
</style>

		<div id="news-listing" class="container-fluid">								
			<div class="section">
				<div class="row">
					<div class="col-sm-9">
						<div id="site-content" class="site-content">
							<h1 class="section-title title">
							<h1>Choose Category</h1>
						<a href="listing.html"></a></h1>
						<div class="middle-content" id="middle">								
							<div class="section">
							<center>
									<div class="row" id="ajax_table">
										<div class="col-md-6">
											<div class="hover14 column">
												<a href="<?php echo base_url(); ?>home/category/alam"><div>
													<figure><img src="http://www.viamigo.com/img/l/tour/379/bird-watching-and-nature-tours-from-san-francisco.jpg" /></figure>
													<span><h2>Alam</h2></span>
												</div></a>
											</div>
										</div>
										<div class="col-md-6"><h2>Buatan</h2></div>
									</div>
								</div><!--/.lifestyle -->
							</div><!--/.middle-content-->
						</div><!--/#site-content-->
					<div class="load-more text-center">
						<input type="hidden" name="limit" id="limit" value="2"/>
						<input type="hidden" name="offset" id="offset" value="10"/>
					</div>
					</div>
				
				</div>	
			</div><!--/.section-->

		</div><!--/.container-fluid-->
		
		<div class="footer-top">
			<div class="container-fluid">
			
			</div>
		</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/frontend/vendor/jquery/jquery.min"></script> -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/libs/jquery/jquery-1.11.2.min.js" ></script>

