<!-- Body Start-->
<div class="container">
	    <!-- Inner Layout Start -->
    <div class="inner">
        <div class="row">
            <!-- Content Start -->
            <div class="col-md-12">
                <div class="drawer-overlay members">
                    
                    <div class="module">
                        <h1>Choose User Role</h1>
                            <?php if($this->session->flashdata('success_message')): ?>
                            <div class="alert alert-success">
                                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
                                <?php echo $this->session->flashdata('success_message'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if($this->session->flashdata('message')):?>
                            <div class="alert alert-success">
                                <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif; ?>
                        <form method="POST" action="" name="formAction" id="formAction" role="form" class="form-horizontal">
                            <fieldset>
                                <legend>Select a Role :<span class="required">*</span></legend>

                                <div id="actionMessage"></div>
                                <div class="row">
                                    <div class="co-md-8 co-sm-8 co-xs-8 col-md-offset-2 col-sm-offset-4 col-xs-offset-4">
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="role_id" value="user" /> Registered User                            </label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="role_id" value="agent" /> Agent                            
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                 </div>

                            </fieldset>

                            <div class="text-center btn-groups">
                               
                            <button type="submit" name="submit_role" class="add_user_btn btn btn-primary">Go To Registeration</button>
                            </div>

                        </form>
                        </div>

                                <div id="dialog_msg" title="Choose User Role Dialog"></div>
                                        <div id="dialog_container" title="Processing........ Please wait." style="display:none">
                                        	<div class="progress progress-striped active">
                                        		<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                	<span class="sr-only">100% Complete</span>
                                                </div>
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Content Start -->
                            </div>
                        </div>
                        <!-- Inner Layout End -->
                    </div>
<!-- Body End-->

<script type="text/javascript">
$(function() {
    if ( ! $("input").is(':checked') ){
        $(".add_user_btn").prop('disabled', true);
    }

    $('input:radio[name="role_id"]').change(
    function(){
        if (this.checked) {
            $(".add_user_btn").prop('disabled', false);
        }else{
              $(".add_user_btn").prop('disabled', true);
        }
    });

    // setTimeout() function will be fired after page is loaded
    // it will wait for 5 sec. and then will fire
    // $("#successMessage").hide() function
    setTimeout(function() {
        $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>