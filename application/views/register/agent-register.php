<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Body Start-->
<div class="container">
        <!-- Inner Layout Start -->
    <div class="inner">
        <div class="row">

            <!-- Content Start -->
            <div class="col-md-12">
                <div class="drawer-overlay members">        
                    <div class="module" id="topGo">
                        <h1>Agent Registration</h1>
                        <div class="clearfix"></div>
                        <div id="actionMessage"></div>
                        <div class="input-errors " name="membershipInfoBlock[package_id]_err"></div>
                    </div>
                </div>
            
        </fieldset><fieldset>
            <?php if(isset($success_message)): ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <?php echo $success_message ?>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($message)):?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <?php echo $message ?>
                            </div>
            <?php endif; ?>

            <legend>Member Information</legend>
            <span class="top-go clearfix">
                
            </span><br>
               
            <form method="POST" name="user_register_form" id="user_register_form" enctype="multipart/form-data"> 
                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="fullname">Fullname :<span class="required">*</span></label>
                        <div class="col-sm-6 col-xs-10">
                            <input type="text" name="fullname" id="fullname" placeholder="Enter Fullname" class="form-control" title="Enter Fullname" required="required" value="<?php echo isset($_POST['fullname']) ? $_POST['fullname'] : ''; ?>">
                            <div class="input-errors " name="membersInfoBlock[first_name]_err"></div>
                        </div>
                        <div class="ui-widget ui-helper-clearfix info">
                            <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Full Name"><p><span class="ui-icon ui-icon-info"></span></p>
                            </div>
                        </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="fullname">Address :<span class="required">*</span></label>
                        <div class="col-sm-6 col-xs-10">
                            <input type="text" name="address" id="fullname" placeholder="Enter Address" class="form-control" title="Enter Address" required="required" value="<?php echo isset($_POST['address']) ? $_POST['address'] : ''; ?>">
                            <div class="input-errors " name="membersInfoBlock[first_name]_err"></div>
                        </div>
                        <div class="ui-widget ui-helper-clearfix info">
                            <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter First Name"><p><span class="ui-icon ui-icon-info"></span></p>
                            </div>
                        </div>
                </div>


               <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="email">Email :<span class="required">*</span></label>
                        <div class="col-sm-6 col-xs-10">
                            <input type="text" name="email" id="email" placeholder="Enter Email" class="form-control" title="Enter Email" required="required" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                            <div class="input-errors " name="membersInfoBlock[last_name]_err">
                            </div>
                        </div>

                        <div class="ui-widget ui-helper-clearfix info">
                            <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Email"><p><span class="ui-icon ui-icon-info"></span></p>
                            </div>
                        </div>
                </div>

                  <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="email">Password :<span class="required">*</span></label>
                        <div class="col-sm-6 col-xs-10">
                            <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control" title="Enter Password" required="required" value="<?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>">
                            <div class="input-errors " name="membersInfoBlock[last_name]_err">
                            </div>
                           

                        </div>


                        <div class="ui-widget ui-helper-clearfix info">
                             <button id="show" onclick="ShowPassword()"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button style="display:none" id="hide" value="Hide Password" onclick="HidePassword()">
                                <i class="fa fa-eye-slash" aria-hidden="true"></i>
                            </button>
                            </div>
                        </div>
                 </div>

       </fieldset><fieldset>
            <legend>Other Information</legend>
            <span class="top-go clearfix">
                <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a>
            </span><br>
               

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="fullname">Jenis Kelamin :<span class="required">*</span></label>
                    <div class="col-md-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="jk" value="laki-laki" id="jk" />  Laki-laki</label>
                        </div>
                    </div>
                      <div class="col-md-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="jk" value="perempuan" id="jk" />Perempuan</label>
                        </div>
                    </div>

                       
                        <div class="ui-widget ui-helper-clearfix info">
                            <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Jenis Kelamin"><p><span class="ui-icon ui-icon-info"></span></p>
                            </div>
                        </div>
                </div>


        </fieldset><fieldset>
            <legend>Contact Information</legend>
            <span class="top-go clearfix">
                <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a></span><br>
               

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="phone">Phone :<span class="required">*</span></label>
                        <div class="col-sm-6 col-xs-10">
                            <input type="text" name="phone" id="phone" placeholder="Enter Phone" class="form-control" title="Enter Phone" required="required" value="">
                            <div class="input-errors " name="membersInfoBlock[first_name]_err"></div>
                        </div>
                        <div class="ui-widget ui-helper-clearfix info">
                            <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Phone"><p><span class="ui-icon ui-icon-info"></span></p>
                            </div>
                        </div>
                </div>
                 </fieldset><fieldset>
            
            <legend>Upload Photo</legend>
            <span class="top-go clearfix">
                <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a>
            </span><br>
               

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="phone">Upload Photo :</label>
                        <div class="col-sm-6 col-xs-10">
                            <div class="form-group" id="place_image" style="display: none;">
                                  <img src="" id="image_category" style="width: 95px;height: 50px;">
                              </div>

                              <div class="form-group">
                                  <a class="btn btn-primary" id="btn_choose_image" onclick="$('#choose_image').click();">Choose Image</a>
                                  <input style="display: none;" type="file" id="choose_image" name="image"></input>
                              </div>
                        </div>
                        <div class="ui-widget ui-helper-clearfix info">
                            <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Photo"><p><span class="ui-icon ui-icon-info"></span></p>
                            </div>
                        </div>
                </div>

        
        <div class="clearfix"></div>

        <div class="text-center btn-groups bottom">
            <a class="reset_btn btn btn-success" onclick="resetForm()"><i class="fa fa-retweet"></i> Reset</a>
            <button type="submit" name="submit" class="save_data_btn btn btn-primary" id="submit_user">Register</button>      
        </div>
    </div>
</div>
</form>
                    <div id="dialog_msg" title="Registered User Sign Up Dialog"></div>
                    <div id="dialog_container" title="Processing........ Please wait." style="display:none">
                        <div class="progress progress-striped active">
                            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only">100% Complete</span>
                            </div>
                        </div>
                    </div>
                <div id="upload-form" title="File Upload Manager" style="display:none;">
                    <div>
                    </div>
                </div>
            <!-- Content Start -->
        </div>
    </div>
    <!-- Inner Layout End -->
</div>
<!-- Body End-->
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!--  <script type="text/javascript" src="https://raw.githubusercontent.com/davgothic/AjaxFileUpload/master/jquery.ajaxfileupload.js"></script> -->
<script type="text/javascript">
function resetForm() {
    document.getElementById("user_register_form").reset();
}
        $(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
        });
        function ShowPassword()
        {
            if(document.getElementById("password").value!="")
            {
                document.getElementById("password").type="text";
                document.getElementById("show").style.display="none";
                document.getElementById("hide").style.display="block";
            }
        }

        function HidePassword()
        {
            if(document.getElementById("password").type == "text")
            {
                document.getElementById("password").type="password"
                document.getElementById("show").style.display="block";
                document.getElementById("hide").style.display="none";
            }
        }

</script>