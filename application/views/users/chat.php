<script type="text/javascript" src="<?php echo base_url(); ?>assets/user/js/plugins/jquery-1.11.2.min.js"></script>  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/user/css/chat.css">
 <section id="content">
  <div class="container">
          <div class="section">
			<div class="row">
				<div class="col-md-4">
		            <div class="panel panel-success">
					    <div class="panel-heading"><span class="glyphicon glyphicon-user" id="sum_user"></span> Users Online</div>
					    <div class="panel-body">
					        <ul class="list-group" id="listaOnline">
					            
					        </ul>
					    </div>
					</div>
		        </div>
		    	<!-- <div class="btn" id="maximize"></div> -->

		        <div class="col-md-8">
		        	<div class="chat_window col-offset-4" id="chat_window">
					    <div class="top_menu">
					        <div class="buttons">
					            <div class="button close" id="minimize"></div>
					            <div class="button minimize"></div>
					            <div class="button maximize"></div>
					        </div>
					        	<div class="title">Chat</div>
					    </div>
					    <ul class="messages"></ul>
					    <div class="bottom_wrapper clearfix">
					    <h5 id="sedangnulis"></h5>
					        <div class="message_input_wrapper">
					            <input class="message_input" placeholder="Type your message here..." />
					        </div>
					        <div class="send_message">
					            <div class="icon"></div>
					            <div class="text">Send</div>
					        </div>
					    </div>
					</div>
					<div class="message_template">
					    <li class="message">
					        <div class="avatar"></div>
					        <div class="text_wrapper">
					            <div class="text"></div>
					            <h6 class="lasttime"></h6>
					        </div>
					    </li>
					</div>
		        </div>
				<!-- <audio id="audio" src="<?php echo base_url(); ?>assets/ringtone/pop.mp3" ></audio> -->
			</div>
		</div>
	</div>
</section>

    <!-- Socket IO -->
<script type="text/javascript" src="<?php echo $this->config->item('socker_url').'/socket.io/socket.io.js'; ?>"></script>
<script type="text/javascript">
	var url 		= '<?php echo base_url(); ?>';
	var surl        = '<?php echo $this->config->item('socker_url'); ?>';
	var room        = 'public';
	var users_id 	= '<?php echo $this->session->userdata('front_userid'); ?>';
	var message_input = $('.message_input').val();
	var user 		= [];
	var socket  	= io.connect(surl);
	var wrote;
	var loginwith	= '<?php echo $this->session->userdata('front_login'); ?>';
	var email 		= '<?php echo $this->session->userdata('front_email'); ?>';
	socket.emit('join:room', {'room_name' : room, 'email' : email, 'id' : users_id});

	$(window).on('load', function(){
			get_chats(room);
			// get_message(142);
	});


	  $(document).on('click', '.get_detail', function(){
    		// var room = $(this).attr('room');
    		var id;

    		socket.emit('join:room', {'room_name' : room});

    		$.ajax({
	        url 	: url+'chat/get_detail',
	        data    : 'room='+room+'&user_id='+id,
	        type 	: 'POST',
	        dataType: 'JSON',
	        beforeSend: function()
	        {
	        	$('#inbox-msg-box').show();
	        	$('#inbox-msg-box').find('.place_loader').html('<div class="loader"><div class="loader__figure"></div></div>');
	        	$('#inbox-msg-box #inbox-msg-content').find('ul').html("");
	        	$('#header_message_content').html("");
	        	$('#inbox-msg-content').removeClass();

	        },
	        success: function(message)
	        {

	        	if(message.counter > 0)
	        	{
	        		$('span.notif_message_count').show();
	        	}
	        	else
	        	{
	        		$('span.notif_message_count').hide();
	        	}

	        	if(message.notif_cnt > 0)
	        	{
	        		$('span.notif_all_count').show();
	        	}
	        	else
	        	{
	        		$('span.notif_all_count').hide();
	        	}

	        	$('span.notif_message_count').html(message.counter);
	        	$('span.notif_all_count').html(message.notif_cnt);
	        	$('#sidr ul li.msg_count span').html('Kotak Masuk ('+message.counter+')');
	        	$('#sidr ul li.msg_count').attr('count', message.counter);

	        	$('#inbox-msg-content').addClass('content_'+message.detail.id);
				$('#inbox-wrapper').find('#inbox-msg-field').find('input[name=room]').val(room);
				$('#inbox-wrapper').find('#inbox-msg-field').find('input[name=agent_id]').val(message.detail.id);
				$('#inbox-wrapper').find('#inbox-msg-field').find('input[name=to_type]').val(message.to_type);
				
				var last_seen = "";
				if(message.last_seen != "")
				{
					last_seen = ' Last seen '+ message.last_seen;
				}

				$('#header_message_content').html('<div id="inbox-header">'+
												   '<a href="#" class="closes-chat"><i class="icon icon-arrow_left"></i></a>'+
												   '<div class="res-active">'+
						                           '<h3 class="person-name heavy-360">'+message.detail.first_name + ' ' + message.detail.last_name+'</h3>'+
						                            '<span class="person-meta">'+message.detail.phone + last_seen + ' </span>'+
						                           '</div>'+
						                        '</div>');
				


	        	$.each( message.data, function( key, value ) {
	        		var img = "";
	        		if(value.span == "")
	        		{
	        			img = '<img class="person-avatar" src="'+value.user_photo+'" alt="">';
	        		}
	        		$('#inbox-msg-box #inbox-msg-content').find('ul').append('<li class="'+value.li+'">'+
												                                    img+
												                                    '<span class="msg-bubble '+value.span+'">'+
												                                        '<p class="msg-text">'+value.description+'</p>'+
												                                        '<span class="msg-date">'+value.date+'</span>'+
												                                    '</span>'+
												                                '</li>');
				});

	        	$('#inbox-wrapper #inbox-msg-box').find('#chatField').removeAttr('disabled');
	        	$('#inbox-wrapper #inbox-msg-box').find('.submitChat').removeAttr('disabled');
	        	$('#inbox-msg-field').show();
	        	$('.empty-placeholder').hide();
				$('#inbox-msg-box').find('.place_loader .loader').remove();


	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	        }         
	    });
	    return false;
    	});


	 socket.on('get users',function(data){

	 	console.log(data);
		var html = "";
		var cnt = 0;
		for(i=0; i<data.length; i++ ){
			html += '<li class="list-group-item" id='+data[i]+'><span class="label label-success">●</span> - '+data[i]+'</li>';
		}
			
		$('#listaOnline').html(html);
		// console.log(data);

	});
	

	socket.on('message', function(data){
			var $message;
			var message_side = 'left';

		    $message = $($('.message_template').clone().html());
		    $message.addClass(message_side).find('.text').html(data.content);
		    $message.addClass('appeared');
		    $message.find('.avatar').css('background-image', 'url(../' + data.avatar + ')');
		    $('.messages').append($message);
		    $('.messages').animate({ scrollTop: $('.messages').prop('scrollHeight') }, 300);
		    // var audio = document.getElementById("audio");
		   	// audio.play();
	});

    function send_message(text, group)
    {
    	$.ajax({
	        url 	: url+'User/chat/send',
	        data    : 'group='+group+'&content='+text,
	        type 	: 'POST',
	        dataType: 'JSON',
	        beforeSend: function()
	        {
	        	
	        },
	        success: function(message)
	        {	
        		if(message.status)
        		{
        			socket.emit('send:message', message);
        			// socket.emit('typingMsg', "");
	                

        			$('.message_input').val('');
        			var $message;
	        		var message_side = 'right';

	                $message = $($('.message_template').clone().html());
                

	                $message.addClass(message_side).find('.text').html(message.content);
	                $message.addClass('appeared');

	            	$message.find('.avatar').css('background-image', 'url(../' + message.avatar + ')');
	                $('.messages').append($message);
	                $('.messages').animate({ scrollTop: $('.messages').prop('scrollHeight') }, 300);
	                // socket.emit('wrote message',{'msg' : $('.message_input').val(), 'email' : email});
	                // place_lastupdate = $message.find('.lasttime');
	                // place_lastupdate.attr('id', "chat_"+message.id);
	                console.log(message.avatar);
        		}

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	        }         
	    });
	    return false;
    }

    function get_message(id)
    {
        $.ajax({
	        url 	: url+'Chat/get_inbox',
	        data    : 'user_id='+id,
	        type 	: 'POST',
	        dataType: 'JSON',
	        beforeSend: function()
	        {
	        	$('#inbox-msg-list').find('ul .place_loader').html('<div class="loader"><div class="loader__figure"></div></div>');
	        },
	        success: function(message)
	        {
	        	total_page = message.total_page;

	        	if(message.total_message > 0)
	        	{	
	        		var count = '';
	        		$.each( message.data, function( key, value ) {
	        			if(value.count > 0) 
						{
							count = '<label class="count_message">'+value.count+'</label>';
						}
		        		$('#inbox-msg-list').find('ul.inbox')
							.append('<li class="get_detail" room="'+value.room+'">'+
										'<a>'+
											'<img class="person-avatar" src="'+value.user_photo+'" alt="">'+
											'<span class="person-name heavy-360">'+value.user_name+'</span>'+
											'<p class="person-msg">'+value.description+'</p>'
											+ count  +
											'<div class="clearfix"></div>'+
										'</a>'+
									'</li>');
						count = '';
					});

	        	}else{
					$('#inbox-msg-content')
						.html('<div class="empty-placeholder">'+
								'<img height="150" src="https://www.rumah360.com/frontend-assets/dist/img/icons/placeholder-message.svg" alt="">'+
								'<h4>Anda belum memiliki obrolan.</h4>'+
								'<p>Temukan rumah impian anda dan mulai obrolan!</p>'+
								'<a href="'+url+'search" class="btn btn-360-green btn-360-md">Cari Properti</a>'+
							  '</div>');
					$('#chatField').prop('disabled', true);
					$('.submitChat').prop('disabled', true);
	        	}
	        	
	        	if(message.total_page > 0 && message.current_page != message.total_page){
	        		$('#inbox-msg-list').find('ul.load')
						.html('<li class="loadmore">'+
								'<a class="loadmore_message">'+
									'<center> Load more .. </center>'+
									'<div class="clearfix"></div>'+
								'</a>'+
							  '</li>');
	        	}

				$('#inbox-msg-list').find('ul .loader').remove();

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	        }         
	    });
	    return false;
    }

  	function get_chats(group)
    {
		        	console.log(group);
        $.ajax({
	        url 	: url+'User/chat/get_chats',
	        data    : 'group='+group,
	        type 	: 'POST',
	        dataType: 'JSON',
	        beforeSend: function()
	        {
	        	
	        },
	        success: function(message)
	        {	
	        	$.each( message.data, function( key, value ) {
	        		console.log(value.users_id);

	        		var $message;
	        		value_userid = value.users_id;
    				var message_side = users_id == value.users_id ? 'right' : 'left';
        	
    			 	$message = $($('.message_template').clone().html());
                	$message.addClass(message_side).find('.text').html(value.content);

        			if (users_id == value.users_id) {
	               		place_lastupdate = $message.find('.lasttime');
               		  	var lastupdate_day 	= value.day;
		                var lastupdate_our 	= value.our;
		                var lastupdate_minute = value.minute;
		                var lastupdate_second = value.second;
		                var today = new Date();
						var h = today.getHours();
						var m = today.getMinutes();
						var s = today.getSeconds();
						today = h+":"+m;

		                if(lastupdate_day > 0) place_lastupdate.html('Terakhir Dilihat '+lastupdate_day+' hari yang lalu ');
		                if(lastupdate_day == 0 && lastupdate_our > 0) place_lastupdate.html('Terakhir Dilihat '+lastupdate_our+' jam yang lalu');
		                if(lastupdate_day == 0 && lastupdate_our == 0 && lastupdate_minute > 0 ) place_lastupdate.html('Terakhir Dilihat '+lastupdate_minute+' menit yang lalu');
		                if(lastupdate_day == 0 && lastupdate_our == 0 && lastupdate_minute == 0 && lastupdate_second > 0) place_lastupdate.html('Terakhir Dilihat '+lastupdate_second+' detik yang lalu');
		                if(lastupdate_day == 0 && lastupdate_our == 0 && lastupdate_minute == 0 && lastupdate_second == 0 ) place_lastupdate.html('Telah Dilihat pada '+today);

		                place_lastupdate.attr('id', "chat_"+value.id);
		                
	                }

	             	$message.addClass('appeared');
		            $message.find('.avatar').css('background-image', 'url(../' + value.image + ')');

	                $('.messages').append($message);

	        	});	

	        	
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	        }         
	    });
	    return false;
    }



    $(document).on('click', '.send_message', function(e){
    	e.preventDefault();
    	var message = $('.message_input').val();
    	send_message(message, room);
    });

     $('.message_input').keyup(function (e) {
        if (e.which === 13) {
			var message = $('.message_input').val();
            send_message(message, room);
        }
        e.preventDefault();
		// socket.emit('wrote message',{'msg' : $('.message_input').val(), 'email' : email});
    });



</script>


<style type="text/css">
	/*#maximize{
		background-image: url(http://cdn.onlinewebfonts.com/svg/download_522400.png);
		width:20px;
		height: 25px;
		background-size: cover;
	}*/
</style>