<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <!-- START CONTENT -->
      <section id="content">

        <!--breadcrumbs start-->
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen -->
            <div class="header-search-wrapper grey hide-on-large-only">
                <i class="mdi-action-search active"></i>
                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
            </div>
          <div class="container">
            <div class="row">
              <div class="col s12 m12 l12">
                <h5 class="breadcrumbs-title">My Account</h5>
                <ol class="breadcrumbs">
                  <li><a href="index-2.html">Dashboard</a>
                  </li>
                  <li><a href="#">Forms</a>
                  </li>
                  <li class="active">Forms Elements</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!--breadcrumbs end-->


        <!--start container-->
        <div class="container">
          <div class="section">

            <div class="divider"></div>

            <!--Input fields-->
            <div id="input-fields">
              <div class="row">
                <div class="col s12 m4 l3">
                
                </div>
                <div class="col s12 m8 l9">
                  <div class="row">
                      <form method="POST" name="user_register_form" id="user_register_form" enctype="multipart/form-data" class="col s12">
                      <div class="row">
                        <div class="input-field col s12">
                          <input id="full_name" type="text" class="validate" value="<?php echo $account['name']; ?>" name="fullname">
                          <label for="full_name">Full Name</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <input id="address" type="text" class="validate" value="<?php echo $account['address']; ?>" name="address">
                          <label for="first_name">Address</label>
                        </div>
                      </div>

                      <div class="row">
                        <div class="input-field col s12">
                          <input id="address" type="text" class="validate" value="<?php echo $account['phone']; ?>" name="phone">
                          <label for="first_name">Phone</label>
                        </div>
                      </div>

                      <div class="row">
                        <div class="input-field col s12">
                          <input id="password" type="password" class="validate">
                          <label for="password">Password</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <input id="email" type="email" class="validate" name="email" value="<?php echo $account['email']; ?>">
                          <label for="email">Email</label>
                        </div>
                      </div>
                      <p>
                      <input name="jk" type="radio" id="test1" value="laki-laki" <?php if($account['jk'] == 'laki-laki') echo "checked";?>/>
                      <label for="test1">Laki-Laki</label>
                    </p>
                    <p>
                      <input name="jk" type="radio" id="test2" value="perempuan" <?php if($account['jk'] == 'perempuan') echo "checked";?>/>
                      <label for="test2">Perempuan</label>
                    </p>
                                   <div class="form-group" id="place_image">

                                <?php
                                    if ($account['image'] == "") {
                                ?>  <script>
                                        $('#place_image').hide();
                                        $('#btn_choose_image').html('Update Image');
                                    </script>
                                <?php   
                                    }else{
                                ?>
                                    <script>
                                        $('#place_image').show();
                                        $('#btn_choose_image').html('Update Image');
                                     </script>

                                <?php
                                    } 
                                ?>

                                  <img src="<?php echo base_url(isset($account['image']) ? $account['image'] : '')?>" id="image_category" style="width: 200px; height:200px;" name="">
                              </div>
                              <div class="form-group">
                                  <a class="btn btn-primary" id="btn_choose_image" onclick="$('#choose_image').click();">Choose Image</a>
                                  <input style="display: none;" type="file" id="choose_image" name="image"></input>
                              </div>
                              
                              <p>
                                <div class="form-group">
                                    <input type="submit" name="submit" value="Submit" class="btn waves-effect waves-light blue"></input>
                                </div>
                              </p>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div class="divider"></div>

            <!--Prefilling Text Inputs-->
            <div id="prefilling-text" class="section">
     
        <!-- Floating Action Button -->
            <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                <a class="btn-floating btn-large">
                  <i class="mdi-action-stars"></i>
                </a>
                <ul>
                  <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                  <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                  <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                  <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                </ul>
            </div>
            <!-- Floating Action Button -->
    </div>
  <!--end container-->




  </section>
  <!-- END CONTENT -->
  <script>
      $(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
        });
  </script>