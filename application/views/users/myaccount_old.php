<!-- Body Start-->
<section class="container members-area">
    <link href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/j-breadcrumb/css/breadcrumb.min.css" media="screen" rel="stylesheet" type="text/css">
    <div class="breadCrumbHolder j-mod">
        <div id="breadCrumb3" class="breadCrumb j-mod clearfix">

        </div>
    </div>
    <div class="row">
        <!-- Sidebar Start -->
        <div class="col-md-3 col-sm-4 col-xs-12">

            <div class="overview">
                <div class="list-group">
                    <span class="list-group-item active">ACCOUNT OVERVIEW</span>
                    <a href="/Members/hotels/list" class="list-group-item">Number of Listing(s)            
        <span class="label label-success pull-right">0</span>
        </a>
                </div>
                <div style="margin-top:20px;">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-angle-down"></i>MY ACCOUNT</div>
                        <div class="panel-body">
                            <ul class="sky-mega-menu sky-mega-menu-pos-left sky-mega-menu-anim-scale sky-mega-menu-response-to-switcher">
                                <li>
                                    <a href="<?php echo base_url(); ?>user/" target="">
                            Home 
                            </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>user/myaccount" target="">
                            My Account 
                            </a>
                                </li>
                                <li aria-haspopup="true">
                                    <a href="My-Hotels-420-en_US" target="">
                                My Hotels 
                            </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="Add-New-Hotels-423-en_US">Add New Hotels</a>
                                            </li>
                                            <li>
                                                <a href="Manage-My-Hotels-424-en_US">Manage My Hotels</a>
                                            </li>
                                            <li>
                                                <a href="Room-Management-427-en_US">Room Management<i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="Add-New-Room-428-en_US"> Add New Room </a>
                                                        </li>
                                                        <li>
                                                            <a href="Manage-My-Rooms-429-en_US"> Manage My Rooms </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="Room-Type-Management-430-en_US"> Room Type Management <i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="Add-New-Room-Type-431-en_US"> Add New Room Type </a>
                                                        </li>
                                                        <li>
                                                            <a href="Manage-My-Room-Types-432-en_US"> Manage My Room Types </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li aria-haspopup="true">
                                    <a href="My-Vacationrentals-421-en_US" target="">
                                My Vacationrentals 
                            </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="Add-New-Rentals-433-en_US"> Add New Rentals </a>
                                            </li>
                                            <li>
                                                <a href="Manage-My-Rentals-434-en_US"> Manage My Rentals </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li aria-haspopup="true">
                                    <a href="My-Tours-422-en_US" target="">
                                My Tours 
                            </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="Add-New-Packages-436-en_US">Add New Packages</a>
                                            </li>
                                            <li>
                                                <a href="Manage-My-Tours-437-en_US">Manage My Tours </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li aria-haspopup="true">
                                    <a href="Billing-439-en_US" target="">
                                        <!--<i class="fa fa-link"></i>-->Billing
                                    </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="My-Invoices-440-en_US"> My Invoices </a>
                                            </li>
                                            <li>
                                                <a href="My-Itineraries-446-en_US"> My Itineraries <i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="My-Hotel-Itineraries-447-en_US"> My Hotel Itineraries </a>
                                                        </li>
                                                        <li>
                                                            <a href="My-Rentals-Itineraries-448-en_US"> My Rentals Itineraries </a>
                                                        </li>
                                                        <li>
                                                            <a href="My-Tours-Itineraries-449-en_US"> My Tours Itineraries </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="Payment-Gateways-441-en_US"> Payment Gateways <i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="Available-Gateways-442-en_US"> Available Gateways </a>
                                                        </li>
                                                        <li>
                                                            <a href="My-Gateways-Only-443-en_US"> My Gateways Only </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="Upgrade-Membership-444-en_US"> Upgrade Membership </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center hidden-xs">
            </div>
        </div>
        <!-- Sidebar End -->
        <!-- Content Start -->
        <div class="col-md-9 col-sm-12 col-xs-12">
            <script type="text/javascript">
            </script>
            <div class="welcome-panel">
                <h1>My Account</h1>
                <?php if(isset($success_message)): ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $success_message ?>
                    </div>
                    <?php endif; ?>

                        <?php if(isset($message)):?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <?php echo $message ?>
                            </div>
                            <?php endif; ?>
                            <fieldset>
                                    <legend>Member Information</legend>
                                    <span class="top-go clearfix">
                                    <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a>
                                </span>
                                    <br>
                                                                 <form method="POST" name="user_register_form" id="user_register_form" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-sm-4 col-xs-12 control-label" for="fullname">Fullname :<span class="required">*</span></label>
                                            <div class="col-sm-6 col-xs-10">
                                                <input type="text" name="fullname" id="fullname" placeholder="Enter Fullname" class="form-control" title="Enter Fullname" required="required" value="<?php echo $account['name']; ?>">
                                                <div class="input-errors " name="membersInfoBlock[first_name]_err"></div>
                                            </div>
                                            <div class="ui-widget ui-helper-clearfix info">
                                                <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Full Name">
                                                    <p><span class="ui-icon ui-icon-info"></span></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 col-xs-12 control-label" for="fullname">Address :<span class="required">*</span></label>
                                            <div class="col-sm-6 col-xs-10">
                                                <input type="text" name="address" id="fullname" placeholder="Enter Address" class="form-control" title="Enter Address" required="required" value="<?php echo $account['address']; ?>">
                                                <div class="input-errors " name="membersInfoBlock[first_name]_err"></div>
                                            </div>
                                            <div class="ui-widget ui-helper-clearfix info">
                                                <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter First Name">
                                                    <p><span class="ui-icon ui-icon-info"></span></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 col-xs-12 control-label" for="email">Email :<span class="required">*</span></label>
                                            <div class="col-sm-6 col-xs-10">
                                                <input type="text" name="email" id="email" placeholder="Enter Email" class="form-control" title="Enter Email" required="required" value="<?php echo $account['email']; ?>">
                                                <div class="input-errors " name="membersInfoBlock[last_name]_err">
                                                </div>
                                            </div>

                                            <div class="ui-widget ui-helper-clearfix info">
                                                <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Email">
                                                    <p><span class="ui-icon ui-icon-info"></span></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 col-xs-12 control-label" for="email">Password :<span class="required">*</span></label>
                                            <div class="col-sm-6 col-xs-10">
                                                <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control" title="Enter Password" required="required" value="<?php echo isset($_POST['password']) ? $_POST['password'] : ''; ?>">
                                                <div class="input-errors " name="membersInfoBlock[last_name]_err">
                                                </div>

                                            </div>

                                            <div class="ui-widget ui-helper-clearfix info">
                                                <button id="show" onclick="ShowPassword()"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                                <button style="display:none" id="hide" value="Hide Password" onclick="HidePassword()">
                                                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </div>
            </div>

            </fieldset>
            <fieldset>
                <legend>Other Information</legend>
                <span class="top-go clearfix">
                <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a>
            </span>
                <br>

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="fullname">Jenis Kelamin :<span class="required">*</span></label>
                    <div class="col-md-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="jk" value="laki-laki" id="jk" <?php if($account['jk'] == 'laki-laki') echo "checked";?>/> Laki-laki</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="jk" value="perempuan" id="jk" <?php if($account['jk'] == 'perempuan') echo "checked";?>/>Perempuan</label>
                        </div>
                    </div>

                    <div class="ui-widget ui-helper-clearfix info">
                        <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Jenis Kelamin">
                            <p><span class="ui-icon ui-icon-info"></span></p>
                        </div>
                    </div>
                </div>

            </fieldset>
            <fieldset>
                <legend>Contact Information</legend>
                <span class="top-go clearfix">
                <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a></span>
                <br>

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="phone">Phone :<span class="required">*</span></label>
                    <div class="col-sm-6 col-xs-10">
                        <input type="text" name="phone" id="phone" placeholder="Enter Phone" class="form-control" title="Enter Phone" required="required" value="<?php echo $account['phone']; ?>">
                        <div class="input-errors " name="membersInfoBlock[first_name]_err"></div>
                    </div>
                    <div class="ui-widget ui-helper-clearfix info">
                        <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Phone">
                            <p><span class="ui-icon ui-icon-info"></span></p>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>

                <legend>Upload Photo</legend>
                <span class="top-go clearfix">
                <a href="#topGo" title="Click To Go Top" class="btn btn-blue btn-xs pull-right"><i class="fa fa-chevron-up"></i></a>
            </span>
                <br>

                <div class="form-group">
                    <label class="col-sm-4 col-xs-12 control-label" for="phone">Upload Photo :</label>
                      <div class="col-sm-6 col-xs-10">
                                <div class="form-group" id="place_image">

                                <?php
                                    if ($account['image'] == "") {
                                ?>  <script>
                                        $('#place_image').hide();
                                        $('#btn_choose_image').html('Update Image');
                                    </script>
                                <?php   
                                    }else{
                                ?>
                                    <script>
                                        $('#place_image').show();
                                        $('#btn_choose_image').html('Update Image');
                                     </script>

                                <?php
                                    } 
                                ?>

                                  <img src="<?php echo base_url(isset($account['image']) ? $account['image'] : '')?>" id="image_category" style="width: 120px; height:120px;" name="">
                              </div>
                              <div class="form-group">
                                  <a class="btn btn-primary" id="btn_choose_image" onclick="$('#choose_image').click();">Choose Image</a>
                                  <input style="display: none;" type="file" id="choose_image" name="image"></input>
                              </div>
                        </div>
                    <div class="ui-widget ui-helper-clearfix info">
                        <div class="ui-widget-header ui-corner-all ui-state-default" title="Enter Photo">
                            <p><span class="ui-icon ui-icon-info"></span></p>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="text-center btn-groups bottom">
                    <a class="reset_btn btn btn-success" onclick="resetForm()"><i class="fa fa-retweet"></i> Reset</a>
                    <button type="submit" name="submit" class="save_data_btn btn btn-primary" id="submit_user">Register</button>
                </div>
        </div>
    </div>
    </form>
                                </div>
            </div>
            <br>
          
        </div>
        <!-- Content End -->

    </div>
</section>
<script type="text/javascript">
    function ShowPassword() {
        if (document.getElementById("password").value != "") {
            document.getElementById("password").type = "text";
            document.getElementById("show").style.display = "none";
            document.getElementById("hide").style.display = "block";
        }
    }

    function HidePassword() {
        if (document.getElementById("password").type == "text") {
            document.getElementById("password").type = "password"
            document.getElementById("show").style.display = "block";
            document.getElementById("hide").style.display = "none";
        }
    }
            $(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
        });
</script>