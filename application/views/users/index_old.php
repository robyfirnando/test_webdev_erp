<!-- Body Start-->
<section class="container members-area">
    <link href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/j-breadcrumb/css/breadcrumb.min.css" media="screen" rel="stylesheet" type="text/css">
    <div class="breadCrumbHolder j-mod">
        <div id="breadCrumb3" class="breadCrumb j-mod clearfix">

        </div>
    </div>
    <div class="row">
        <!-- Sidebar Start -->
        <div class="col-md-3 col-sm-4 col-xs-12">

            <div class="overview">
                <div class="list-group">
                    <span class="list-group-item active">ACCOUNT OVERVIEW</span>
                    <a href="/Members/hotels/list" class="list-group-item">Number of Listing(s)            
        <span class="label label-success pull-right">0</span>
        </a>
                </div>
                <div style="margin-top:20px;">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-angle-down"></i>MY ACCOUNT</div>
                        <div class="panel-body">
                            <ul class="sky-mega-menu sky-mega-menu-pos-left sky-mega-menu-anim-scale sky-mega-menu-response-to-switcher">
                                <li>
                                    <a href="<?php echo base_url(); ?>user/" target="">
                            Home 
                            </a>
                                </li>
                               
                                <li>
                                    <a href="<?php echo base_url(); ?>user/myaccount" target="">
                                    My Account 
                                    </a>
                                </li>
                                <li aria-haspopup="true">
                                    <a href="My-Hotels-420-en_US" target="">
                                My Hotels 
                            </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="Add-New-Hotels-423-en_US">Add New Hotels</a>
                                            </li>
                                            <li>
                                                <a href="Manage-My-Hotels-424-en_US">Manage My Hotels</a>
                                            </li>
                                            <li>
                                                <a href="Room-Management-427-en_US">Room Management<i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="Add-New-Room-428-en_US"> Add New Room </a>
                                                        </li>
                                                        <li>
                                                            <a href="Manage-My-Rooms-429-en_US"> Manage My Rooms </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="Room-Type-Management-430-en_US"> Room Type Management <i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="Add-New-Room-Type-431-en_US"> Add New Room Type </a>
                                                        </li>
                                                        <li>
                                                            <a href="Manage-My-Room-Types-432-en_US"> Manage My Room Types </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li aria-haspopup="true">
                                    <a href="My-Vacationrentals-421-en_US" target="">
                                My Vacationrentals 
                            </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="Add-New-Rentals-433-en_US"> Add New Rentals </a>
                                            </li>
                                            <li>
                                                <a href="Manage-My-Rentals-434-en_US"> Manage My Rentals </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li aria-haspopup="true">
                                    <a href="My-Tours-422-en_US" target="">
                                My Tours 
                            </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="Add-New-Packages-436-en_US">Add New Packages</a>
                                            </li>
                                            <li>
                                                <a href="Manage-My-Tours-437-en_US">Manage My Tours </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li aria-haspopup="true">
                                    <a href="Billing-439-en_US" target="">
                                        <!--<i class="fa fa-link"></i>-->Billing
                                    </a>
                                    <div class="grid-container3">
                                        <ul>
                                            <li>
                                                <a href="My-Invoices-440-en_US"> My Invoices </a>
                                            </li>
                                            <li>
                                                <a href="My-Itineraries-446-en_US"> My Itineraries <i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="My-Hotel-Itineraries-447-en_US"> My Hotel Itineraries </a>
                                                        </li>
                                                        <li>
                                                            <a href="My-Rentals-Itineraries-448-en_US"> My Rentals Itineraries </a>
                                                        </li>
                                                        <li>
                                                            <a href="My-Tours-Itineraries-449-en_US"> My Tours Itineraries </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="Payment-Gateways-441-en_US"> Payment Gateways <i class="indicator fa fa-angle-right"></i></a>
                                                <div class="grid-container3">
                                                    <ul class="dropdown">
                                                        <li>
                                                            <a href="Available-Gateways-442-en_US"> Available Gateways </a>
                                                        </li>
                                                        <li>
                                                            <a href="My-Gateways-Only-443-en_US"> My Gateways Only </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="Upgrade-Membership-444-en_US"> Upgrade Membership </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center hidden-xs">
            </div>
        </div>
        <!-- Sidebar End -->
        <!-- Content Start -->
        <div class="col-md-9 col-sm-12 col-xs-12">
            <script type="text/javascript">
            </script>
            <div class="welcome-panel">
                <h1>My Dashboard</h1>
                <?php if(isset($success_message)): ?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $success_message ?>
                    </div>
                    <?php endif; ?>

                        <?php if(isset($message)):?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <?php echo $message ?>
                            </div>
                            <?php endif; ?>

                                <div class="row">
                                    <div class="col-md-9 col-sm-9">
                                        <div class="row user-info">
                                            <div class="col-md-3 col-sm-4 col-xs-3">
                                                <img src="<?php echo $this->session->userdata('front_pic'); ?>" border="0" class="img-circle img-responsive" />
                                            </div>

                                            <div class="col-md-9 col-sm-8 col-xs-9">
                                                <p>
                                                    User Name :
                                                    <?php echo $account['name']; ?>
                                                        <br> Email :
                                                        <?php echo $account['email']; ?>
                                                            <br> Last Login : Friday, February &#49;&#48;, &#50;&#48;&#49;&#55; &#48;&#52;:&#48;&#53; AM
                                                            <br>
                                                </p>
                                                <a href="<?php echo base_url(); ?>user/logout" class="btn btn-success no-radius"><i class="fa fa-unlock-alt"></i> &nbsp;Log Out</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 hidden-xs">
                                        <figure>
                                            <i class="fa fa-calendar date"></i> Feb
                                            <br>
                                            <span class="count"></span>
                                        </figure>
                                    </div>
                                </div>
            </div>
            <div id="tabstrip-left" class="k-tabstrip-left">

                <div class="dashboard-icon">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/list">
                                    <div class="circle circle-blue"><i class="fa fa-user"></i><span><i class="fa fa-plus action"></i></span></div><span>Manage User</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/list">
                                    <div class="circle circle-red"><i class="fa fa-user"></i><span><i class="fa fa-search action"></i></span></div><span>Search User</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/change-password">
                                    <div class="circle circle-purple"><i class="fa fa-key"></i></div><span>Change Password</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/profile">
                                    <div class="circle circle-orange"><i class="fa fa-key"></i><span><i class="fa fa-info action"></i></span></div><span>View Profile</span></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="dashboard-icon">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/add">
                                    <div class="circle circle-purple"><i class="fa fa-hospital-o"></i><span><i class="fa fa-plus action"></i></span></div><span>Add Hotels</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/list">
                                    <div class="circle circle-orange"><i class="fa fa-hospital-o"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Hotels</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/price/add">
                                    <div class="circle circle-yellow"><i class="fa fa-briefcase"></i><span><i class="fa fa-plus action"></i></span></div><span>Add Price Plan</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/price/list">
                                    <div class="circle circle-pink"><i class="fa fa-briefcase"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Price Plan</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/rooms/add">
                                    <div class="circle circle-deep-purple"><i class="fa fa-key"></i><span><i class="fa fa-plus action"></i></span></div><span>Add Rooms</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/rooms/list">
                                    <div class="circle circle-blue"><i class="fa fa-key"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Rooms</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/roomtype/add">
                                    <div class="circle circle-green"><i class="fa fa-inbox"></i><span><i class="fa fa-plus action"></i></span></div><span>Add Room Type</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/hotels/roomtype/list">
                                    <div class="circle circle-red"><i class="fa fa-inbox"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Room Type</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-icon">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/vacationrentals/favorite/list">
                                    <div class="circle circle-blue"><i class="fa fa-bookmark"></i><span><i class="fa fa-check-square-o action"></i></span></div><span>Manage Favorite</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/vacationrentals/add">
                                    <div class="circle circle-purple"><i class="fa fa-umbrella"></i><span><i class="fa fa-plus action"></i></span></div><span>Add Product</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/vacationrentals/list">
                                    <div class="circle circle-orange"><i class="fa fa-umbrella"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Product</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-icon">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/tours/add">
                                    <div class="circle circle-purple"><i class="fa fa-tree"></i><span><i class="fa fa-plus action"></i></span></div><span>Add New Listing</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Members/tours/list">
                                    <div class="circle circle-orange"><i class="fa fa-tree"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Listing</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Administrator/tours/type/list/create/new">
                                    <div class="circle circle-yellow"><i class="fa fa-suitcase"></i><span><i class="fa fa-plus action"></i></span></div><span>Add Business Types</span></a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4">
                            <div class="tools-icon">
                                <a href="/Administrator/tours/type/list">
                                    <div class="circle circle-pink"><i class="fa fa-suitcase"></i><span><i class="fa fa-cog action"></i></span></div><span>Manage Business Types</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/div-->
            </div>
            <br>
            <div id="news-tab">
                <ul class="resp-tabs-list">
                    <li style="border-width:1px 1px 0 1px; border-color:#dedede;">Latest News</li>
                </ul>
                <div class="resp-tabs-container">
                    <div>
                        <div class="media">
                            <a href="/News-Article/News/Maintenance-and-Optimization/Create-Manage-and-Setup-Access-privilege" title="Create-Manage-and-Setup-Access-privilege" class="pull-left"><img src="data/frontFiles/news/page_thumb/roles2.jpg" width="50" border="0" class="img-thumbnail" /></a>
                            <div class="media-body">
                                <a href="/News-Article/News/Maintenance-and-Optimization/Create-Manage-and-Setup-Access-privilege" title="Create-Manage-and-Setup-Access-privilege">Create Manage and Setup Access privilege</a>
                                <br> All of Eicra&amp;rsquo;s web application has built-in permission system which was based...
                                <br />
                                <span class="date">01-01-1970 [12:00 AM]</span>
                            </div>
                        </div>
                        <hr>
                        <div class="media">
                            <a href="/News-Article/News/Templates-Customization/What-is-Website-template" title="What-is-Website-template" class="pull-left"><img src="data/frontFiles/news/page_thumb/no-imager.jpg" width="50" border="0" class="img-thumbnail" /></a>
                            <div class="media-body">
                                <a href="/News-Article/News/Templates-Customization/What-is-Website-template" title="What-is-Website-template">What is Website template</a>
                                <br> A Web template system describes the software and methodologies used to produce...
                                <br />
                                <span class="date">01-01-1970 [12:00 AM]</span>
                            </div>
                        </div>
                        <hr>
                        <div class="media">
                            <a href="/News-Article/News/Knowledgebase/How-to-Create-new-Menu-Group." title="How-to-Create-new-Menu-Group." class="pull-left"><img src="data/frontFiles/news/page_thumb/menu-grp.png" width="50" border="0" class="img-thumbnail" /></a>
                            <div class="media-body">
                                <a href="/News-Article/News/Knowledgebase/How-to-Create-new-Menu-Group." title="How-to-Create-new-Menu-Group.">How to Create new Menu Group.</a>
                                <br> What is&amp;nbsp;Menu Group ? Menu navigation group is the science and skill...
                                <br />
                                <span class="date">01-01-1970 [12:00 AM]</span>
                            </div>
                        </div>
                        <hr>
                        <div class="text-right">
                            <a href="http://demo.phpholidays.com/News" class="btn btn-primary">More News <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content End -->

    </div>
</section>