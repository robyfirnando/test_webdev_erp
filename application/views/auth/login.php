<section class="section-account">
	<div class="img-backdrop" style="background-image: url('<?php echo base_url();?>assets/backend/img/img16.jpg')"></div>
	<div class="spacer"></div>
	<div class="card contain-sm style-transparent">
		<div class="card-body">
			<div class="row">
				<div class="col-sm-12">
					<br/>
					<span class="text-lg text-bold text-primary"><?php echo label('login');?></span>
					<br/><br/>
					<form class="form floating-label" accept-charset="utf-8" method="post" >
						<?php if(isset($message)): ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<?php echo $message ?>
							</div>
						<?php endif; ?>
						<?php if(isset($success_message)): ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								<?php echo $success_message ?>
							</div>
						<?php endif; ?>
						<div class="row" style="display:none;">
							<div class="col-xs-6 text-left"><a href="<?php echo $login_url; ?>" class="btn btn-default" style="background-color:#DD4B39; color:white; width:100%;">
							    <i class="fa fa-google-plus"></i> Google +
							  </a></div>
							<div class="col-xs-6 text-right">
								<a href="<?php echo getFacebookLoginUrl(); ?>" class="btn btn-default" style="background-color:#6c8dbe; color:white; width:100%;">
							    <i class="fa fa-facebook"></i> Facebook
							  </a>
							</div>
						</div>
						</br>
						<div class="form-group">
							<input type="email" class="form-control" id="email" name="email" required value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
							<label for="username">Email</label>
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="password" name="password" required >
							<label for="password"><?php echo label('password');?></label>
							
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-6 text-left">
								<div class="checkbox checkbox-inline checkbox-styled" style="display: none">
									<label>
										<input type="checkbox" name="remember"> <span><?php echo label('remember')?></span>
									</label>
								</div>
								<p>
                                    <br/>
                                   
                                </p>
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-primary btn-raised" type="submit">
								<?php echo label('login');?></button>
							</div>
						</div>
							<input type=button id="show" value="Show Password" onclick="ShowPassword()">
								<input type=button style="display:none" id="hide" value="Hide Password" onclick="HidePassword()">
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<div style="float:right;margin-right:15px;font-size:15px;">
        <a href="<?php echo base_url();?>auth/languages/english">English</a> |
           <a href="<?php echo base_url();?>auth/languages/indonesia">Indonesia</a> |
           <a href="<?php echo base_url();?>auth/languages/jawa">Jawa</a> 
 </div>
 
 <script type="text/javascript">
 	function ShowPassword()
{
	if(document.getElementById("password").value!="")
	{
		document.getElementById("password").type="text";
		document.getElementById("show").style.display="none";
		document.getElementById("hide").style.display="block";
	}
}

function HidePassword()
{
	if(document.getElementById("password").type == "text")
	{
		document.getElementById("password").type="password"
		document.getElementById("show").style.display="block";
		document.getElementById("hide").style.display="none";
	}
}
 </script>