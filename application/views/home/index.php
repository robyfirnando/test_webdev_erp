<!-- Body Start-->
<div class="container">
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <!-- Content Start -->

                <div id="dialog_container_block" title="dialog Message"></div>

                <div class="block block-search phpholidays">
                    <div class="block-body">
                        <h1 class="text-center">Find attractions, Find Your Homestay In Batu City & Find Tour Place </h1>
                        <div id="featured-tab">
                            <ul class="resp-tabs-list">
                                <li><span>Batu City ?</span></li>
                            </ul>

                            <div class="resp-tabs-container">

                                <!-- Tab 6 start -->
                                <div style="display:none">
                                    <h3>Find Your Homestay & Find Tour Place</h3>
                                    <p>Kami menyediakan Penginapan, Homestay dan Villa Murah di Kota Batu yang letaknya strategis berdekatan dengan tempat wisata seperti BNS, Museum Satwa, Jatim Park museum angkut dan lain-lain.</p>
                                </div>
                                <!-- Tab 6 end -->

                                <!-- Tab 7 start -->
                                <div style="display:none">
                                    <h3>Find Us</h3>

                                    <div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-7">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" width="20">&nbsp;</td>
                                                            <td>Adderss:</td>
                                                            <td>Company Office Address</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">&nbsp;</td>
                                                            <td>Telephone No:</td>
                                                            <td>000 00 00</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">&nbsp;</td>
                                                            <td>Fax No:</td>
                                                            <td>000 00 00</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">&nbsp;</td>
                                                            <td>Hotline:</td>
                                                            <td>000 0000 00</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">&nbsp;</td>
                                                            <td>Support:</td>
                                                            <td><a href="mailto:you@yourdomain.com">you@youremail.com</a>
                                                                <br /> <a href="mailto:you@yourdomain.com">you@youremail.com</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">&nbsp;</td>
                                                            <td>Website URL:</td>
                                                            <td><a href="#" target="blank">http://www.yourdomain.com</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="alert alert-info">Our Office Hours Monday to Friday from 10:00 A.M to 6:00 P.M</div>
                                            </div>
                                            <div class="col-xs-12 col-md-5">
                                                <iframe src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Flick+Media+Ltd+Suite+110,+Queens+Way+House+275-285+High+Street+Stratford,+London+%E2%80%93+E15+2TF&amp;sll=37.0625,-95.677068&amp;sspn=34.038806,56.513672&amp;ie=UTF8&amp;hq=Flick+Media+Ltd+Suite+110,+Queens+Way+House+275-285+High+Street&amp;hnear=London+E15+2TF,+UK&amp;view=map&amp;ei=i-w-S6vQJ5CojAeE7ZSWCQ&amp;attrid=&amp;ll=51.544253,0.000601&amp;spn=0.007101,0.013797&amp;z=14&amp;iwloc=A&amp;cid=2785574160978382141&amp;output=embed" width="100%" height="350" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab 7 start -->
                            </div>
                        </div>
                    </div>
                </div>

                <div id="home-search-tab">
                    <ul class="resp-tabs-list">
                        <li>Popular Tours</li>
                        <li>Popular Penginapan</li>
                    </ul>

                    <div class="resp-tabs-container">
                        <div>
                            <div class="block block-popular">
                                <h3>Popular Tours</h3>
                                <?php
        foreach($tops as $top):
        // $date=date_create($top->datetime);

        ?>
                                    <figure>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="thumb">
                                                    <?php foreach(get_image_top_only($top->id, 'images') as $key => $img): 
                                                    
                  // var_dump($img->image);
                  // exit();                           
                  ?>
                                                        <?php if($img->image){?>
                                                            <a href="Hotels-details/Ameritania-Hotel.html" class="preview" link="<?php echo base_url($img->image);?>" title="<?php echo $top->title; ?>" width="300">
                                                                <?php }else{?>
                                                                    <a href="Hotels-details/Ameritania-Hotel.html" class="preview" link="http://vignette3.wikia.nocookie.net/max-steel-reboot/images/7/72/No_Image_Available.gif/revision/latest?cb=20130902173013" title="Ameritania Hotel" width="300">
                                                                        <?php } ?>

                                                                            <?php if($img->image){?>
                                                                                <span class="type"><?php 
                                                                                            $nm = $img->tags;
                                                                                            if (strlen($nm) > 10) $nm = substr($nm, 0, 15) . '...';
                                                                                            echo $nm;
                                                                                    ?></span>
                                                                                <img src="<?php echo base_url($img->image); ?>" </a>

                                                                                <?php }else{ ?>
                                                                                    <span class="type">
                                                                                    <?php 
                                                                                            $nm = $img->tags;
                                                                                            if (strlen($nm) > 10) $nm = substr($nm, 0, 15) . '...';
                                                                                            echo $nm;
                                                                                    ?></span>
                                                                                    <img src="http://vignette3.wikia.nocookie.net/max-steel-reboot/images/7/72/No_Image_Available.gif/revision/latest?cb=20130902173013"></a>
                                                                    <?php } ?>
                                                                        <?php endforeach; ?>

                                                                            <!-- <div class="photo-count-container">
                        <div class="photo-count">
                            <a href="Hotels-details/Ameritania-Hotel/1.html" title="Click to view listing"><i class="fa fa-camera"></i> <span>&#50;</span> Photos</a>
                        </div>
                    </div> -->
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <h6><a href="<?php echo base_url().'blog/detail/'.$top->slug; ?>" class="preview" link="" title="<?php echo $top->title; ?>"><?php echo $top->title;?></a></h6>

                                                <div class="ratings" style="font-size:10px;">

                                                </div>
                                               
                                                <br>
                                                <?php 
                                            $str = $top->description;
                                            if (strlen($str) > 20) $str = substr($str, 0, 130) . '...';
                                            echo $str;
                                        ?>
                                                    <br>
                                                    <br>
                                                    <!-- <span class="label label-primary">Fantastic</span> <span class="label label-success">Hotel</span> -->
                                            </div>

                                            <div class="col-md-3">
                                                <div class="info">

                                                    <!--  <span class="price"><?php echo $top->price; ?></span> -->
                                                    <!-- <s>$&#54;&#52;&#53;,&#48;&#48;</s> -->

                                                    <br>
                                                    <br>

                                                    <!-- <span class="label label-warning"><a href="Hotels-details/Ameritania-Hotel/11.html"><i class="fa fa-comment"></i> <span>&#48;</span> reviews</a>
                                                    </span> -->

                                                </div>
                                            </div>
                                        </div>
                                    </figure>
                                    <?php endforeach; ?>

                                        <div class="text-center" style="display:none;">
                                            <a href="All-Hotel-List/group_id/1.html" class="btn btn-global">More Tours <i class="fa fa-chevron-right"></i></a>
                                        </div>
                            </div>
                        </div>
                        <div>
                            <div class="block block-popular">
                                <h3>Popular Penginapan</h3>
                                <?php
                                foreach($tops_inap as $top_inap):
                                // $date=date_create($top->datetime);
                                ?>
                                <figure>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="thumb">
                                            <?php foreach(get_image_inap_top_only($top_inap->id, 'images') as $key => $inap_img): 

                                            ?>

                                                <span class="type"><?php 
                                                                                            $nm = $inap_img->tags;
                                                                                            if (strlen($nm) > 10) $nm = substr($nm, 0, 15) . '...';
                                                                                            echo $nm;
                                                                                    ?></span>
                                                <a href="<?php echo base_url().'blog/detail_inap/'.$inap_img->slug; ?>" class="preview" link="<?php echo base_url($inap_img->image);?>" title="<?php echo $inap_img->title; ?>" width="300"><img src="<?php echo base_url($inap_img->image);?>" border="0" class="img-responsive" /></a>
                                            <?php endforeach; ?>

                                                <div class="photo-count-container">
                                                    <div class="photo-count">
                                                        <a href="#" title="Click to view listing" style="display:none;"><i class="fa fa-camera"></i> <span>&#50;</span> Photos</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h6><a href="<?php echo base_url().'blog/detail_inap/'.$inap_img->slug; ?>" class="preview" link=""  title="<?php echo $inap_img->title?>"><?php echo $inap_img->title;?></a></h6>

                                            
                                            <br>
                                            <a href="Vacationrentals-details/A-Beach-Haven-world/8.html"></a>
                                            <br>   <?php 
                                            $str = $top_inap->description;
                                            if (strlen($str) > 20) $str = substr($str, 0, 130) . '...';
                                            echo $str;
                                        ?> </div>

                                        <div class="col-md-3">
                                            <div class="info">
                                                <img src="http://html.rumah360.com/assets/img/icons/detail_properti_kamartidur.svg" width="20px"> <?php echo $top_inap->jml_kamartidur." rooms";?> <img src="http://html.rumah360.com/assets/img/icons/detail_properti_kamarmandi.svg" width="20px"><?php echo $top_inap->jml_kamarmandi." bathroom";?> 
                                                <br>
                                                <div class="clearfix"></div>
                                                <span class="price"><?php echo rupiah($top_inap->harga); ?> <span class="label label-success">Nightly Price</span></span>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                        <?php endforeach; ?>
                             
                            </div>
                        </div>

                        <div>
                            <div class="block block-popular">
                                <h3>Popular Tour Packages</h3>
                                <figure>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="thumb">
                                                <span class="type">Educational</span>
                                                <a href="Tours-details/Natural-View-of-Jaflong-2-91.html" class="preview" link="data/frontFiles/tours/tours_image/Jaflong.jpg" width="300" title="Natural View of Jaflong"><img src="data/frontFiles/tours/tours_image/Jaflong.jpg" border="0" class="img-responsive"></a>
                                                <div class="photo-count-container">
                                                    <div class="photo-count">
                                                        <a href="Tours-details/Natural-View-of-Jaflong-2-91/1.html" title="Click to view listing"><i class="fa fa-camera"></i> <span>&#50;</span> Photos</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h6><a href="Tours-details/Natural-View-of-Jaflong-2-91.html" class="preview" link="" title="Natural View of Jaflong">Natural View of Jaflong</a></h6>

                                            <div class="ratings">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>

                                            <i class="fa fa-map-marker"></i> Location :
                                            <a href="Tours-details/Natural-View-of-Jaflong-2-91.html"></a>
                                            <br> Description : It is one of the most attractive tourist sites of Sylhet division. It is about... </div>

                                        <div class="col-md-3">
                                            <div class="info">
                                                <div class="duration"><span class="day"><i class="fa fa-sun-o"></i>2 Day(s)</span><span class="night"><i class="fa fa-moon-o"></i>2 Night(s)</span></div>
                                                <br>
                                                <span class="price"> $&#50;&#48;&#48;,&#48;&#48;</span>
                                                <br>
                                                <span class="label label-success"><i class="fa fa-comment"></i> <a href="Tours-details/Natural-View-of-Jaflong-2-91/9.html"><span>&#48;</span> reviews</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                                <figure>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="thumb">
                                                <span class="type">River cruise</span>
                                                <a href="Tours-details/Natural-beauty-of-Cox-s-Bazar-3-3-5-91.html" class="preview" link="data/frontFiles/tours/tours_image/Cox-s Bazar beach tour.jpg" width="300" title="Natural beauty of Cox-s Bazar"><img src="data/frontFiles/tours/tours_image/Cox-s%20Bazar%20beach%20tour.jpg" border="0" class="img-responsive"></a>
                                                <div class="photo-count-container">
                                                    <div class="photo-count">
                                                        <a href="Tours-details/Natural-beauty-of-Cox-s-Bazar-3-3-5-91/1.html" title="Click to view listing"><i class="fa fa-camera"></i> <span>&#50;</span> Photos</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h6><a href="Tours-details/Natural-beauty-of-Cox-s-Bazar-3-3-5-91.html" class="preview" link="" title="Natural beauty of Cox-s Bazar">Natural beauty of Cox-s Bazar</a></h6>

                                            <div class="ratings">

                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>

                                            </div>
                                            <i class="fa fa-map-marker"></i> Location :
                                            <a href="Tours-details/Natural-beauty-of-Cox-s-Bazar-3-3-5-91.html"></a>
                                            <br> Description : Cox&amp;rsquo;s Bazar is known for its wide sandy beach which is claimed to be the...
                                        </div>

                                        <div class="col-md-3">
                                            <div class="info">
                                                <div class="duration"><span class="day"><i class="fa fa-sun-o"></i>7 Day(s)</span><span class="night"><i class="fa fa-moon-o"></i>6 Night(s)</span></div>
                                                <br>
                                                <span class="price"> $&#51;&#48;&#48;,&#48;&#48;</span>
                                                <br>
                                                <span class="label label-success"><i class="fa fa-comment"></i> <a href="Tours-details/Natural-beauty-of-Cox-s-Bazar-3-3-5-91/9.html"><span>&#48;</span> reviews</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                                <figure>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="thumb">
                                                <span class="type">Public / Anybody</span>
                                                <a href="Tours-details/Ratargul-Swamp-Forest-91.html" class="preview" link="data/frontFiles/tours/tours_image/sylhet ratergul.jpg" width="300" title="Ratargul Swamp Forest"><img src="data/frontFiles/tours/tours_image/sylhet%20ratergul.jpg" border="0" class="img-responsive"></a>
                                                <div class="photo-count-container">
                                                    <div class="photo-count">
                                                        <a href="Tours-details/Ratargul-Swamp-Forest-91/1.html" title="Click to view listing"><i class="fa fa-camera"></i> <span>&#50;</span> Photos</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h6><a href="Tours-details/Ratargul-Swamp-Forest-91.html" class="preview" link="" title="Ratargul Swamp Forest">Ratargul Swamp Forest</a></h6>

                                            <div class="ratings">

                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>

                                            </div>
                                            <i class="fa fa-map-marker"></i> Location :
                                            <a href="Tours-details/Ratargul-Swamp-Forest-91.html"></a>
                                            <br> Description : Ratargul Swamp Forest is a freshwater swamp forest located in Gowainghat, Sylhet, Bangladesh. It is... </div>

                                        <div class="col-md-3">
                                            <div class="info">
                                                <div class="duration"><span class="day"><i class="fa fa-sun-o"></i>4 Day(s)</span><span class="night"><i class="fa fa-moon-o"></i>3 Night(s)</span></div>
                                                <br>
                                                <span class="price"> $&#50;&#53;&#48;,&#48;&#48;</span>
                                                <br>
                                                <span class="label label-success"><i class="fa fa-comment"></i> <a href="Tours-details/Ratargul-Swamp-Forest-91/9.html"><span>&#48;</span> reviews</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>

                        <div>
                            <div class="block block-latest block-flight-pkg">
                                <h3>Latest Flight Packages</h3>

                                <figure>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="thumb">
                                                <a class="preview" link="data/frontFiles/flight/airlines/saudi-arabian-airlines-logo.png" width="174"><img src="data/frontFiles/flight/airlines/saudi-arabian-airlines-logo.png" border="0" class="img-responsive" /></a>
                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="text-center">
                                                <i class="fa fa-plane"></i> Tour Flight
                                                <hr>
                                                <h4 class="form-group">
                        Adult - $&#51;&#50;&#52;,&#48;&#48;, Child - $&#52;&#51;,&#48;&#48;, Infant - $&#50;&#51;,&#48;&#48;                    </h4>
                                                <a href="Flight-Details/Tour-Flight.html" class="btn btn-global">See Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                                <figure>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="thumb">
                                                <a class="preview" link="data/frontFiles/flight/airlines/saudi-arabian-airlines-logo.png" width="174"><img src="data/frontFiles/flight/airlines/saudi-arabian-airlines-logo.png" border="0" class="img-responsive" /></a>
                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="text-center">
                                                <i class="fa fa-plane"></i> Usa Toure
                                                <hr>
                                                <h4 class="form-group">
                        Adult - $&#51;&#52;&#51;,&#48;&#48;, Child - $&#53;&#52;,&#48;&#48;, Infant - $&#49;&#49;,&#48;&#48;                    </h4>
                                                <a href="Flight-Details/Usa-Toure.html" class="btn btn-global">See Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                                <figure>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="thumb">
                                                <a class="preview" link="data/frontFiles/flight/airlines/saudi-arabian-airlines-logo.png" width="174"><img src="data/frontFiles/flight/airlines/saudi-arabian-airlines-logo.png" border="0" class="img-responsive" /></a>
                                            </div>
                                        </div>

                                        <div class="col-md-10">
                                            <div class="text-center">
                                                <i class="fa fa-plane"></i> tures1
                                                <hr>
                                                <h4 class="form-group">
                        Adult - $&#50;&#51;,&#48;&#48;, Child - $&#53;&#52;,&#48;&#48;, Infant - $&#51;,&#48;&#48;                    </h4>
                                                <a href="Flight-Details/tures1.html" class="btn btn-global">See Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="accordion-list">
                    <ul class="resp-tabs-list">
                        <li>Recently Added Hotels</li>
                        <li>Great Vacations and Popular Getaways</li>
                        <li>Popular Destinations</li>
                    </ul>
                    
                </div>
                <!-- Content End -->
            </div>
            <div class="col-md-3">
                <!-- Sidebar Start -->
                <div class="clearfix"></div>
                <div class="block block-subscribe">
                    <div class="block-body">
                        <h3>Email Subscription</h3>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-check-circle"></i> Don't miss out on our great offers</li>
                            <li><i class="fa fa-check-circle"></i> Receive deals and updated from our agents via e-mail</li>
                        </ul>

                        <p>Join our mailing list and win exciting offer, get special promotional offer and other services. Type your name and email address below for newsletter subscriptions.</p>

                        <div id="actionMessage_newsletter"></div>

                        <input type="hidden" id="newsletter_common_err_msg" name="newsletter_common_err_msg" value="There are some error to save this article. Please See below --" />

                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" action="<?php echo base_url(); ?>home/subscription_add" name="newsletter_form_box" id="newsletter_form_box" role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" required="required" value="" title="Enter Name" id="name">
                                            <div class="input-errors " name="name_err"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" name="email" class="form-control" placeholder="Enter Email" required="required" value="" title="Enter Email" id="email">
                                            <div class="input-errors " name="email_err"></div>
                                        </div>
                                    </div>

                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" name="submit" value="Subscribe Now" class="newsletter_snd_btn btn btn-global btn-block" id="save">
                                </div>
                            </div>

                            </form>
                        </div>

                        <div id="newsletter_loader_process" style="display:none;"><i class="fa fa-circle-o-notch fa-spin"></i></div>
                        <div id="sent" style="display:none;"><i class="fa fa-check-circle" aria-hidden="true">
        </i>Thank You For Subscribe Us !</div>

                    </div>
                </div>

            

                <!-- Sidebar End -->
            </div>
        </div>
    </section>
</div>

<!-- Body End-->

<script type="text/javascript">
    $(function() {
        $("#save").click(function(event) {
            event.preventDefault(); //prevent auto submit data

            var name = $("#name").val();
            var email = $("#email").val();
            //assign our rest of variables
            $.ajax({
                type: "post",
                url: "<?php echo base_url(); ?>home/subscription_add",
                data: {
                    name: name,
                    email: email
                },
                beforeSend: function() {
                    $("#newsletter_loader_process").show();
                },
                success: function(data) {
                    console.log(data);
                    $("#newsletter_loader_process").hide();
                    $("#sent").show();
                }
            });
        });
    });
</script>