<!DOCTYPE html>
<html dir="ltr" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Travel classified Listing Script , API based Travel Booking Script">
    <meta name="description" content="Complete classifieds software solution: feature-rich PHP classifieds php software for travel and tour package business man who also want hotel">
    <link rel="shortcut icon" type="image/png" href="http://divineandaman.com/images/logo-thumb.png"/>
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/vendor/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/vendor/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/vendor/scripts/jquery-ui/blue/jquery-ui.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/top-nav.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/members/member_menu.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/members/members.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/responsive-tabs.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/tipso.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/template.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/general.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/vendor/scripts/kendo/styles/kendo.common.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/vendor/scripts/kendo/styles/kendo.uniform.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/pc/css/resp-carousel.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/pc/css/resp-theme.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/pc/css/carousel-custom.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/pc/css/typeahead.css" media="screen" rel="stylesheet" type="text/css">
    <script type="" src="<?php echo base_url();?>assets/frontend/vendor/scripts/jquery/jquery-1-10-2/jquery.js"></script>
    <script type="" src="<?php echo base_url();?>assets/frontend/phpholidays_default/vendor/scripts/js/modernizr.custom.min.js"></script>

    <!-- include star rating -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/css/star-rating.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/css/star-rating.min.css">

<script src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/js/star-rating.js"></script>
<script src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/rating/js/star-rating.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <title>Traveling Now !</title>
</head>

<body>
    <!-- PHP Holidays Main Template Start -->
    <!-- Header Start -->

    <div id="top-bar">
        <div class="container">
            <a href="#" class="menu button"><i class="fa fa-navicon"></i></a>
            <ul class="nav">
                <li class="active"><a><span>Welcome To Travelling Now</span></a></li>
                <?php 
                if ($this->session->userdata('kk_level') == 2) { ?>
                    <li><a href="<?php echo base_url(); ?>agen/dashboard"><span>Dashboard</span></a></li>
                    <?php   
                }else if($this->session->userdata('kk_level') == 3){ ?>
                        <li><a href="<?php echo base_url(); ?>user/dashboarduser"><span>Dashboard</span></a></li>
                        <?php
                }
            ?>
                            <li><a href="<?php echo base_url(); ?>registration"><span>Registration</span></a></li>
            </ul>
            <div class="flags">

                <script type="text/javascript">
                    $(document).bind('click', function(e) {
                        var $clicked = $(e.target);
                        if (!$clicked.parents().hasClass("dropdown"))
                            $(".dropdown dd ul").hide();
                    });
                </script>
                <div class="flags">
                    <dl id="sample" class="dropdown" style="margin:0; padding:0">
                        <dt><a href="javascript:void(0);"><span class="lang">English - United States</span><span class="flag"><img src="<?php echo base_url();?>assets/data/adminFiles/flagsImage/US.gif" border="0" title="English" /></span> &nbsp;</a></dt>
                        <dd>
                            <ul>
                                <li><a class="change_lang" rel="en_US:ltr" href="javascript:void(0);"><span class="lang">English - United States</span><span class="flag"><img src="<?php echo base_url();?>assets/data/adminFiles/flagsImage/US.gif" border="0" title="English" /></span></a></li>
                                </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12">
                    <div class="logo">
                        <a href="<?php echo base_url();?>/home">
                        <img src="<?php echo base_url();?>assets/data/adminFiles/headerImages/holiday-logo.png" alt="Travel classified Listing Script | API based Travel Booking Script" title="Travel classified Listing Script | API based Travel Booking Script" border="0">
                    </a>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="support"><i class="fa fa-phone"></i> Book online or call <span>+12 3456 789</span></div>
                    <div class="block-count">
                        <h4>&#51;&#48; Penginapan    + &#52;&#53; Package Tours around Batu City. </h4>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Top Navigation Start-->
    <nav class="top-nav">
        <div class="container">
            <nav>
                <ul class="jetmenu blue">
                    <li style="display: block;" class="active "><a href="#" title="HOME"><i class="fa fa-home"></i> </a>
                    </li>
                      <li style="display: block;" class=" "><a href="#"  title="">TOUR PLACES <i class="indicator fa fa-angle-down"></i></a>
                                <ul class="dropdown" style="display: none;">
                                    <li>
                                        <a href="<?php echo base_url();?>home/category_wisata/alam" >NATURE TRIP </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>home/category_wisata/buatan" > ARTIFICIAL TRIP </a>
                                    </li>
                                </ul>                    
                        </li>
                    <li style="display: block;" class=" "><a href="<?php echo base_url(); ?>find_place" title="">FIND PLACE BY MAP</a>
                    </li>
                   <li style="display: block;" class=" "><a href="#"  title="">PENGINAPAN<i class="indicator fa fa-angle-down"></i></a>
                                <ul class="dropdown" style="display: none;">
                                    <li>
                                        <a href="<?php echo base_url();?>home/category_penginapan/homestay">HOMESTAY</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>home/category_penginapan/villa"> VILLA </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>home/category_penginapan/hotel"> HOTEL </a>
                                    </li>
                                </ul>                    
                    </li>
                    <!-- <li style="display: block;" class=" "><a href="<?php echo base_url(); ?>car_hire" title="">CAR HIRE</a>
                    </li> -->
                </ul>
            </nav>
        </div>
    </nav>
    <!-- Top Navigation End-->

    <!-- Banner Start -->
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/banner-slideshow.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/phpholidays_default/pc/css/rs-slides.min.css" media="screen" rel="stylesheet" type="text/css">

    <div class="callbacks_container hidden-xs">
        <ul class="rslides" id="slider4">
            <li>
                <!--  <img  src="<?php echo base_url();?>assets/data/frontFiles/advertise/advertise_thumb/phpholidays-slide-1.jpg" alt="Image miss"/> -->
                <?php echo $map['js']; ?>
                    <?php echo $map['html']; ?>
                        <div class="dialog">
                            <h1>Travelling Now !</h1>
                            <p>Easy To Find Homestay & Tour Place</p>
                        </div>
            </li>
            <li>
                <img src="<?php echo base_url();?>assets/data/frontFiles/advertise/advertise_thumb/phpholidays-slide-2.jpg" alt="Image miss" />
                <div class="dialog">
                    <h1>PHP Holidays Banner</h1>
                    <p>PHP Holidays Banner</p>
                </div>
            </li>
        </ul>
    </div>
    <!-- Banner End-->
    <?php echo $contents; ?> 
    <!-- iki dinamis content e iso berubah2  -->
        <!--Footer Start-->
        <footer>
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <ins>Important Links:</ins>
                            <ul class="list-inline">
                                <li class="first"><a href="index.html">Home</a></li>
                                <li><a href="All-Hotel-List/group_id/1.html">Hotels</a></li>
                                <li><a href="Flights.html">Flights</a></li>
                                <li><a href="All-Vacation-List/group/1.html">Vacation Rentals</a></li>
                                <li><a href="All-Tour-List/group_id/1.html">Tour Packages</a></li>
                                <li><a href="<?php echo base_url(); ?>car_hire">Hire a Car</a></li>
                                <li><a href="Travel-Tips.html">Travel Tips, Guides, Tools & More</a></li>
                                <li><a href="Photo-Album.html">Photo Album</a></li>
                                <li><a href="Video-Album.html">Video Album</a></li>
                            </ul>
                            <hr>
                            <ins>PHP Holidays Informations:</ins>
                            <ul class="list-inline">
                                <li class="first"><a href="index.html">Home</a></li>
                                <li><a href="About-Us.html">About PHP Holidays</a></li>
                                <li><a href="Write-a-Review.html">Write a review</a></li>
                                <li><a href="Membership.html">Membership</a></li>
                                <li><a href="News.html">News & Events</a></li>
                                <!--li><a href="Feedback">Feedback</a></li-->
                                <li><a href="Site-Map.html">Sitemap</a></li>
                                <li><a href="Contact-Us.html">Contact Us</a></li>
                            </ul>
                        </div>

                        <div class="col-md-4">
                            <div class="social">
                                Always stay connected with us..
                                <br>
                                <br>
                                <a href="https://www.facebook.com/" target="_blank" class="facebook" title="Facebook" rel="nofollow"><span><i class="fa fa-facebook"></i></span></a>
                                <a href="http://www.twitter.com/" target="_blank" class="twitter" title="Twitter" rel="nofollow"><span><i class="fa fa-twitter"></i></span></a>
                                <a href="https://plus.google.com/" target="_blank" class="googleplus" title="Google+" rel="nofollow"><span><i class="fa fa-google-plus"></i></span></a>
                                <a href="https://www.youtube.com/" target="_blank" class="youtube" title="YouTube" rel="nofollow"><span><i class="fa fa-youtube"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottom">
                <div class="container text-center">
                    <div class="copyright">
                        Copyright &copy; 2007- 2017 Travel classified Listing Script | API based Travel Booking Script&trade; All Rights Reserved&nbsp;
                        <a rel="nofollow" href="http://www.phpholidays.com/" target="_blank" title="Demo of Travel Script">Demo of Travel Script</a>&nbsp; Powered by Eicra Soft Ltd&nbsp; Version : &#51;.&#49;&#48;.&#48;&#48;
                    </div>
                </div>
            </div>
        </footer>
        <div class="go-top"><a href="#top" id="scroll-top"><i class="fa fa-angle-up"></i></a></div>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/jquery-ui/blue/jquery-ui.min.js"></script>
        <script type="" src="vendor/scripts/js/frontend/common.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/responsive-tabs.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/tipso.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/top-bar.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/scroll.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/scripts.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/vendor/scripts/js/frontend/slicker.min.js"></script>

        <script type="text/javascript">
            //<!--
            $(document).ready(function() { //on DOM ready

                $("a.change_lang").click(function() {
                    var self = this;
                    var lang = $(self).attr('rel');

                    $.ajax({
                        url: '/Administrator/Settings/language/update',
                        type: 'POST',
                        data: {
                            lang_name: lang
                        },
                        beforeSend: function() {
                            $(self).children("span:nth-child(2)").children("img:nth-child(1)").attr('src', 'data/adminFiles/commonImages/flag_loader.gif');
                            $(self).children("span:nth-child(2)").children("img:nth-child(1)").attr('height', '15');
                        },
                        success: function(response) {
                            //alert(response);
                            var json_arr = eval("(" + response + ")");
                            var url = 'index.html';
                            var baseUrl = '';
                            var ckUrl = 'Administrator/login.html';
                            var match_res = url.match(/Administrator\/login/g);
                            var tmp = false;
                            for (i in match_res) {
                                if (match_res[i] == ckUrl) {
                                    tmp = true;
                                }
                                //alert(i + ' : ' + match_res[i]);
                            }
                            if (tmp == true) {
                                var rightUrl = baseUrl + '/Administrator';
                            } else {
                                var rightUrl = url;
                            }

                            if (json_arr.status == 'ok') {
                                document.location.href = rightUrl;
                            }
                        }
                    });
                });

                $(".dropdown img.flag").addClass(""); //flagvisibility

                $(".dropdown dt a").click(function() {
                    $(".dropdown dd ul").toggle();
                });

                $(".dropdown dd ul li a").click(function() {
                    var text = $(this).html();
                    $(".dropdown dt a").html(text);
                    $(".dropdown dd ul").hide();
                    $("#result").html("Selected value is: " + getSelectedValue("sample"));
                });

                function getSelectedValue(id) {
                    return $("#" + id).find("dt a span.value").html();
                }

                $("#flagSwitcher").click(function() {
                    $(".dropdown img.flag").toggleClass("flagvisibility");
                });
            });

            //-->
        </script>

        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/top-nav.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/members/top-nav.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/banner-slideshow.min.js"></script>
        <script type="text/javascript">
            //<!--
            $(document).ready(function() {

                calender_block('check_in', 'check_out');
                calender_block('check_out', 'check_in');

            });

            function calender_block(id, second_id) {
                $("#" + id).datepicker({
                    showOn: 'button',
                    buttonImage: 'http://demo.phpholidays.com/templates/frontend/phpholidays_default/images/calendar.png',
                    buttonImageOnly: true,
                    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    showAnim: 'slide',
                    buttonText: 'Calender',
                    dateFormat: 'yy-mm-dd',
                    autoSize: false,
                    showButtonPanel: false,
                    changeMonth: true,
                    changeYear: true,
                    onSelect: function(dateText, inst) {
                        var id_type_arr = id.split('_');
                        var true_id = id_type_arr[0] + id_type_arr[1];
                        var currentTime = new Date();
                        var currentDay = (currentTime.getDate() < 10) ? '0' + currentTime.getDate() : currentTime.getDate();
                        var currentMonth = ((currentTime.getMonth() + 1) < 10) ? '0' + (currentTime.getMonth() + 1) : (currentTime.getMonth() + 1);
                        var today = currentTime.getFullYear() + '-' + currentMonth + '-' + currentDay;
                        if (true_id == 'checkin') {
                            if ((dateText != '') && ((toTimestamp_block(dateText) >= toTimestamp_block($('#' + second_id).val())) || (toTimestamp_block(dateText) < toTimestamp_block(today)))) {
                                var msg = "Checkin date can not be less than or equal to current date and greater than or equal to Checkout date.";
                                openMsgDialog_block(msg);
                                $('#' + id).val('');
                            }
                        } else if (true_id == 'checkout') {
                            if ((dateText != '') && ((toTimestamp_block(dateText) <= toTimestamp_block($('#' + second_id).val())) || (toTimestamp_block(dateText) < toTimestamp_block(today)))) {
                                var msg = "Checkout date can not be less than or equal to current date and Checkin date.";
                                openMsgDialog_block(msg);
                                $('#' + id).val('');
                            }
                        }

                        if (id_type_arr[2] == 'departure') {
                            if ((dateText != '') && ((toTimestamp_block(dateText) >= toTimestamp_block($('#' + second_id).val())) || (toTimestamp_block(dateText) < toTimestamp_block(today)))) {
                                var msg = "Departure date cannot be less than or equal to current date and greater than or equal to Arrival date.";
                                openMsgDialog_block(msg);
                                $('#' + id).val('');
                            }
                        } else if (id_type_arr[2] == 'arrival') {
                            if ((dateText != '') && ((toTimestamp_block(dateText) <= toTimestamp_block($('#' + second_id).val())) || (toTimestamp_block(dateText) < toTimestamp_block(today)))) {
                                var msg = "Arrival date cannot be less than or equal to current date and Departure date.";
                                openMsgDialog_block(msg);
                                $('#' + id).val('');
                            }
                        }
                    }
                });

                $("img[class='ui-datepicker-trigger']").each(function() {
                    $(this).attr('style', '');
                });
            }

            function toTimestamp_block(strDate) {
                var datum = Date.parse(strDate);
                return datum / 1000;
            }

            function openMsgDialog_block(msg) {
                $('#dialog_container_block').html(msg).dialog({
                    autoOpen: true,
                    modal: false,
                    show: 'fade',
                    resizable: 'false',
                    buttons: {
                        "Ok": function() {
                            $(this).dialog('option', 'hide', 'explode').dialog("close");
                        }
                    },
                    open: function(event, ui) {
                        setTimeout(function() {
                            if ($('#' + event.target.id).dialog('isOpen')) {
                                //$('#' + event.target.id).dialog('option', 'hide', 'explode').dialog('close');
                            }
                        }, 5000);
                    }
                });
            }

            function stripslashes_block(str) {
                str = str.replace(/\\'/g, '\'');
                str = str.replace(/\\"/g, '"');
                str = str.replace(/\\0/g, '\0');
                str = str.replace(/\\\\/g, '\\');
                return str;
            }

            function removeError_block() {
                var filename = '';
                var filevalue = '';
                $.each($('form').serializeArray(), function(i, field) {
                    if (field.name.match(/\[\]$/)) {
                        var filearr = field.name.split('[]');
                        filename = filearr[0];
                    } else {
                        filename = field.name;
                    }
                    $('input[name="' + filename + '[]"]').removeClass('ui-state-error');
                });
            }

            function checkRouteFieldValue_block() {
                var check = true;
                var attr_id = '';
                var id_no_arr = [];
                removeError_block();
                for (var i = 0; i < $('input.flight_route_departure').length; i++) {
                    attr_id = $('input.flight_route_departure:eq(' + i + ')').attr('id');
                    id_no_arr = attr_id.split('flight_route_departure');
                    if (jQuery.trim($('input.flight_route_departure:eq(' + i + ')').val()) == '' || jQuery.trim($('input.flight_route_arrival:eq(' + i + ')').val()) == '' || jQuery.trim($('input.flight_route_departure_date:eq(' + i + ')').val()) == '' || ($('input[name="trip_type"]:checked').val() == 'Round' && jQuery.trim($('input.flight_route_arrival_date:eq(' + i + ')').val()) == '')) {
                        if (jQuery.trim($('input.flight_route_departure:eq(' + i + ')').val()) == '') {
                            $('input.flight_route_departure:eq(' + i + ')').addClass('ui-state-error');
                        }

                        if (jQuery.trim($('input.flight_route_arrival:eq(' + i + ')').val()) == '') {
                            $('input.flight_route_arrival:eq(' + i + ')').addClass('ui-state-error');
                        }

                        if (jQuery.trim($('input.flight_route_departure_date:eq(' + i + ')').val()) == '') {
                            $('input.flight_route_departure_date:eq(' + i + ')').addClass('ui-state-error');
                        }

                        if ($('input[name="trip_type"]:checked').val() == 'Round' && jQuery.trim($('input.flight_route_arrival_date:eq(' + i + ')').val()) == '') {
                            $('input.flight_route_arrival_date:eq(' + i + ')').addClass('ui-state-error');
                        }

                        check = false;
                    }
                }
                if (check == false) {
                    msg = '<span class=\'ui-icon ui-icon-notice\' style=\'float:left; margin:0 0px 0px 0;\'></span>Some Fields are empty';
                    openMsgDialog_block(msg);
                }
                return check;
            }

            function autoList_block(id) {
                autoSuggestion(id, "/Flight/frontend/iata", {
                    'datas': ''
                }, ['iata_code', 'iata_code', 'flight_label', 'flight_value', 'flight_location'], '', 3);
            }

            function checkSameData_block(from_id, to_id, id_no) {
                $('#' + from_id + id_no).blur(function() {
                    if (($('#' + from_id + id_no).val() != '') && ($('#' + from_id + id_no).val() == $('#' + to_id + id_no).val())) {
                        var msg = "From city and destination city can not be same.";
                        openMsgDialog_block(msg);
                        $('#' + from_id + id_no).val('');
                        $('#' + from_id + id_no).focus();
                    }
                });

                $('#' + to_id + id_no).blur(function() {
                    if (($('#' + to_id + id_no).val() != '') && ($('#' + from_id + id_no).val() == $('#' + to_id + id_no).val())) {
                        var msg = "From city and destination city can not be same.";
                        openMsgDialog_block(msg);
                        $('#' + to_id + id_no).val('');
                        $('#' + to_id + id_no).focus();
                    }
                });

                calender_block(from_id + '_date' + id_no, to_id + '_date' + id_no);
                calender_block(to_id + '_date' + id_no, from_id + '_date' + id_no);
                autoList_block(from_id + id_no);
                autoList_block(to_id + id_no);
            }

            $(document).ready(function() {

                checkSameData_block('flight_route_departure', 'flight_route_arrival', 0);

                $("input[name='trip_type']").click(function() {
                    var val = $(this).val();

                    switch (val) {
                        case 'One way':
                            $('span.flight_route_arrival_date_box').css('display', 'none');
                            $('#class_type_div').removeClass('col-md-4')
                            $('#class_type_div').addClass('col-md-3');
                            break;
                        case 'Round':
                            $('span.flight_route_arrival_date_box').css('display', 'inline');
                            $('#class_type_div').removeClass('col-md-3');
                            $('#class_type_div').addClass('col-md-4');
                            break;
                    }
                });

            });

            $(document).ready(function() {

                $('a.searchHotelButton').click(function() {
                    $('#formHotelBlockSearch').submit();
                });

                autoSuggestion('location_for_search', "/Hotels/search/autosearch", {
                    'location_for_search': '',
                    'hotels_name': '',
                    'city': '',
                    'state_name': '',
                    'country_name': ''
                }, ['hotels_primary_image', 'hotels_image', 'hotels_name', 'location_for_search', 'hotels_location'], 'data/frontFiles/hotels/hotels_image', 2);

            });
            $(document).ready(function() {

                $('a.searchTourButton').click(function() {
                    $('#formTourBlockSearch').submit();
                });

                autoSuggestion(
                    'post_code_tour_b',
                    "/tours/frontend/searchname", {
                        'post_code': ''
                    }, ['tours_primary_image', 'tours_image', 'tours_name', 'post_code', 'owner_name', 'tours_price', 'country', 'state', 'tours_code'],
                    'data/frontFiles/tours/tours_image', 2);

                $('#country_id_T').change(function() {
                    if ($('#country_id_T option:selected').val() == 'any' || $('#country_id_T option:selected').val() == '') {
                        $('#area_id_T').html('<option value="any">Select Area .....</option>');
                        $('#state_id_T').html('<option value="any">Select State .....</option>');
                    } else {
                        $.ajax({
                            url: "/tours/frontend/state",
                            type: 'POST',
                            data: {
                                id: $('#country_id_T option:selected').val()
                            },
                            beforeSend: function() {
                                $('#state_id_T').html('<option value="any">Loading Data.....</option>');
                                $('#area_id_T').html('<option value="any">Select State .....</option>');
                                $('#state_id_T_err').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            },
                            success: function(response) {
                                var json_arr = eval("(" + response + ")");
                                var option = '<option value="any">Select State .....</option>';
                                if (json_arr.status == 'ok') {
                                    for (var i = 0; i < json_arr.states.length; i++) {
                                        option += '<option value="' + json_arr.states[i].state_id + '" >' + stripslashes_block(json_arr.states[i].state_name) + '</option>';
                                    }
                                    $('#state_id_T').html(option);
                                } else {
                                    $('#state_id_T').html('<option value="any">' + json_arr.msg + '</option>');
                                }
                                $('#state_id_T_err').html('');
                            }
                        });
                    }
                });

                $('#state_id_T').change(function() {

                    if ($('#state_id_T option:selected').val() == 'any' || $('#state_id_T option:selected').val() == '') {
                        $('#area_id_T').html('<option value="any">Select State .....</option>');
                    } else {
                        $.ajax({
                            url: "/tours/frontend/area",
                            type: 'POST',
                            data: {
                                id: $('#state_id_T option:selected').val()
                            },
                            beforeSend: function() {
                                $('#area_id_T').html('<option value="any">Loading Data.....</option>');
                                $('#area_id_T_err').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            },
                            success: function(response) {
                                var json_arr = eval("(" + response + ")");
                                var option = '<option value="any">Select State .....</option>';
                                if (json_arr.status == 'ok') {
                                    for (var i = 0; i < json_arr.cities.length; i++) {
                                        option += '<option value="' + json_arr.cities[i].city_id + '" >' + stripslashes_block(json_arr.cities[i].city) + '</option>';
                                    }
                                    $('#area_id_T').html(option);
                                } else {
                                    $('#area_id_T').html('<option value="any">' + json_arr.msg + '</option>');
                                }
                                $('#area_id_T_err').html('');
                            }
                        });
                    }
                });

            });

            $(document).ready(function() {
                $('#return_box').hide();
                var settingBlockObj = {
                    form_action: {
                        form_id: 'formCarHireSearch',
                        common_msg_field_id: ''
                    },
                    search_action: {},
                    calendar: {
                        calendar_include_time: true,
                        calendar_img_link: 'templates/backend/default/pc/images/common/calendar.png',
                        monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                        buttonText: 'calendar',
                        dateFormat: 'yy-mm-dd',
                        dateTimeFormat: "yyyy-MM-dd hh:mm:ss tt",
                        culture: "",
                        value: ''
                    },
                    combobox: {
                        combobox_id: '#rental_car_type',
                        combobox_settings: {
                            autoBind: true,
                            dataTextField: "car_type_format",
                            dataValueField: "car_type",
                            optionLabel: "Select...........",
                            headerTemplate: '<div class="container-fluid"><div class="row text-center" style="background-color:#FCF8E3; border-bottom:1px solid #BBB; box-shadow: 0 1px 2px #CCC; padding-bottom:6px;"><div class="dropdown-header">' +
                                '<div class="col-md-3"><span class="k-widget k-header" style="padding:2px 5px;">' + "Thumb" + '</span></div>' +
                                '<div class="col-md-9"><span class="k-widget k-header" style="padding:2px 5px;">' + "Description" + '</span></div>' +
                                '</div></div></div>',
                            valueTemplate: '# if(data.vehicle_image_format){ #<img class="selected-value" src=\"#= data.vehicle_image_format #\" alt=\"#:data.vehicle_name#\" height="30" class="img-responsive" /># } #  #if(data.car_type_format){ #<span>#:data.car_type_format#</span># } #',
                            template: '# if(data.vehicle_image_format){ #<div class="container-fluid desc"><div class="row"><div class="col-md-3"><span class="k-state-default"><img src=\"#= data.vehicle_image_format #\" alt=\"#:data.vehicle_name#\" class="img-responsive img-thumbnail car-img"/></span></div># } #' +
                                '# if(data.description){ # <div class="col-md-9"><span class="k-state-default"><span class="label label-success">#: data.car_type_format # </span><br> <span class="label label-warning">#: data.vehicle_name #</span><p>#: data.description #</p></span></div></div># }else{ #' + "Select..........." + '# } #',
                            dataSource: {
                                type: "json",
                                schema: {
                                    data: 'data_result'
                                },
                                transport: {
                                    read: {
                                        type: "POST",
                                        dataType: "json",
                                        url: "/Costcalculator/search/vehicles"
                                    }
                                }
                            },
                            value: "",
                            change: function(e) {}
                        }
                    },
                    messages: {
                        display: 'portfolio_pagination_messages_display',
                        empty: 'portfolio_pagination_messages_empty',

                        page: 'portfolio_pagination_messages_page',
                        of: 'portfolio_pagination_messages_of',
                        itemsPerPage: 'portfolio_pagination_messages_items_per_page',
                        first: 'portfolio_pagination_messages_first',
                        previous: 'portfolio_pagination_messages_previous',
                        next: 'portfolio_pagination_messages_next',
                        last: 'portfolio_pagination_messages_last',
                        refresh: 'portfolio_pagination_messages_refresh',

                        common_msg_dialog_id: 'dialog_msg',
                        common_confirn_dialog_id: 'dialog-confirm',
                        common_ok: "common_ok",
                        common_delete_title: "common_delete_title",
                        common_delete_selected: "common_delete_selected",
                        common_cancel: "common_cancel",
                        common_delete_confirm: "cost_list_delete_all",
                        common_save_btn_text: "autos_date_add_page_save",
                        common_field_must_not_empty: "common_field_must_not_empty",
                        common_publish_title: "common_publish_title",
                        common_unpublish_title: "common_unpublish_title",
                        common_publish_selected: "common_publish_selected",
                        common_unpublish_selected: "common_unpublish_selected",
                        common_publish_perm: "common_publish_perm",
                        common_perm_err: "common_perm_err",
                        common_selected_err: "cost_selected_err"
                    }
                };

                commonCalendar('pickup_date_time', settingBlockObj, settingBlockObj.calendar.calendar_include_time);
                commonCalendar('return_date_time', settingBlockObj, settingBlockObj.calendar.calendar_include_time);
                kendoComboBox(settingBlockObj);

                $('a.calculate_cost_btn').click(function() {
                    var r = true;
                    commonRemoveFormError(settingBlockObj);
                    if ($('#pickup_location').val() == null || $('#pickup_location').val() == '') {
                        commonAddFormError('pickup_location', "Please enter pickup location");
                        r = false;
                    }
                    if ($('#return_car').is(':checked')) {
                        if ($('#return_location').val() == null || $('#return_location').val() == '') {
                            commonAddFormError('return_location', "Please enter return location");
                            r = false;
                        }
                    }
                    if ($('#pickup_date_time').val() == null || $('#pickup_date_time').val() == '') {
                        commonAddFormError('pickup_date_time', "Please enter pickup date time");
                        r = false;
                    }
                    if ($('#return_date_time').val() == null || $('#return_date_time').val() == '') {
                        commonAddFormError('return_date_time', "Please enter return date time");
                        r = false;
                    }
                    if ($('#rental_car_type').val() == null || $('#rental_car_type').val() == '') {
                        commonAddFormError('rental_car_type', "Select A Car Type");
                        r = false;
                    }

                    if (r === true) {
                        $('#' + settingBlockObj.form_action.form_id).submit();
                    }
                });

                $('#return_car').click(function() {
                    if ($(this).is(':checked')) {
                        $('#return_box').slideDown();
                    } else {
                        $('#return_box').slideUp();
                    }
                });
            });
            $(document).ready(function() {

                $('a.searchVacationButton').click(function() {
                    $('#formVacationBlockSearch').submit();
                });

                autoSuggestion('post_code_B', "/vacationrentals/frontend/searchname", {
                        'post_code': ''
                    }, ['vacationrentals_primary_image', 'vacationrentals_image', 'vacationrentals_name', 'post_code', 'owner_name', 'vacationrentals_price', 'country', 'state', 'vacationrentals_address'],
                    'data/frontFiles/vacationrentals/vacationrentals_image', 2);

                $('#country_id_B').change(function() {
                    if ($('#country_id_B option:selected').val() == 'any' || $('#country_id_B option:selected').val() == '') {
                        $('#area_id_B').html('<option value="any">Select Area .....</option>');
                        $('#state_id_B').html('<option value="any">Select State .....</option>');
                    } else {
                        $.ajax({
                            url: "/Vacationrentals-list-state",
                            type: 'POST',
                            data: {
                                id: $('#country_id_B option:selected').val()
                            },
                            beforeSend: function() {
                                $('#state_id_B').html('<option value="any">Loading Data.....</option>');
                                $('#area_id_B').html('<option value="any">Select Area .....</option>');
                                $('#state_id_err').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            },
                            success: function(response) {
                                var json_arr = eval("(" + response + ")");
                                var option = '<option value="any">Select State .....</option>';
                                if (json_arr.status == 'ok') {
                                    for (var i = 0; i < json_arr.states.length; i++) {
                                        option += '<option value="' + json_arr.states[i].state_id + '" >' + stripslashes_block(json_arr.states[i].state_name) + '</option>';
                                    }
                                    $('#state_id_B').html(option);
                                } else {
                                    $('#state_id_B').html('<option value="any">' + json_arr.msg + '</option>');
                                }
                                $('#state_id_err').html('');
                            }
                        });
                    }
                });

                $('#state_id_B').change(function() {

                    if ($('#state_id_B option:selected').val() == 'any' || $('#state_id_B option:selected').val() == '') {
                        $('#area_id_B').html('<option value="any">Select Area .....</option>');
                    } else {
                        $.ajax({
                            url: "/Vacationrentals-list-city",
                            type: 'POST',
                            data: {
                                id: $('#state_id_B option:selected').val()
                            },
                            beforeSend: function() {
                                $('#area_id_B').html('<option value="any">Loading Data.....</option>');
                                $('#area_id_err').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                            },
                            success: function(response) {
                                //alert(response);
                                var json_arr = eval("(" + response + ")");
                                var option = '<option value="any">Select Area .....</option>';
                                if (json_arr.status == 'ok') {
                                    for (var i = 0; i < json_arr.cities.length; i++) {
                                        option += '<option value="' + json_arr.cities[i].city_id + '" >' + stripslashes_block(json_arr.cities[i].city) + '</option>';
                                    }
                                    $('#area_id_B').html(option);
                                } else {
                                    $('#area_id_B').html('<option value="any">' + json_arr.msg + '</option>');
                                }
                                $('#area_id_err').html('');
                            }
                        });
                    }
                });

            });
            //-->
        </script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/resp-carousel.min.js"></script>
        <script type="text/javascript">
            //<!--
            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    autoPlay: 4000,
                    stopOnHover: true,
                    autoHeight: true,
                    items: 3,
                    itemsDesktop: [1199, 2],
                    itemsDesktopSmall: [979, 2]
                });
            });
            //-->
        </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/frontend/phpholidays_default/vendor/scripts/js/email-newsletter.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/vendor/scripts/kendo/js/kendo.web.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/vendor/scripts/kendo/js/cultures/kendo.culture.en.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/vendor/scripts/kendo/js/cultures/kendo.culture.en-US.min.js"></script>
        <script type="" src="<?php echo base_url(); ?>assets/frontend/module/Portfolio/js/uploader.min.js"></script>

        <script type="text/javascript">
            //<!--
            function loginSendAction(e) {
                var settingObj = e.data;
                var formData = commonGetFormData(settingObj.form_action.form_id, settingObj.form_action.hasTinyMCE);

                $.ajax({
                    url: settingObj.form_action.url,
                    type: 'POST',
                    data: formData,
                    beforeSend: function() {
                        if (settingObj.form_action.beforeSend && settingObj.form_action.beforeSend.commonRemoveFormError) {
                            commonRemoveFormError(settingObj);
                        }
                        if (settingObj.form_action.beforeSend && settingObj.form_action.beforeSend.commonOpenLoaderDialog) {
                            commonOpenLoaderDialog(settingObj);
                        }
                        if (settingObj.form_action.beforeSend && settingObj.form_action.beforeSend.beforeSendLoginFunction) {
                            settingObj.form_action.beforeSend.beforeSendLoginFunction(e);
                        }
                    },
                    success: function(response) {
                        try {
                            var json_arr = $.parseJSON(response);

                            if (json_arr.status == 'ok') {
                                $('#' + settingObj.form_action.common_msg_field_id).html(succMsgDesign(json_arr.msg));
                                commonResetFormFields(settingObj);
                                $('#login_box').css('display', 'none');
                                $('#logout_box').css('display', '');
                                if (settingObj.form_action.success_function) {
                                    settingObj.form_action.success_function(e, json_arr);
                                }
                            } else if (json_arr.status == 'errV') {
                                $('#' + settingObj.form_action.common_msg_field_id).html(errMsgDesign("There are some error to perform this action. Please See below --"));

                                for (var i = 0; i < json_arr.msg.length; i++) {
                                    commonAddFormError(json_arr.msg[i].key, json_arr.msg[i].value);
                                }
                                if (settingObj.form_action.errv_function) {
                                    settingObj.form_action.errv_function(json_arr);
                                }
                            } else {
                                $('#' + settingObj.form_action.common_msg_field_id).html(errMsgDesign(json_arr.msg));
                                if (settingObj.form_action.error_function) {
                                    settingObj.form_action.error_function(json_arr);
                                }
                            }
                            if (settingObj.form_action.beforeSend && settingObj.form_action.beforeSend.commonOpenLoaderDialog) {
                                commonCloseLoaderDialog(settingObj);
                            }
                        } catch (error) {
                            var error_arr = [error, response];
                            var msg = commonErrMsgException(error_arr);
                            $('#' + settingObj.form_action.common_msg_field_id).html(errMsgDesign(msg));
                        }
                    },
                    error: function(xhr, status, error) {
                        if (settingObj.form_action.beforeSend && settingObj.form_action.beforeSend.commonOpenLoaderDialog) {
                            commonCloseLoaderDialog(settingObj);
                        }
                        var msg = "Error! " + xhr.status + " " + error;
                        $('#' + settingObj.form_action.common_msg_field_id).html(errMsgDesign(msg));
                    }
                });
            }
            //-->
        </script>
        <script type="text/javascript">
            //<!--
            $(document).ready(function() {
                $('a.topbar_logout_btn').click(logoutSettingObj, logoutAction);
            });

            var logoutSettingObj = {
                form_action: {

                    logout_url: "/Frontend-Logout",
                    hasTinyMCE: false,
                    common_msg_field_id: 'actionMessage',
                    dialog_container_id: 'dialog_container',
                    dialog_progressbar_id: 'progressbar',
                    extra_field_for_reset: [],

                    beforeSend: {
                        beforeSendLogoutFunction: function(e) {
                            $('a.topbar_logout_btn').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        }
                    },
                    success_function_logout: function(e, json_arr) {
                        $('a.topbar_logout_btn').html("<span>Log In</span>");
                        $('a.topbar_logout_btn').attr('href', 'Frontend-Login');
                        $('a.topbar_logout_btn').unbind('click');
                        $('a.topbar_logout_btn').attr('class', '');
                    }
                },
                messages: {
                    common_msg_dialog_id: 'dialog_msg',
                    common_confirn_dialog_id: 'dialog-confirm',
                    common_ok: "Ok"
                }
            };
            //-->
        </script>
        <!--Footer End-->
        <!-- PHP Holidays Main Template End -->

</body>

<!-- Mirrored from demo.phpholidays.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 16 Jan 2017 05:29:37 GMT -->

</html>