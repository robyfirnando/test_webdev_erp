<!-- Body Start-->
<div class="container">
	    <!-- Inner Layout Start -->
    <div class="inner">
        <div class="row">

            <!-- Content Start -->
            <div class="col-md-12">
                <div class="drawer-overlay costcalculator">
                    
<div class="module car-hire">
	<div class="banner" style="min-height: 640px;">
		<img class="img-responsive" alt="Hire a Car" src="http://demo.phpholidays.com/data/frontFiles/costcalculator/costcalculator_frontend/autos-slide-2.jpg">
		<div class="block-car-hire">
			<div class="block-body">
				<form name="formSearch" id="formSearch" method="post" role="form" action="/Search-Vehicle-Find">
					<div id="actionMessage"></div>
					<div id="generalInfo">
						<h3><i class="fa fa-car"></i> JOURNEY DETAILS</h3>
						<div class="form-group">
							<label for="pickup_location">Pickup Location (City, State, Airport Code or US Zip Code) <span class="required">*</span></label>
							<input type="text" name="pickup_location" id="pickup_location" value="" class="form-control"/>
							<span class="input-errors" id="pickup_location_err"></span>
						</div>

						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="return_car" id="return_car"   /> Return car to a different Hertz location</label>
								<span class="input-errors" id="return_car_err"></span>
							</div>
						</div>

						<div class="form-group" id="return_box">
							<label for="return_location">Return Location (City, State, Airport Code or US Zip Code) <span class="required">*</span></label>
							<input type="text" name="return_location" id="return_location" value=""  class="form-control" />
							<span class="input-errors" id="return_location_err"></span>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="pickup_date_time">Pickup Date & Time : <span class="required">*</span></label>
									<input type="text" name="pickup_date_time" id="pickup_date_time"  value="" />
									<span class="input-errors" id="pickup_date_time_err"></span>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="return_date_time">Return Date & Time : <span class="required">*</span></label>
									<input type="text" name="return_date_time" id="return_date_time" value="" />
									<span class="input-errors" id="return_date_time_err"></span>
								</div>
							</div>
						</div>
												<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="service_type">Type of Service : <span class="required">*</span></label>
								   	<select name="package&#x5B;generalInfoBlock&#x5D;&#x5B;service_type&#x5D;" id="service_type" class="form-control" title="Type&#x20;of&#x20;Service" style="width&#x3A;100&#x25;" required="required"><option value="">--Select Service--</option>
<option value="Ride&#x20;TO&#x20;the&#x20;airport">Ride TO the airport</option>
<option value="Ride&#x20;FROM&#x20;the&#x20;airport">Ride FROM the airport</option>
<option value="Hourly&#x20;Service">Hourly Service</option>
<option value="Door&#x20;to&#x20;Door&#x20;Service">Door to Door Service</option>
<option value="Long&#x20;Distance&#x20;Service">Long Distance Service</option></select>									<span class="input-errors" id="service_type_err"></span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="passengers">Passengers : <span class="required">*</span></label>
																		<select name="passengers" id="passengers" class="form-control">
										<option value="">Select.......</option>
										<option value="1" >1</option><option value="2" >2</option><option value="3" >3</option><option value="4" >4</option><option value="5" >5</option><option value="6" >6</option><option value="7" >7</option><option value="8" >8</option><option value="9" >9</option><option value="10" >10</option>									</select>
									<span class="input-errors" id="passengers_err"></span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label for="luggage">Luggage : <span class="required">*</span></label>
																		<select name="luggage" id="luggage" class="form-control">
										<option value="">Select.......</option>
										<option value="1" >1</option><option value="2" >2</option><option value="3" >3</option><option value="4" >4</option><option value="5" >5</option><option value="6" >6</option><option value="7" >7</option><option value="8" >8</option><option value="9" >9</option><option value="10" >10</option><option value="11" >11</option><option value="12" >12</option><option value="13" >13</option><option value="14" >14</option><option value="15" >15</option><option value="16" >16</option><option value="17" >17</option><option value="18" >18</option><option value="19" >19</option><option value="20" >20</option><option value="21" >21</option><option value="22" >22</option><option value="23" >23</option><option value="24" >24</option><option value="25" >25</option><option value="26" >26</option><option value="27" >27</option><option value="28" >28</option><option value="29" >29</option><option value="30" >30</option>									</select>
									<span class="input-errors" id="luggage_err"></span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group">

										<label for="rental_car_type">Rental Car Type : <span class="required">*</span></label>
										<br/>
										<input name="rental_car_type" id="rental_car_type" style="width: 100%" value="" />
										<span class="input-errors" id="rental_car_type_err"></span>

								</div>
							</div>
						</div>
					</div>
				</form>

				<div class="text-center">
					<a href="javascript: void(0);" class="calculate_cost_btn btn">Calculate</a>
				</div>
			</div>
		</div>
	</div>
   			<div class="row">
		<br/>
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- Costcalculator-v400-1 -->
			<ins class="adsbygoogle"
			     style="display:block"
			     data-ad-client="ca-pub-9052681341488812"
			     data-ad-slot="7189172938"
			     data-ad-format="auto"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
        </div>
		<div class="col-md-2"></div>

		</div>
</div>

<div id="dialog_msg" title="Dialog msg"></div>
<div id="dialog_container" style="display:none" title="Processing........ Please wait.">
	<div class="alert alert-success">Calculating distance from Google Distance Matrix API and Costing for this distance</div>
    <div class="progress progress-striped active">
		<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        	<span class="sr-only">100% Complete</span>
        </div>
	</div>
</div>
<div id="dialog_container_remove" style="display:none" title="Processing........ Please wait.">
	<div class="alert alert-success">Please wait while removing additional cost(s)</div>
	<div class="progress progress-striped active">
		<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        	<span class="sr-only">100% Complete</span>
        </div>
	</div>
</div>
<div id="dialog_not_calculate" style="display:none" title="Processing........ Please wait.">
	<div class="alert alert-success">Please wait while calculating the cost.</div>
	<div class="progress progress-striped active">
		<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
        	<span class="sr-only">100% Complete</span>
        </div>
	</div>
</div>                </div>
            </div>
            <!-- Content Start -->
        </div>
    </div>
    <!-- Inner Layout End -->
    
</div>
<!-- Body End-->

